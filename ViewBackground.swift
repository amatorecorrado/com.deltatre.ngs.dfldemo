//
//  ViewBackground.swift
//  AppleTV
//
//  Created by Federico Bortoluzzi on 06/07/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ViewBackground: BaseViewController {
    //var appInitType = AppInitType()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        self.view.isUserInteractionEnabled = true
        
        let urlBackground = URL(string: (self.viewConfiguration?.images.getValue("background"))!)
        let dataBackground = try? Data(contentsOf: urlBackground!)
        if (dataBackground?.count > 0) {
            self.view.backgroundColor = UIColor(patternImage: UIImage(data: dataBackground!)!)
        } else {
            self.view.backgroundColor = UIColor.black
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
