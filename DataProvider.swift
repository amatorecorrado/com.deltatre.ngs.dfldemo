//
//  DataProvider.swift
//  AppleTV
//
//  Created by Corrado Amatore on 03/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}



class DataProvider{
    
    var matchTranslation: [String:Translations]
    //var notificationList: [EventModel] = []
    
    var oldNotifications: [EventModel]
    var newNotifications: [EventModel]
    
    var oldLineups: LineupModel
    var newLineups: LineupModel
    
    var oldSchedules: Schedule
    var newSchedules: Schedule
    
    var oldClips: [ClipModel]
    var newClips: [ClipModel]
    
    var newMatch: Match?
    var oldMatch: Match?
    
    var matchNotifications: [Int:[EventModel]]
    var matchClips: [Int:[ClipModel]]
    var matchLineups: [Int:LineupModel]
    var schedules: [String:Schedule]
    
    var _cameraTraslation: [CameraModel]
    var cameraTraslation: [CameraModel]{
        get{
            if(self._cameraTraslation.count == 0){
                let controller = CameraController(feedUrl: feedCameraTraslation)
                self._cameraTraslation = controller.get()
            }
            return self._cameraTraslation
        }
        set(value){
            self._cameraTraslation = value
        }
    }
    
    var eventsIconsCommentaryBaseUrl: String = ""
    var feedVideodataUrl: String = ""
    var feedEventsNGS: String = ""
    var feedClips: String = ""
    var feedCameraTraslation: String = ""
    var feedLineups: String = ""
    var feedMatchdaySchedule = ""
    var feedFullMatchList = ""
    var feedTranslations = ""
    var feedMatch = ""
    var playerThumbBaseUrl: String = ""
    var teamLogoBaseUrl: String = ""
    //var isLive: Bool = false
    
    //var timerFetch: Timer? = Timer.init()
    var repeatingTimer = RepeatingTimer()
    //var fetchInterval: Int = 0
    
    static let sharedInstance = DataProvider()
    
    init(){
        
        matchTranslation = [:]
        matchNotifications = [:]
        matchClips = [:]
        matchLineups = [:]
        schedules = [:]
        
        _cameraTraslation = []
        oldNotifications = [EventModel]()
        newNotifications = [EventModel]()
        
        oldLineups = LineupModel()
        newLineups = LineupModel()
        
        oldSchedules = Schedule()
        newSchedules = Schedule()
        
        oldClips = []
        newClips = []
        
    }
    
    /*init(appInitType: AppInitType)
     {
     eventsIconsCommentaryBaseUrl = appInitType.UrlEventsIcons_commentary
     feedVideodataUrl = appInitType.FeedVideodata
     feedEventsNGS = appInitType.FeedEventsNGS
     feedClips = appInitType.FeedClips
     feedCameraTraslation  = appInitType.FeedVocabularyCamera
     feedLineups = appInitType.FeedLineups
     feedMatchdaySchedule = appInitType.FeedMatchdaySchedule
     feedTranslations = appInitType.FeedTranslations
     playerThumbBaseUrl = appInitType.FeedPlayerThumb
     teamLogoBaseUrl = appInitType.FeedTeamLogo
     
     matchTranslation = [:]
     matchNotifications = [:]
     matchClips = [:]
     matchLineups = [:]
     schedules = [:]
     
     _cameraTraslation = []
     }
     */
    func setParameters(_ configuration: ConfigurationModel){
        
        //FEEDS
        feedVideodataUrl = configuration.feeds.getValue("feedVideodata")
        feedEventsNGS = configuration.feeds.getValue("feedEventsNGS")
        feedClips = configuration.feeds.getValue("feedClips")
        feedCameraTraslation  = configuration.feeds.getValue("feedVocabularyCamera")
        feedLineups = configuration.feeds.getValue("feedLineups")
        feedMatchdaySchedule = configuration.feeds.getValue("feedMatchdaySchedule")
        feedFullMatchList = configuration.feeds.getValue("feedFullMatchList")
        feedTranslations = configuration.feeds.getValue("feedTranslations")
        feedMatch = configuration.feeds.getValue("feedMatch")
        
        //IMAGES
        playerThumbBaseUrl = configuration.images.getValue("playerThumb")
        teamLogoBaseUrl = configuration.images.getValue("teamLogo")
        eventsIconsCommentaryBaseUrl = configuration.images.getValue("events_icons_commentary")
        
    }
    
    //    func fetchMatchData(_ val:Timer){
    //        let userInfo = val.userInfo as! Dictionary<String, AnyObject>
    //        let cup = userInfo["cup"] as! Int
    //        let season = userInfo["season"] as! Int
    //        let roundId = userInfo["roundId"] as! Int
    //        let matchDay = userInfo["matchDay"] as! Int
    //        let matchId = userInfo["matchId"] as! Int
    //
    //        fetchMatchData(cup: cup, season: season, roundId: roundId, matchDay: matchDay, matchId: matchId)
    //    }
    
    public func fetchMatchData(parameters: ParameterModel, fetchInterval: Int = 0){
        
        var cup:Int = 0
        var cupCode:String = ""
        var season:Int = 0
        var roundId: Int = 0
        var matchDay: Int = 0
        var matchId: Int = 0
        
        if let cupid = parameters.Cup, cupid.count > 0 {
            cup = Int(cupid)!
        }
        if let cCode = parameters.CupCode, cCode.count > 0 {
            cupCode = cCode
        }
        if let seas = parameters.Season, seas.count > 0 {
            season = Int(seas)!
        }
        if let rId = parameters.RoundId, rId.count > 0 {
            roundId = Int(rId)!
        }
        if let mday = parameters.Matchday, mday.count > 0 {
            matchDay = Int(mday)!
        }
        if let mid = parameters.MatchId, mid.count > 0 {
            matchId = Int(mid)!
        }
        
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async(execute: {
            self.getMatch(cup, season: season, matchId: matchId)
            self.getLineups(cup, season: season, matchId: matchId)
            self.getSchedule(cupcode: cupCode, season: season)
            self.getNotifications(cup, season: season, matchID: matchId)
            self.getClips(cup, season: season, matchId: matchId)
            
            if(fetchInterval>0){
                
                DispatchQueue.global().async(execute: {
                    DispatchQueue.main.sync{
                        self.repeatingTimer.intervalSeconds = fetchInterval
                        self.repeatingTimer.timer.setEventHandler(handler: { [weak self] in
                            // called every so often by the interval we defined above
                            self?.fetchMatchData(parameters: parameters)
                        })
                        self.repeatingTimer.resume()
                    }
                    
                })
            }
            
            
            
        })
    }
    
    func dismissMatchData(){
        repeatingTimer.deinitTimer()
        oldNotifications = [EventModel]()
        newNotifications = [EventModel]()
        
        oldLineups = LineupModel()
        newLineups = LineupModel()
        
        oldSchedules = Schedule()
        newSchedules = Schedule()
        
        oldClips = []
        newClips = []
        
        newMatch = nil
        oldMatch = nil
        
    }
    
    
    // ----- DATA GATHERING
    
    func getMatchNotifications(_ cup:Int, season: Int, matchID: Int) -> [EventModel]{
        if(newNotifications.count > 0){
            return newNotifications
        }else{
            return self.getNotifications(cup, season: season, matchID: matchID)
        }
    }
    
    func getMatchLineups(_ cup:Int, season:Int, matchId: Int) -> LineupModel{
        if(newLineups.Homes != nil && newLineups.Aways != nil && newLineups.Homes!.count > 0 && newLineups.Aways!.count > 0){
            return newLineups
        }else{
            return self.getLineups(cup, season: season, matchId: matchId)
        }
    }
    
    func getMatchClips(_ cup:Int, season:Int, matchId: Int) -> [ClipModel]{
        if(newClips.count > 0){
            return newClips
        }else{
            return self.getClips(cup, season: season, matchId: matchId)
        }
    }
    
    func getMatches(cupcode:String, season:Int, actualDate: Date) -> Schedule{
        var _sched: Schedule?
        if newSchedules.Matches.count > 0 {
            _sched = newSchedules
        }else{
            _sched =  self.getSchedule(cupcode: cupcode, season: season)
        }
        
        let datematchstr: String = Utility.setDateValue(actualDate)
        
        _sched!.Matches = self.filterMatchesByDate(datematchstr, matches: _sched!.Matches)
        
        return _sched!
    }
    
    func getMatchData(_ cup: Int, season: Int, matchId: Int) -> Match?{
        if(newMatch != nil){
            return newMatch
        }else{
            return self.getMatch(cup, season: season, matchId: matchId)
        }
    }
    
    func getMatchTranslation(_ cup: Int, season: Int) -> Translations{
        let key: String = String(cup) + "_" + String(season)
        if(matchTranslation[key] == nil){
            matchTranslation[key] = getTranslation(cup, season: season)
        }
        
        return matchTranslation[key]!
    }
    
    
    //MARK:-
    //MARK: DATA CALLS
    
    
    fileprivate func getNotifications(_ cup:Int, season: Int, matchID: Int) -> [EventModel]{
        var _notificationList: [EventModel] = []
        let eventController = EventController(urlFeedEvents: self.feedEventsNGS, translations: self.getMatchTranslation(cup, season: season))
        _notificationList = eventController.getNotifications(matchID)
        matchNotifications[matchID] = _notificationList
        
        
        let lockQueue = DispatchQueue(label: "com.deltatre.lockqueue.notifications", attributes: [])
        lockQueue.sync {
            oldNotifications = newNotifications
            newNotifications = _notificationList
        }
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "notificationsDataReady"), object: nil))
        print(" --- > Notifications matches data updated, match id \(matchID), found items: \(newNotifications.count)")
        return _notificationList
    }
    
    fileprivate func getClips(_ cup:Int, season:Int, matchId: Int) -> [ClipModel]{
        
        var _clipList:[ClipModel] = []
        let clipController = ClipController(mainfeedUrl: self.feedClips, events: self.getMatchNotifications(cup, season: season, matchID: matchId), cameras: self.cameraTraslation)
        _clipList = clipController.get(cup, matchId: matchId)
        matchClips[matchId] = _clipList
        
        let lockQueue = DispatchQueue(label: "com.deltatre.lockqueue.clips", attributes: [])
        lockQueue.sync {
            oldClips    = newClips
            newClips = _clipList
        }
        
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "multicamDataReady"), object: nil))
        print(" --- > Clips data updated, match id \(matchId), found items: \(newClips.count)")
        return _clipList
        
    }
    
    fileprivate func getLineups(_ cup:Int, season:Int, matchId: Int) -> LineupModel{
        var lineupsObj = LineupModel()
        let playerPhotoURL = self.playerThumbBaseUrl.replacingOccurrences(of: "{CUP}", with: String(cup)).replacingOccurrences(of: "{SEASON}", with: String(season))
        
        let controller = LineupController(_feedlineupUrl: self.feedLineups, _playerThumbBaseUrl: playerPhotoURL,_teamLogoBaseUrl: self.teamLogoBaseUrl)
        
        lineupsObj = controller.get(matchId)
        matchLineups[matchId] = lineupsObj
        
        let lockQueue = DispatchQueue(label: "com.deltatre.lockqueue.lineups", attributes: [])
        lockQueue.sync {
            oldLineups = newLineups
            newLineups = lineupsObj
        }
        
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "lineupsDataReady"), object: nil))
        print(" --- > Lineups data updated, match id \(matchId)")
        return lineupsObj
    }
    
    
    func getSchedule(cupcode:String, season:Int) -> Schedule{
        
        var schedule = Schedule()
        
        if let matchDayScheduleUrl = URL(string: self.feedFullMatchList.replacingOccurrences(of: "{CUPCODE}", with: String(cupcode)).replacingOccurrences(of: "{SEASON}", with: String(season))){
            feedMatch = self.feedMatch.replacingOccurrences(of: "{CUPCODE}", with: String(cupcode)).replacingOccurrences(of: "{SEASON}", with: String(season))
            
            let jsonData = try? Data(contentsOf: matchDayScheduleUrl)
            if (jsonData?.count > 0) {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments)
                    schedule = Schedule.deserialize((json as? Dictionary<String, AnyObject>)!, feedMatch: feedMatch)
                    
                    let lockQueue = DispatchQueue(label: "com.deltatre.lockqueue.schedule", attributes: [])
                    lockQueue.sync {
                        oldSchedules = newSchedules
                        newSchedules = schedule
                    }
                    
                    let key: String = String(cupcode) + "_" + String(season)
                    schedules[key] = schedule
                    
                }
                catch {
                    NSLog("error serializing JSON: \(error)")
                }
                
            }
            
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "matchesDataReady"), object: nil))
            print(" --- > Schedule matches data updated")
        }
        return schedule
    }
    
    func getMatch(_ cup:Int, season:Int, matchId: Int) -> Match? {
        
        var match: Match?
        
        let matchUrl = URL(string: self.feedMatch.replacingOccurrences(of: "{CUP}", with: String(cup)).replacingOccurrences(of: "{SEASON}", with: String(season)).replacingOccurrences(of: "{MATCHID}", with: String(matchId)))!
        do {
            let jsonString = try String(contentsOf: matchUrl)
            
            //jsonString = jsonString.stringByReplacingOccurrencesOfString("{\"matchbox\":", withString: "")//.stringByReplacingOccurrencesOfString("}]}}", withString: "}]}")
            
            //jsonString = jsonString.substringToIndex(jsonString.endIndex.advancedBy(-1))
            //jsonString = "{" + jsonString + "}"
            let jsonData = jsonString.data(using: String.Encoding.utf8)
            if (jsonData?.count > 0) {
                
                let json = try JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as? [String: Any]
                if let matchbox = json?["matchbox"] as? [String: AnyObject]{
                    var competition: [String: AnyObject] = [:]
                    if let comp = json?["competition"] as? [String: AnyObject]{
                        competition = comp
                    }
                    match = Match.deserialize(competition: competition,matchInfo: matchbox)
                    if let videos = json?["MatchVideos"] as? [String: AnyObject]{
                        match!.Videos = MatchVideos.deserialize(videos)
                        let lockQueue = DispatchQueue(label: "com.deltatre.lockqueue.match", attributes: [])
                        lockQueue.sync {
                            oldMatch = newMatch
                            newMatch = match
                        }
                    }
                }
                
            }
            
        }catch {
            NSLog("error serializing JSON: \(error)")
        }
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "matchDataReady"), object: nil))
        print(" --- > Match data updated, match id \(matchId), found ")
        return match
    }
    
    func filterMatchesByDate(_ date: String, matches: [Match]) -> [Match]{
        var res: [Match] = []
        
        for match: Match in matches
        {
            
            let userCalendar = Calendar.current
            let hourMinuteComponents: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
            let datetime = (userCalendar as NSCalendar).components(
                hourMinuteComponents,
                from: match.DateCET as Date)
            
            let datematchstr: String = String(describing: datetime.year)+String(format: "%02d", datetime.month!)+String(format: "%02d", datetime.day!)
            if(date == datematchstr){
                res.append(match)
            }
        }
        return res
    }
    
    func getTranslation(_ cup: Int, season: Int) -> Translations
    {
        //if let url: String? = { //self.feedTranslations.replacingOccurrences(of: "{CUPID}", with: String(cup)).replacingOccurrences(of: "{SEASONID}", with: String(season)){
        var translationData = Translations()
        translationData = translationData.getTranslations("http://en.static.ngs.deltatre.net/feed/appletv/apps/hbs_demo/feeds/cup=101/season=2017/translations.json?ff")
        let key: String = String(cup) + "_" + String(season)
        matchTranslation[key] = translationData
        return translationData
        
        //}
    }
    
    
}
