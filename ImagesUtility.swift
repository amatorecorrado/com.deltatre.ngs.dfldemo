//
//  File.swift
//  AppleTV
//
//  Created by Corrado Amatore on 16/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

//public var _sharedInstance: ImagesUtility?

open class ImagesUtility{

    var forceDownload: Bool = false
    var useLocalStorage: Bool = false
    
//    struct Static {
//        static var token: dispatch_once_t = 0
//    }
    
//    public class var sharedInstance: ImagesUtility{
//        if (_sharedInstance == nil){
//            NSLog("error: shared called before setup")
//        }
//        return _sharedInstance!
//    }
    
//    class func setup(forceDownload: Bool = false, useLocalStorage: Bool = false){
//        if (_sharedInstance == nil){
//            dispatch_once(&Static.token) {
//                _sharedInstance = ImagesUtility(forceDownload: forceDownload, useLocalStorage: useLocalStorage)
//            
//            }
//        }
//    }
    
//    init(forceDownload: Bool = false, useLocalStorage: Bool = false) {
//        self.forceDownload = forceDownload
//        self.useLocalStorage = useLocalStorage
//    }
    static let sharedInstance = ImagesUtility()
    
    
    func getImage(_ urlString: String) -> UIImage?{
        let url = URL(string: urlString)
        
        let fileName: String = url!.lastPathComponent
        var image: UIImage?
        if(useLocalStorage){
            image = getStoredImage(fileName,url: url!)
        }
        
        if(forceDownload || image == nil){
            image = downloadImage(url!)
            if(image != nil && useLocalStorage){
                storeImage(image!, imageName: fileName)
            }
        }
        return image
    }
    
    func storeImage(_ image: UIImage, imageName: String){
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        print(paths)
        let imageData = UIImageJPEGRepresentation(image, 1.0)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
    }
    
    func getStoredImage(_ imageName: String, url: URL) -> UIImage?{
        var image: UIImage? = nil
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent(imageName)
        if fileManager.fileExists(atPath: imagePAth){
            image = UIImage(contentsOfFile: imagePAth)
        }else{
            image = downloadImage(url)
            //NSLog("No Image")
        }
        return image
    }
    
    fileprivate func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    fileprivate func downloadImage(_ urlString: String) -> UIImage?{
        let url = URL(string: urlString)
        let image = downloadImage(url!)
        return image
    }
    
    fileprivate func downloadImage(_ url: URL) -> UIImage?{
        var image: UIImage?
        let data = try? Data(contentsOf: url)
        if(data != nil){
            image = UIImage(data: data!)
        }
        return image
    }
}
