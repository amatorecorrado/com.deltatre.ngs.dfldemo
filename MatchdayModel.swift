//
//  MatchdayModel.swift
//  AppleTV
//
//  Created by Corrado Amatore on 21/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public struct MatchdayModel {
    
    var matchday_id: Int = 0
    var round_id: Int = 0 
    var round_code: String = ""
    var round_n: String = ""
    var phase: Int = 0
    var dates: [String] = []
    var date_string_formatted: String = ""
    var url: String = ""
    
}