//
//  HighlightImage.swift
//  AppleTV
//
//  Created by Corrado Amatore on 30/06/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

class HighlightImage: UIImageView
{
   
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        coordinator.addCoordinatedAnimations(
            {
                if self.isFocused
                {
                    UIView.animate(withDuration: 0.6 ,
                        animations: {
                            self.layer.shadowOffset = CGSize.zero
                            self.layer.shadowRadius = 7
                            self.layer.shadowOpacity = 0.6
                        })

                    
                }
                else
                {
                    UIView.animate(withDuration: 0.6, animations: {
                        self.layer.shadowOffset = CGSize.zero
                        self.layer.shadowRadius = 0
                        self.layer.shadowOpacity = 0
                    })
                }
            },
            completion: nil)
    }
}
