//
//  ClipController.swift
//  AppleTV
//
//  Created by Corrado Amatore on 14/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


open class ClipController{
    fileprivate var _mainfeedUrl: String = ""
    fileprivate var _events: [EventModel] = []
    fileprivate var _cameras: [CameraModel] = []
    open var isMatchEvent = true
    
    public init(){}
    
    public init(mainfeedUrl: String){
        _mainfeedUrl = mainfeedUrl
    }
    
    public init(mainfeedUrl: String, events: [EventModel], cameras: [CameraModel] ){
        _mainfeedUrl = mainfeedUrl
        _events = events
        _cameras = cameras
    }
    
    open func get(_ cup:Int, matchId: Int) -> ([ClipModel]) {

        var clips: [ClipModel] = []
        var clip:ClipModel = ClipModel()
        
        let feedUrl = _mainfeedUrl.replacingOccurrences(of: "{CUP}", with: String(cup)).replacingOccurrences(of: "{MATCHID}", with: String(matchId))
        
        let feedNSUrl = URL(string: feedUrl)
        
        let jsonData = try? Data(contentsOf: feedNSUrl!)
        if (jsonData?.count > 0) {
            do {
                let json = try  JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as? [String: Any]
                for clipGroups in (json?["ClipGroups"] as? [Dictionary<String, AnyObject>])! {
                    clip = ClipModel()
                    if let val = clipGroups["ID"] as? String {
                        clip.ID = val
                    }
                    if let val = clipGroups["Title"] as? String {
                        clip.Title = val
                    }
                    if let val = clipGroups["minute"] as? String {
                        clip.minute = val
                    }
                    if let val = clipGroups["timeCodeIn"] as? String {
                        clip.timeCodeIn = Date.init(dateTimeString: val)
                    }
                    if let val = clipGroups["ThumbnailUrl"] as? String {
                        clip.ThumbnailUrl = val
                    }
                    if let val = clipGroups["Url"] as? String {
                        clip.jsonDetailUrl = val
                    }
                    
                    if(isMatchEvent && clip.minute != nil ){
                        
                        getClipDetails(&clip)
                        NSLog("Log Detail Clip: \(clip.eventID)")
                        if(clip.Videos.count > 0){
                            clips.append(clip)
                        }
                    }else if(!isMatchEvent && clip.Videos.count > 0){
                        getClipDetails(&clip)
                        if(clip.Videos.count > 0){
                            clips.append(clip)
                        }
                    }
                }

            } catch {
                NSLog("error serializing JSON: \(error)")
            }
        }
        return clips
    }
    
    func getClipDetails(_ clip: inout ClipModel){
        let feedUrl = clip.jsonDetailUrl
        let feedNSUrl = URL(string: feedUrl!)
        
        var videos: [VideoClipModel] = []
        var video:VideoClipModel = VideoClipModel()
        
        let jsonData = try? Data(contentsOf: feedNSUrl!)
        if (jsonData?.count > 0) {
            do {
                let json = try  JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as? [String: Any]
                if let jsonclipDet = json?["ClipGroup"] as? [String: AnyObject] {
                    if let jsonbinding = jsonclipDet["Binding"] as? [String: AnyObject] {
                        if let val = jsonbinding["competitionID"] as? Int {
                            clip.competitionID = val
                        }
                        if let val = jsonbinding["competitionTypeID"] as? Int {
                            clip.competitionTypeID = val
                        }
                        if let val = jsonbinding["seasonID"] as? Int {
                            clip.seasonID = val
                        }
                        if let val = jsonbinding["roundID"] as? Int {
                            clip.roundID = val
                        }
                        if let val = jsonbinding["matchdayID"] as? Int {
                            clip.matchdayID = val
                        }
                        if let val = jsonbinding["seasonID"] as? Int {
                            clip.seasonID = val
                        }
                        if let val = jsonbinding["matchID"] as? Int {
                            clip.matchID = val
                        }
                        if let val = jsonbinding["eventID"] as? String {
                            clip.eventID = val
                            if(_events.count>0 && val.characters.count > 0){
                                clip.Event =  EventController.GetEventById(Int(val)!,events: _events)!
                            }
                        }
                    }
                    if let val = jsonclipDet["Phase"] as? String {
                        clip.Phase = val
                    }
                    for vids in (jsonclipDet["Videos"] as? [Dictionary<String, AnyObject>])! {
                        video = VideoClipModel()
                        
                        if let val = vids["UrlMP4"] as? String {
                            video.UrlMP4 = val
                        }
                        if let val = vids["UrlHLS"] as? String {
                            video.UrlHLS = val
                        }
                        if let val = vids["ID"] as? String {
                            video.ID = val
                        }
                        if let val = vids["operation"] as? String {
                            video.operation = val
                        }
                        if let val = vids["version"] as? String {
                            video.version = val
                        }
                        if let val = vids["VideoFeed"] as? String {
                            video.VideoFeed = val
                            
                            video.Camera = CameraController.getCameraFromCode(val, cameras: _cameras)
                        }
                        if let val = vids["Target"] as? String {
                            video.Target = val
                        }
                        if let val = vids["Duration"] as? String {
                            video.Duration = val
                        }
                        if let val = vids["Codec"] as? String {
                            video.Codec = val
                        }
                        if let val = vids["Bitrate"] as? String {
                            video.Bitrate = val
                        }
                        if let val = vids["Language"] as? String {
                            video.Language = val
                        }
                        if let val = vids["ThumbnailUrl"] as? String {
                            video.ThumbnailUrl = val
                        }
                        
                        videos.append(video)
                    }
                    
                    clip.Videos = videos

                }
                
            } catch {
                NSLog("error serializing JSON: \(error)")
            }
        }
        
    }
    open func getFromID(_ id: String, clips: [ClipModel]) -> ClipModel?{
        for clip in clips{
            if(clip.ID == id){
                return clip
            }
        }
        return nil
    }
    
    open func getClipVideoFromCameraID(_ cameraid: String, clips: [ClipModel])->VideoClipModel?
    {
        for clip in clips{
            for vid in clip.Videos{
                if vid.ID == cameraid{
                    return vid
                }
            }
        }
        return nil
    }
}
