
//
//  Video.swift
//  AppleTV
//
//  Created by Corrado Amatore on 30/06/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


public struct VideoController
{
    fileprivate var feedVideoList: String
    fileprivate var feedMatch: String
    fileprivate var feedVideoData: String
    
    fileprivate var cupCode: String
    fileprivate var season: String
    
    public init(_urlVideoList: String, _urlMatch: String, _feedVideoData: String, _cupCode: String, _season: String) {
        self.feedVideoList = _urlVideoList
        self.feedMatch = _urlMatch
        self.feedVideoData = _feedVideoData
        self.cupCode = _cupCode
        self.season = _season
        
    }
    
    public func getVideoList(maxItems: Int?) -> ([VideoModel]) {
        //var videoHiglightsMatchDictionary: Dictionary<String,String> = [:]
        var results: [VideoModel] = []
        var video: VideoModel
        var jsonurl = URL(string: "")
        var jsonurlmatch = URL(string: "")
        
        var urlFeed = feedVideoList.replacingOccurrences(of: "{CUPCODE}", with: cupCode).replacingOccurrences(of: "{SEASON}", with: season).replacingOccurrences(of: "|", with: "|".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)

        jsonurl = URL(string: urlFeed)
        let jsonData = try? Data(contentsOf: jsonurl!)
        
        if (jsonData?.count > 0) {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments)  as? [String: Any]
                    if let modules = json!["modules"] as? [String: AnyObject]{
                        if let videos = modules["videos"] as? Array<AnyObject>{
                            for v in videos {
                                
                                video = VideoModel()
                                
                                if let val = v["vId"] as? String{
                                    video.video_id = val
                                    let videoidstr = String(val)
                                    video.videoData = VideoDataController().getVideoData(feedVideoData, videoId: videoidstr)
                                }
                                //if let val = videos["video_date"] as? String {
                                //video.video_date = NSDate.init(dateTimeString: val)
                                //}
                                if let val = v["matchday"] as? Int {
                                    video.matchday = val
                                }
                                if let val = v["title"] as? String {
                                    video.video_title = val
                                }
                                if let val = v["desc"] as? String {
                                    video.video_descr = val
                                }
                                if let val = v["thumb"] as? String {
                                    video.video_thumb = val
                                }
                                if let val = v["thumbSmall"] as? String {
                                    video.video_thumb_small = val
                                }
                                if let val = v["thumbLarge"] as? String {
                                    video.video_thumb_large = val
                                }
                                
                                if let val = v["mId"] as? String {
                                    video.match_id = val
//                                    let matchidstr = val
//                                    if let urlmat: String? = feedMatch.replacingOccurrences(of: "{MATCHID}", with: matchidstr){
//                                        jsonurlmatch = URL(string: urlmat!)!
//                                    }
//                                    let jsonDataMatch = try? Data(contentsOf: jsonurlmatch!)
//                                    if (jsonDataMatch?.count > 0) {
//                                        let jsonMatch = try JSONSerialization.jsonObject(with: jsonDataMatch!, options: .allowFragments) as? [String: Any]
//                                        if let matchbox = jsonMatch!["matchbox"] as? [String: AnyObject]{
//                                            var competition: [String: AnyObject] = [:]
//                                            if let comp = jsonMatch!["competition"] as? [String: AnyObject]{
//                                                competition = comp
//                                            }
//                                            video.match = Match.deserialize(competition: competition, matchInfo: matchbox)
//                                        }
//                                    }
                                }
                                
                                
                                
                                if(results.count == maxItems){
                                    return results
                                }
                                
                                results.append(video)
                            }
                            
                        }
                    }
                    
                    
                } catch {
                    NSLog("error serializing JSON \(error)")
                }
            
        }
        return results
    }
}

