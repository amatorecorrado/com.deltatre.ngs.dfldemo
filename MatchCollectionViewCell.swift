//
//  MatchCollectionViewCell.swift
//  AppleTV
//
//  Created by Corrado Amatore on 14/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit

class MatchCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var homeTeamLabel: UILabel!
    @IBOutlet weak var awayTeamLabel: UILabel!
    @IBOutlet weak var backCellImage: UIImageView!
    @IBOutlet weak var timeScoreLabel: UILabel!
    @IBOutlet weak var homeTeamImage: UIImageView!
    @IBOutlet weak var awayTeamImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var selectionPercentageShadow = 5
    var selectionPercentageScale = 5
    var selectionMotionEffectAmount = 10
    
    //PARAMETERS
    var matchId: Int?
    var enable: Bool = true
    
    fileprivate var group = UIMotionEffectGroup()
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        coordinator.addCoordinatedAnimations( {
            if self.isFocused {
                self.layer.zPosition = 1
                let scale = 1 + (CGFloat(0.01) * CGFloat(self.selectionPercentageScale))
                let shadow = CGFloat(0.01) * CGFloat(self.selectionPercentageShadow)
                
                self.transform = CGAffineTransform(scaleX: scale, y: scale)
                self.layer.shadowOffset = CGSize(width: 0,height: (self.frame.height * shadow))
                self.layer.shadowRadius = 7
                self.layer.shadowOpacity = 0.7
                
                let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
                horizontal.minimumRelativeValue = -self.selectionMotionEffectAmount
                horizontal.maximumRelativeValue = self.selectionMotionEffectAmount
                
                let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
                vertical.minimumRelativeValue = -self.selectionMotionEffectAmount
                vertical.maximumRelativeValue = self.selectionMotionEffectAmount
                
                self.group.motionEffects = [horizontal, vertical]
                self.addMotionEffect(self.group)
            }
            else {
                self.layer.zPosition = 0
                self.transform = CGAffineTransform.identity
                self.layer.shadowOffset = CGSize.zero
                self.layer.shadowRadius = 0
                self.layer.shadowOpacity = 0
                
                self.removeMotionEffect(self.group)
            }
            },
        completion: nil)

    }
    
    fileprivate var _selected: Bool?
    override var isSelected: Bool{
        set {
            _selected = newValue
            
            if(_selected == true && enable == true){
                self.spinner.startAnimating()
            }else{
                self.spinner.stopAnimating()
            }
        }
        get{
            return _selected!
        }
    }
    
    fileprivate var _highlighted: Bool?
    override var isHighlighted: Bool{
        set {
            _highlighted = newValue
            
            if(_highlighted == true && enable == true){
                self.spinner.startAnimating()
            }
        }
        get{
            return _highlighted!
        }
    }
    
    static func drawMatchStatus(_ cell: MatchCollectionViewCell,item: Match, videoDataUrl: String, skin: String){
        cell.isUserInteractionEnabled = true
        cell.statusLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        cell.statusLabel.layer.cornerRadius = 10
        cell.statusLabel.layer.masksToBounds = true
        cell.statusLabel.backgroundColor = nil
      
        let status: Match.Status = item.getStatus(videoDataUrl: videoDataUrl)
        cell.enable = true
        if(status == Match.Status.live){
            cell.statusLabel.alpha = 1
            cell.statusLabel.text = "LIVE"
            cell.statusLabel.backgroundColor = UIColor.red
            cell.timeScoreLabel.text = String(item.Results.HomeGoals) + String("-")+String(item.Results.AwayGoals)
        }else if(status == Match.Status.replay){
            cell.statusLabel.alpha = 1
            cell.statusLabel.text = "REPLAY"
            cell.timeScoreLabel.text = String(item.Results.HomeGoals) + String("-")+String(item.Results.AwayGoals)
        }else if(status == Match.Status.future){
            cell.statusLabel.alpha = 0
            cell.enable = false
        }
    }

    
}
