//
//  UserExperienceUtility.swift
//  AppleTV
//
//  Created by Corrado Amatore on 18/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

open class UserExperienceUtility{
    
    fileprivate static func animateHorizontally(_ view: UIView, dimension: CGFloat, duration: Double, delay: Double){
        let actualOrigin = view.frame.origin.x
        
        view.frame.origin.x = actualOrigin + dimension
        
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseOut, animations: {
            view.frame.origin.x = actualOrigin
            }, completion: nil)
    }
    
    fileprivate static func animateVertically(_ view: UIView, dimension: CGFloat, duration: Double, delay: Double){
        let actualOrigin = view.frame.origin.y
        
        view.frame.origin.y = actualOrigin + dimension
        
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseOut, animations: {
            view.frame.origin.y = actualOrigin
            }, completion: nil)
    }

    
    static func animateSubviewsHorizontally(_ view: UIView, dimension: CGFloat, duration: Double){
    
        var k = 0
        for item in view.subviews {
            
            self.animateHorizontally(item, dimension: dimension, duration: duration, delay: Double(Double(k * 1) / 10))
            
            k += 1
        }
    }
    
    static func animateSubviewsVertically(_ view: UIView, dimension: CGFloat, duration: Double){
        
        var k = 0
        for item in view.subviews {
            
            self.animateVertically(item, dimension: dimension, duration: duration, delay: Double(Double(k * 1) / 10))
            
            k += 1
        }
    }
    
    static func fadeInSubviews(_ view: UIView, duration: Double){
        
        var k = 0
        for item in view.subviews {
            
            let actualValue = item.alpha
            
            item.alpha = 0
            
            UIView.animate(withDuration: duration, delay: 0.1 * Double(k), options: UIViewAnimationOptions.curveEaseOut, animations: {
                item.alpha = actualValue
                }, completion: nil)
            
            k += 1
        }
    }
    
}

