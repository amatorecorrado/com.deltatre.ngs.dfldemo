//
//  LineupController.swift
//  AppleTV
//
//  Created by Corrado Amatore on 04/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



public struct LineupController {
    
    fileprivate var feedlineup: String
    fileprivate var playerThumbBaseUrl: String
    fileprivate var teamLogoBaseUrl: String
    
    public init(_feedlineupUrl: String, _playerThumbBaseUrl: String, _teamLogoBaseUrl: String){
        feedlineup = _feedlineupUrl
        playerThumbBaseUrl = _playerThumbBaseUrl
        teamLogoBaseUrl = _teamLogoBaseUrl
    }
    
    public func get(_ matchId: Int) -> (LineupModel) {
        var results = LineupModel()
        var homes: [PlayerModel] = []
        var aways: [PlayerModel] = []
        
        var player = PlayerModel()
        let matchIdStr = String(matchId)
        let feedUrl = feedlineup.replacingOccurrences(of: "{MATCHID}", with: matchIdStr)
        
        if let encodedString = feedUrl.addingPercentEncoding(
            withAllowedCharacters: CharacterSet.urlFragmentAllowed),
            let feedNSUrl = URL(string: encodedString) {
        
        
        let jsonData = try? Data(contentsOf: feedNSUrl)
        if (jsonData?.count > 0) {
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as? [String: Any]
                
                //GET PLAYERS
                
                if let jsonLineups = json?["lineups"] as? [String: AnyObject] {
                    if let homeTeam = jsonLineups["home_team"] as? [String: AnyObject] {
                        
                        player = createTeam(homeTeam)
                        homes.append(player)
                        
                        for homeTeamPitch in (homeTeam["players"] as? [Dictionary<String, AnyObject>])! {
                            
                            player = createPlayer(homeTeamPitch)
                            homes.append(player)
                        }
                        
                    }
                    
                    if let awayTeam = jsonLineups["away_team"] as? [String: AnyObject] {
                        
                        player = createTeam(awayTeam)
                        aways.append(player)
                        
                        for awayTeamPitch in (awayTeam["players"] as? [Dictionary<String, AnyObject>])! {
                            
                            player = createPlayer(awayTeamPitch)
                            aways.append(player)
                        }
                    }
                }
                
                results.Homes = homes
                results.Aways = aways
                
                //GET EVENTS
                var eventObj: EventModel
                var eventArray: [EventModel]
                
                if let jsonPlayers = json?["eventsforplayer"] as? [String: AnyObject] {
                    for jplayer in jsonPlayers{
                        
                        eventArray = []
                        
                        let playerID = Int(jplayer.0)
                        let playerObj = getPlayer(playerID!, lineups: results)
                        
                        let jevent = jplayer.1
                        let jevents = jevent as! [[String : AnyObject]]
                        
                        for eve in jevents{
                            eventObj = EventModel()
                            
                            if let val = eve["id"] as? Int {
                                eventObj.id = val
                            }
                            if let val = eve["descr"] as? String {
                                eventObj.descr = val
                            }
                            if let val = eve["code"] as? Int {
                                eventObj.code = val
                            }
                            if let val = eve["minute"] as? Int {
                                eventObj.minute = val
                            }
                            if let val = eve["playerFrom"] as? Int {
                                eventObj.playerFrom = val
                            }
                            if let val = eve["playerTo"] as? Int {
                                eventObj.playerTo = val
                            }
                            if let val = eve["subCode"] as? Int {
                                eventObj.subCode = val
                            }
                            if let val = eve["type"] as? Int {
                                eventObj.type = val
                            }
                            if let val = eve["tag"] as? String {
                                eventObj.tag = val
                            }
                            if let val = eve["time"] as? String {
                                eventObj.time = Date.init(dateTimeString: val)
                            }
                            if let val = eve["teamTo"] as? Int {
                                eventObj.teamTo = val
                            }
                            if let val = eve["teamFrom"]! as? Int {
                                eventObj.teamFrom = val
                            }
                            if let val = eve["phase"] as? Int {
                                eventObj.phase = val
                            }
                            if let val = eve["isValidated"] as? Bool {
                                eventObj.isValidated = val
                            }
                            if let val = eve["injuryMinute"] as? Int {
                                eventObj.injuryMinute = val
                            }
                            eventArray.append(eventObj)
                        }
                        playerObj?.events = eventArray
                    }
                }
                
            } catch {
                NSLog("error serializing JSON: \(error)")
            }
            }
        }
        
        return results
    }
    
    func getPlayer(_ playerID: Int, lineups: LineupModel) -> PlayerModel?{
        for h_player in lineups.Homes! {
            if(h_player.ID == playerID){
                return h_player
            }
        }
        for a_player in lineups.Aways! {
            if(a_player.ID == playerID){
                return a_player
            }
        }
        return nil
    }
    
    func createPlayer(_ playerObjJson: Dictionary<String, AnyObject>) -> PlayerModel{
        let player = PlayerModel()
        
        player.ID = playerObjJson["id"] as? Int
        player.BibNumber = playerObjJson["jerseyNumber"] as? Int
        player.OfficialName = playerObjJson["OfficialName"] as? String
        player.OfficialSurname = playerObjJson["OfficialSurname"] as? String
        player.Role = playerObjJson["role"] as? Int
        player.Type = playerObjJson["type"] as? String
        
        //player.ShortName = playerObjJson["ShortName"] as? String
        player.IsCaptain = playerObjJson["iscaptain"] as! Bool
        player.IsGoalkeeper = playerObjJson["isgk"] as! Bool
        
        if let thumb = playerObjJson["thumbs"] as? [String: AnyObject] {
            player.ThumbSmall = self.playerThumbBaseUrl + (thumb["thumb_small"] as? String)!
            player.ThumbMedium = self.playerThumbBaseUrl + (thumb["thumb_medium"] as? String)!
            player.ThumbLarge = self.playerThumbBaseUrl + (thumb["thumb_large"] as? String)!
        }
        
        //player.PhotoURL = self.playerThumbBaseUrl.stringByReplacingOccurrencesOfString("{PLAYERID}", withString: "\(player.ID!)")
        
        return player
    }
    
    func createTeam(_ teamObjJson: Dictionary<String, AnyObject>) -> PlayerModel{
        let team = PlayerModel()
        team.ID = teamObjJson["team_id"] as? Int
        team.OfficialSurname = teamObjJson["team_n"] as? String
        team.ThumbLarge = teamLogoBaseUrl.replacingOccurrences(of: "{TEAMID}", with: "\(team.ID!)")
        team.IsTeam = true
        return team
    }
}
