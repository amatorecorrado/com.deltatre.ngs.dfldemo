//
//  PlayByPlayModel.swift
//  AppleTV
//
//  Created by administrator on 05/07/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation


// Prendo oggetto Notification  applico le traduzioni e ritorno il feed finale


public struct PlayByPlayController {

    
    public var _translation: Translations?

    public func MapFromNotifications(_ notifications: [EventModel]) -> ([NotificationModel]){
        let results: [NotificationModel] = []
        
        return results
    }
    mutating public func deserialize(_ matchId: Int, translations: Translations, urlFeedEvents: String) -> ([NotificationModel]) {
        
        var results: [NotificationModel] = []
        
        let tmpNotification: EventController = EventController(urlFeedEvents: urlFeedEvents, translations: translations)
        var tmpNotifications:  [EventModel] = []
        _translation  = translations

        
        tmpNotifications = tmpNotification.getNotifications(matchId)
        
        var tmpPlayByPlayItemModel: NotificationModel = NotificationModel()
        
        for item in tmpNotifications {
            tmpPlayByPlayItemModel = NotificationModel()
            
            
            if let val = item.id?.description as String! {
                tmpPlayByPlayItemModel.Id = val
            }
            if let val = item.code?.description as String! {
                tmpPlayByPlayItemModel.EventTypeId = val
            }
            if let val = item.descr as String! {
                tmpPlayByPlayItemModel.EventType = val
                tmpPlayByPlayItemModel.EventTypeName = val
            }
            if let val = item.minute?.description as String! {
                tmpPlayByPlayItemModel.GameTime = val
            }
            var tmpTextTranslate = _translation!.generictags[item.tag!]
            
            if tmpTextTranslate != nil && tmpTextTranslate!.range(of: "@@player1") != nil && item.playerFrom != nil {
                
                let tmpPlayerToTranslate = _translation!.players[item.playerFrom!.description]
                
                if(tmpPlayerToTranslate != nil){
                tmpTextTranslate = tmpTextTranslate!.replacingOccurrences(of: "@@player1", with: (tmpPlayerToTranslate?.webname)!)
                }
            }

            if tmpTextTranslate != nil && tmpTextTranslate!.range(of: "@@player2") != nil {
                
                let tmpPlayerFromTranslate = _translation!.players[item.playerTo!.description]
                
                if(tmpPlayerFromTranslate != nil){
                tmpTextTranslate = (tmpTextTranslate!.replacingOccurrences(of: "@@player2", with: (tmpPlayerFromTranslate?.webname)!))
                }
            }
            if tmpTextTranslate != nil && tmpTextTranslate!.range(of: "@@team1") != nil {
                
                let tmpTeamFromTranslate = _translation!.teams[item.teamFrom!.description]
                
                if(tmpTeamFromTranslate != nil){

                    tmpTextTranslate = (tmpTextTranslate!.replacingOccurrences(of: "@@team1", with: (tmpTeamFromTranslate?.name)!))
                }
            }
            if tmpTextTranslate != nil && tmpTextTranslate!.range(of: "@@team2") != nil {
                let tmpTeamToTranslate = _translation!.teams[item.teamFrom!.description]
                
                if(tmpTeamToTranslate != nil){
                    tmpTextTranslate = (tmpTextTranslate!.replacingOccurrences(of: "@@team2", with: (tmpTeamToTranslate?.name)!))
                }
            }
            if tmpTextTranslate != nil && tmpTextTranslate!.range(of: "@@teamID") != nil {
                
                let tmpTeamFromTranslate = _translation!.teams[item.teamFrom!.description]
                
                if(tmpTeamFromTranslate != nil){
                tmpTextTranslate = (tmpTextTranslate!.replacingOccurrences(of: "@@teamID", with: (tmpTeamFromTranslate?.name)!))
                }
            }

            if tmpTextTranslate != nil && tmpTextTranslate != "" {
                    tmpPlayByPlayItemModel.Text = tmpTextTranslate!
            }else{
                tmpPlayByPlayItemModel.Text = item.descr!
            }

            
            
            if let val = item.time.description as String! {
                tmpPlayByPlayItemModel.TimeCode = val
            }
            
            
        results.append(tmpPlayByPlayItemModel)
        }
        
        return results
    }

}

