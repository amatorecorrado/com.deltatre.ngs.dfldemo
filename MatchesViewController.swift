////
//  MatchesViewController.swift
//  AppleTV
//
//  Created by Corrado Amatore on 14/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class MatchesViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var calendarScrollView: UIScrollView!
    @IBOutlet weak var matchesCollectionView: UICollectionView!
    @IBOutlet weak var backMatchesImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var spinnerMatches: UIActivityIndicatorView!
    
    //var appInitType = AppInitType()
    var schedule = Schedule()
    //var appInit: AppInit? = nil
    
    var cup: Int = 0
    var cupSeason: Int = 0
    var season: Int = 0
    var roundId: Int = 0
    var matchDay: Int = 0
    var session: Int = 0
    var matchId: Int = 0
    var offset = 0
    var matchDayScheduleUrl = URL(string: "")
    var teamLogoBaseUrl = ""
    var matchdays: [MatchdayModel] = []
    var isFirstLoading = true
    var isRefresh = false
    var selectedX = 0
    var cupInfoUrl = URL(string: "")
    var translationData: Translations?
    
    var selectedbutton: AdvancedButton? = nil
    var selectedMD = 0
    var selectedSession = 0
    var selectedRoundName: String = ""
    fileprivate var selectedDate: String? = nil
    var cellViewSize: CGSize?
    
    var repeatingTimer = RepeatingTimer(intervalSeconds: 60)
    
    override var preferredFocusedView: UIView?{
        return self.selectedbutton
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinnerMatches.hidesWhenStopped = true
        // Do any additional setup after loading the view.
        spinnerMatches.alpha = 1
        spinnerMatches.startAnimating()
        load()
        let nibCell = UINib(nibName: "MatchCollectionViewCell",bundle: nil)
        matchesCollectionView.register(nibCell, forCellWithReuseIdentifier: "matchCell")
        
        cellViewSize = (Bundle.main.loadNibNamed("MatchCollectionViewCell", owner: self, options: nil)?.first as AnyObject).size
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if self.matchesCollectionView.indexPathsForSelectedItems?.count > 0 {
            self.matchesCollectionView.deselectItem(at: (self.matchesCollectionView.indexPathsForSelectedItems?.first)!, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    override weak var preferredFocusedView: UIView? {
//        
//        return selectedbutton
//        
//    }
    
    func getMatches(round: Int, matchday: Int) {
        DispatchQueue.global(qos: .background).async(execute: {
            
            DataProvider.sharedInstance.setParameters(self.viewConfiguration!)
            var params = self.viewConfiguration?.getParameters()
            self.schedule = DataProvider.sharedInstance.getSchedule(cupcode: (params?.CupCode)!, season: Int((params?.Season)!)!)
            
            if(self.selectedDate != nil){
            
                self.schedule.Matches = self.filterMatchesByDate(self.selectedDate!, matches: self.schedule.Matches)
            }
            
            DispatchQueue.main.sync(execute: {
                UIView.animate(withDuration: 0, animations: {
                    self.spinnerMatches.stopAnimating()
                    self.matchesCollectionView.reloadData()
                    NSLog("dispatched getMatches")
                    }, completion: { (finish) in
                        if finish == true{
                            self.isRefresh = false
                        }
                })
            })
            
            
            
        })
    }
    
    
    
    func getCalendar(){
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async(execute: {
            
            let controller = CalendarController(feedUrl: self.viewConfiguration!.feeds["feedMatchdays"]!, cup: self.viewConfiguration!.parameters["cup"]!, season: self.viewConfiguration!.parameters["season"]!)
            let _matchdays = controller.get()
            
            
            DispatchQueue.main.sync(execute: {
                
                //                _ = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(self.drawCalendar), userInfo: nil, repeats: false)
                self.matchdays = _matchdays
                self.drawCalendar()
                
                NSLog("dispatched getCalendar")
            })
        })
        
        
    }
    
    func getCupInfo() {
        let jsonData = try? Data(contentsOf: self.cupInfoUrl!)
        
        if (jsonData?.count > 0) {
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments)
                Utility.deserializeCupInfo((json as? Array<Dictionary<String, AnyObject>>)!, cup: cup, cupSeason: cupSeason, season: season, roundId: &roundId, matchDay: &matchDay, session: &session)
            } catch {
                NSLog("error serializing JSON: \(error)")
            }
        }
    }
    
    func getTranslation()
    {
        if let url: String? = self.viewConfiguration!.feeds["feedTranslations"]!.replacingOccurrences(of: "{CUPID}", with: String(self.cup)).replacingOccurrences(of: "{SEASONID}", with: String(self.season)){
            translationData = Translations()
            translationData = translationData!.getTranslations(url!)
        }
    }
    
    func load() {
        cup = Int(self.viewConfiguration!.parameters["cup"]!)!
        cupSeason = Int(self.viewConfiguration!.parameters["cupSeason"]!)!
        season = Int(self.viewConfiguration!.parameters["season"]!)!
        
        cupInfoUrl = URL(string: self.viewConfiguration!.feeds["feedCupConfiguration"]!)!
        teamLogoBaseUrl = self.viewConfiguration!.images.getValue("teamLogo")
        
        titleLabel.text = ""
        subtitleLabel.text = ""
        if self.viewConfiguration!.parameters["skin"] != nil && (self.viewConfiguration!.parameters["skin"]! == "light" || self.viewConfiguration!.parameters["skin"]! == "white") {
            titleLabel.textColor = UIColor.white
            subtitleLabel.textColor = UIColor.white
        }
        
        getCupInfo()
        
        getTranslation()
        
        getCalendar()
        
        DispatchQueue.main.async {
            self.repeatingTimer.timer.setEventHandler(handler: { [weak self] in
                self?.getMatches(round: (self?.roundId)!, matchday: (self?.matchDay)!)
            })
            self.repeatingTimer.resume()
            
//            self.getmatchesTimer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(self.getMatches(_:)), userInfo: ["round" : String(self.roundId),"matchday" : String(self.matchDay), "date" : ""], repeats: true)
//            self.getmatchesTimer!.fire()
        }
        
        //var backgroundImage = appInit?.Images.getImage(appInitType.Background)
        var backgroundImage = ImagesUtility.sharedInstance.getImage(self.viewConfiguration!.images.getValue("background"))
        if backgroundImage == nil {
            backgroundImage = Utility.GetCachedImage("app_background_matchesview", url: (self.viewConfiguration?.images.getValue("background"))!)
        }
        backMatchesImage.image = backgroundImage
        
        
        //self.view.addSubview(initView)
        //splashImageView!.removeFromSuperview()
    }
    
    
    func drawCalendar() {
        
        for md:MatchdayModel in matchdays{
            if(!md.round_code.contains("Q")){
                
                drawDate(md)
            }
        }
        
        //calendarScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
        //        calendarScrollView.frame = CGRect(x: 410, y: 70, width: 1000, height: 110)
        let calculateWidth = CGFloat(integerLiteral: calendarScrollView.subviews.count) * 107
        calendarScrollView.contentSize = CGSize(width: calculateWidth, height: 110)
        
        if(isFirstLoading)
        {
            self.calendarScrollView.setContentOffset(CGPoint(x: selectedX, y: 0), animated: true)
            setNeedsFocusUpdate()
            updateFocusIfNeeded()
        }
        
        
    }
    
    @objc func handleDateButton(_ sender: AdvancedButton!) {
        NSLog("DateButton tag --> " + String(sender.tag))
        
        self.repeatingTimer.clean()
        
        isRefresh = true
        isFirstLoading = false
        
        self.schedule.Matches.removeAll()
        self.matchesCollectionView.reloadData()
        
        self.spinnerMatches.alpha = 1
        self.spinnerMatches.startAnimating()
        
        let selected = sender.IDString
        let selectedArr = selected.components(separatedBy: "_")
        let _roundid = selectedArr[0]
        let _matchday = selectedArr[1]
        let _date = selectedArr[2]
        let _session = selectedArr[3]
        selectedRoundName = selectedArr[4]
        
        selectedDate = _date
        selectedMD = Int(_matchday)!
        selectedSession = Int(_session)!
        
        setTitleAndSubTitle()
        
        //labelTitle.removeFromSuperview()
        //labelSubtitle.removeFromSuperview()
        //matchesPanel.removeFromSuperview()
        
        //getMatches(_roundid, matchday: _matchday, date: _date)
        //self.repeatingTimer.suspend()
        self.repeatingTimer.timer.setEventHandler(handler: { [weak self] in
            self?.getMatches(round: Int(_roundid)!, matchday: Int(_matchday)!)
        })
        self.repeatingTimer.resume()
//        getmatchesTimer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(self.getMatches(_:)), userInfo: ["round" : String(_roundid),"matchday" : String(_matchday), "date" : _date], repeats: true)
//        getmatchesTimer!.fire()
        
    }
    
    func setTitleAndSubTitle(){
        let dateTimeTmp = Date.init(dateTimeStringWithoutFormat: selectedDate!)
        
        titleLabel.text = Utility.setDateLabel(dateTimeTmp)
        
        subtitleLabel.text = selectedRoundName.capitalized + " - Matchday " + String(selectedMD)
    }
    
    func filterMatchesByDate(_ date: String, matches: [Match]) -> [Match]{
        var res: [Match] = []
        
        
        for match: Match in matches
        {
            
            let datematchstr: String = Utility.setDateValue(match.DateCET as Date)
            if(date == datematchstr){
                res.append(match)
            }
        }
        return res
    }
    
    func drawDate(_ item: MatchdayModel){
        var countSession = 1
        for date: String in item.dates{
            let buttonWidth =  100
            let buttonFrame = CGRect(x: offset, y: 0, width: buttonWidth, height: 90)
            
            
            let labelMainFrame = CGRect(x: 0, y: 10, width: buttonWidth, height: 50)
            let labelSecondaryFrame = CGRect(x: 0, y: 40, width: buttonWidth, height: 50)
            
            let button = AdvancedButton()
            button.frame = buttonFrame
            button.IDString = String(item.round_id) + "_" + String(item.matchday_id) + "_" + date + "_" + String(countSession) + "_" + String(item.round_n)
            button.tag = Int(date)!
            button.addTarget(self, action: #selector(handleDateButton), for: UIControlEvents.allEvents)
            button.backgroundColorSelected(UIColor.white,Alpha: (self.viewConfiguration!.parameters["skin"] != nil && self.viewConfiguration!.parameters["skin"]! == "light") ? 0.5 : 0.1)
            
            
            var labelColor = UIColor.init(red: 140.0/255.0, green: 183.0/255.0, blue: 222.0/255.0, alpha: 1)
            
            if (self.viewConfiguration!.parameters["skin"] != nil && self.viewConfiguration!.parameters["skin"]! == "light") {
                labelColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1)
            } else if (self.viewConfiguration!.parameters["skin"] != nil && self.viewConfiguration!.parameters["skin"]! == "white") {
                labelColor = UIColor.white
            }
            
            let dateTimeTmp = Date.init(dateTimeStringWithoutFormat: date)
            
            let userCalendar = Calendar.current
            let dateComponents: NSCalendar.Unit = [.year, .month, .day]
            let datetime = (userCalendar as NSCalendar).components(
                dateComponents,
                from: dateTimeTmp)
            
            let labelMain = UILabel(frame: labelMainFrame)
            let dates = String(format: "%02d", datetime.day!)
            labelMain.text = dates
            labelMain.font = UIFont.init(name: "Dosis-Medium", size: 35)
            labelMain.textAlignment = NSTextAlignment.center
            labelMain.textColor = labelColor
            labelMain.lineBreakMode = NSLineBreakMode.byClipping
            button.addSubview(labelMain)
            
            let labelSecondary = UILabel(frame: labelSecondaryFrame)
            
            let df: DateFormatter = DateFormatter()
            let monthName: String = df.monthSymbols[(datetime.month! - 1)].capitalized
            
            labelSecondary.text = monthName // "MD" + String(item.matchday_id) + " DAY" + String(countSession)
            
            labelSecondary.font = UIFont.init(name: "Dosis-Medium", size: 18)
            labelSecondary.textAlignment = NSTextAlignment.center
            labelSecondary.textColor = labelColor
            labelSecondary.lineBreakMode = NSLineBreakMode.byClipping
            
            if(isFirstLoading && matchDay == item.matchday_id && session == countSession)
            {
                selectedDate = date
                selectedMD = matchDay
                selectedSession = session
                selectedX = offset
                selectedbutton = button
                selectedRoundName = item.round_n
                
                setTitleAndSubTitle()
            }
            
            countSession += 1
            offset += Int(buttonWidth)+7
            button.addSubview(labelSecondary)
            calendarScrollView.addSubview(button)
        }
        
    }
    
    func  setTeamImage(_ teamId: Int) -> UIImage?{
        
        let imgUrl = self.teamLogoBaseUrl.replacingOccurrences(of: "{TEAMID}", with: "\(teamId)")
        let img = Utility.GetCachedImage("matches_team_" + String(teamId), url: imgUrl)
        if (img != nil) {
            //let imgView = UIImageView(image: Utility.newImageFromImage(img!, scaledToSize: CGSize(width: imagesWidth, height: imagesHeight), withAplha: 1))
            return img;
        }
        return nil
    }
    
        
    
    
    
    //MARK: CollectionView Datasource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.schedule.Matches.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "matchCell", for: indexPath) as! MatchCollectionViewCell
        
        let match = schedule.Matches[indexPath.row]
        cell.matchId = match.MatchId
        
        cell.homeTeamLabel.text = translationData?.teams[String(match.HomeTeamId)]?.name.uppercased()
        cell.awayTeamLabel.text = translationData?.teams[String(match.AwayTeamId)]?.name.uppercased()
        cell.homeTeamImage.image = setTeamImage(match.HomeTeamId)
        cell.awayTeamImage.image = setTeamImage(match.AwayTeamId)
        cell.timeScoreLabel.text = Utility.setTime(match.DateCET as Date)
        
        if self.viewConfiguration!.parameters["skin"] != nil && self.viewConfiguration!.parameters["skin"]! == "light" {
            cell.homeTeamLabel.textColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1)
            cell.awayTeamLabel.textColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1)
            cell.timeScoreLabel.textColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1)
        }
        
        let backCell = Utility.GetCachedImage("matches_background_singlematch", url: self.viewConfiguration!.images.getValue("background_second"))
        
        cell.backCellImage.image = backCell
        
        MatchCollectionViewCell.drawMatchStatus(cell, item: match, videoDataUrl: self.viewConfiguration!.feeds["feedVideodata"]!, skin: (self.viewConfiguration!.parameters["skin"] != nil) ? self.viewConfiguration!.parameters["skin"]! : "")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if isFirstLoading || isRefresh{
            cell.alpha = 0
            UIView.animate(withDuration: 0.3, delay: 0.1 * Double(indexPath.row), options: UIViewAnimationOptions.curveEaseOut, animations: { cell.alpha = 1 }, completion: nil)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellViewSize!
    }
    
    //MARK: CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "matchCell", for: indexPath) as! MatchCollectionViewCell
        
        NSLog("MatchButton tag --> " + String(cell.tag))

        isFirstLoading = false
        
        let item = schedule.Matches[indexPath.row]
        //var item = self.schedule.getMatch(match.MatchId)
        var videoId = ""
        if(item.isLive()){
            videoId = item.Videos.Live[0].VideoId
        }else if(item.isScheduled()){
            videoId = item.Videos.Scheduled[0].VideoId
        }else if(item.isReplay()){
            videoId = item.Videos.Replay[0].VideoId
        }
        
        if(videoId.count > 0){
            
            let videoData = VideoDataController().getVideoData((self.viewConfiguration?.feeds["feedVideodata"])!, videoId: String(videoId))
            if (videoData.videoUrl.isEmpty == false) {
  
                repeatingTimer.clean()  
                
                
                Match.playMatch(videoData: videoData, matchId: item.MatchId, viewConfiguration: viewConfiguration!)
                
//                var dictAdditionalData: [String: String] = [:]
//                dictAdditionalData["videoUrl"] = videoData.videoUrl
//                dictAdditionalData["matchId"] = String(item.MatchId)
//                
//                let urlString = viewConfiguration?.actions["player"]
//                if(urlString != nil){
//                    ConfiguratorController.sharedInstance.action(urlString!, additionalData: dictAdditionalData)
//                }
                
            }
        }
        
    }
    
}
