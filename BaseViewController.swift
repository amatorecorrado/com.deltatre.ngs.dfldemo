//
//  BaseViewController.swift
//  AppleTV
//
//  Created by Corrado Amatore on 04/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController : UIViewController{
    

    //var appInitType = AppInitType()
    //var aliasType = ""
    //var appInit: AppInit! = nil

    var ngs_analytics = NGSAnalytics()
    
    var viewConfiguration: ConfigurationModel?
    
    var isLoading = false
    var alertIsVisisble = false
    var alert = UIAlertController()
    
    let secret: String = "f197181ca63f24eec6fe5fa7733c55435316534af280783453fba3bfa540722d"
    
    var isConnected = true
    
    //MEASURE
    var startMeasure: Date?;
    var endMeasure: Date?;
    
    override func viewDidLoad() {
        isLoading = true
        startMeasure = Date()
        
        if Utility.isConnectedToNetwork() == false {
            isConnected = false
            showAlert()
        }else{
            isConnected = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if(isLoading){
            isLoading = false
            
            measureEnd()
        }

        
    }
    

    func measureEnd(){
        endMeasure = Date()
        let timeInterval: Double = endMeasure!.timeIntervalSince(startMeasure!); // <<<<< Difference in seconds (double)
        NSLog("Time to load view app: \(timeInterval) seconds");
    }
    
    func dismissAlert() {
        alert.dismiss(animated: false, completion: nil)
        alertIsVisisble = false
    }
    
    func ngs_analytics_setView(_ videoUrl: String, matchId: String, viewName: String){
        ngs_analytics.parameter_VideoUrl = String(videoUrl)
        ngs_analytics.parameter_MatchID = String(matchId)
        ngs_analytics.setView(viewName)
    }
    
    func ngs_analytics_setEvent(_ videoUrl: String, matchId: String, moduleName: String, eventName: String){
        ngs_analytics.parameter_VideoUrl = String(videoUrl)
        ngs_analytics.parameter_MatchID = String(matchId)
        ngs_analytics.setEvent(moduleName,EventAction: eventName,EventLabel: String(matchId))
    }
    
    func ngs_analytics_setEvent(_ videoUrl: String, matchId: String, moduleName: String, eventName: String, linkID: String){
        ngs_analytics.parameter_VideoUrl = String(videoUrl)
        ngs_analytics.parameter_MatchID = String(matchId)
        ngs_analytics.parameter_LinkID = linkID
        ngs_analytics.setEvent(moduleName,EventAction: eventName,EventLabel: String(matchId))
    }
    
    func showAlert() {
        alert = UIAlertController.init(title: "Check you internet connection before enter the app", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.view.frame = self.view.bounds
        
        let x = (1920/2)-(alert.view.frame.size.width/2)
        let y = (1080/2)-(alert.view.frame.size.height/2)
        alert.view.frame.origin = CGPoint(x: x,y: y)
        
        let action = UIAlertAction(title: "Check again", style: UIAlertActionStyle.default, handler: {(alert: UIAlertAction!) in
            
            if Utility.isConnectedToNetwork() == false {
                self.showAlert()
            }else{
                self.isConnected = true
                let mainViewController = DispatcherView()
                self.navigationController?.pushViewController(mainViewController, animated: true)
                self.removeFromParentViewController()
                
            }
        })
        
        alert.addAction(action)
        
        //self.navigationController?.pushViewController(alert, animated: false)
        self.present(alert, animated: false, completion: nil)
        alertIsVisisble = true
    }
    
}
