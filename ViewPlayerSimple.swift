//
//  ViewPlayerSimple.swift
//  AppleTV
//
//  Created by Corrado Amatore on 01/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer

class ViewPlayerSimple: BaseViewController {
    
    let initView = UIView()
    var avPlayerView = AVPlayerViewController()
    var avPlayer = AVPlayer()
    var MatchID: Int?
    var VideoUrl: String  = ""
    var SeekTime: String = ""
    let screenRect = UIScreen.main.bounds
    

    override func viewDidLoad() {
        
        super.viewDidLoad()

        ngs_analytics.parameter_VideoUrl = String(VideoUrl)
        ngs_analytics.parameter_MatchID = (MatchID?.description)!
        ngs_analytics.setView("PlayerSimpleView")
        ngs_analytics.setEvent("VideoStream",EventAction: "Highlight",EventLabel: String(describing: MatchID))
        
        
        let backRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backRecognizer(_:)))
        self.view.addGestureRecognizer(backRecognizer)
        backRecognizer.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue as Int)]
        
        initView.backgroundColor = UIColor.init(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1)
        initView.frame = screenRect
        initView.isUserInteractionEnabled = true
        
        
        let tokenizedUrl: String = AkamaiTokenizer.tokenize(VideoUrl, secret: secret)
        
        let url = URL(string: tokenizedUrl)
        let avAsset = AVAsset(url: url!)
        let avPlayerItem = AVPlayerItem(asset: avAsset)
        avPlayer = AVPlayer(playerItem: avPlayerItem)
        
        avPlayerView.view.frame = screenRect
        avPlayerView.view.isUserInteractionEnabled = false
        avPlayerView.showsPlaybackControls = true
        avPlayerView.player = avPlayer
        avPlayerView.view.backgroundColor = UIColor.clear
        avPlayerView.view.layer.shadowColor = UIColor.black.cgColor
        avPlayerView.view.layer.shadowOpacity = 0.8
        avPlayerView.view.layer.shadowOffset = CGSize.zero
        avPlayerView.view.layer.shadowRadius = 25
        avPlayerView.view.layer.shadowPath = UIBezierPath(rect: avPlayerView.view.bounds).cgPath
        avPlayerView.view.layer.shouldRasterize = false
        avPlayerView.view.transform = CGAffineTransform.identity
        avPlayerView.videoGravity = "AVLayerVideoGravityResize"
        avPlayerView.view.clipsToBounds = false
        self.addChildViewController(avPlayerView)
        NotificationCenter.default.addObserver(self, selector: #selector(avPlayerItemDidPlayToEndTimeNotification), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        avPlayerView.view.isHidden = true
        initView.addSubview(avPlayerView.view)
        self.view.addSubview(initView)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        avPlayer.addObserver(self, forKeyPath:"rate", options:.initial, context:nil)
        avPlayer.addObserver(self, forKeyPath:"status", options:.initial, context:nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        avPlayer.removeObserver(self, forKeyPath: "rate")
        avPlayer.removeObserver(self, forKeyPath: "status")
    }
    
    deinit {
        avPlayerView.removeFromParentViewController()
        
        NSLog("deinit")
    }
    
    @objc func backRecognizer(_ recognizer: UITapGestureRecognizer) {
        avPlayer.pause()
        navigationController?.popViewController(animated: true)
    }
    
//    override func dismissViewControllerAnimated(flag: Bool, completion: (() -> Void)?) {
//        NSLog("dismissViewControllerAnimated")
//
//            avPlayer.pause()
//            super.dismissViewControllerAnimated(flag, completion: completion)
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "rate") {
            NSLog("rate \(avPlayer.rate)")
            if (avPlayer.rate == 0.0) {
                avPlayerView.view.isUserInteractionEnabled = true
            } else {
                avPlayerView.view.isUserInteractionEnabled = false
            }
        }
        if (keyPath == "status") {
            NSLog("status \(avPlayer.status.rawValue)")
            if (avPlayer.status == AVPlayerStatus.unknown) {
                NSLog("AVPlayerStatus.Unknown")
            } else if (avPlayer.status == AVPlayerStatus.failed) {
                NSLog("AVPlayerStatus.Failed")
            } else if (avPlayer.status == AVPlayerStatus.readyToPlay) {
                NSLog("AVPlayerStatus.ReadyToPlay")
                //if(avPlayer.status == AVPlayerStatus.ReadyToPlay && avPlayer.currentItem?.status == AVPlayerItemStatus.ReadyToPlay) {
                if (SeekTime.isEmpty) {
                    avPlayer.currentItem?.seek(to: kCMTimeZero)
                } else {
                    let time = CMTimeMakeWithSeconds(Double(SeekTime)!, avPlayer.currentItem!.asset.duration.timescale)
                    avPlayer.currentItem?.seek(to: time)
                }
                avPlayer.play()
                avPlayerView.view.isHidden = false
                
                print(avPlayer.currentItem?.accessLog()?.events.first?.playbackType)
                //}
            }
        }
    }
    
    @objc func avPlayerItemDidPlayToEndTimeNotification(_ notification: Notification) {
        NSLog("avPlayerItemDidPlayToEndTimeNotification")
        avPlayer.pause()
        super.dismiss(animated: true, completion: nil)
        
    }


}
