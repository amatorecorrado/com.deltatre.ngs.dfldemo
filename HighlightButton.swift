//
//  HighlightButton.swift
//  AppleTV
//
//  Created by Corrado Amatore on 30/06/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit


class HighlightButton: UIButton
{
    
    //PUBLIC PROPERTIES
    var selectionPercentageShadow = 5
    var selectionPercentageScale = 5
    var selectionMotionEffectAmount = 10
    var shadowRadius:CGFloat = 7
    var shadowOpacity:Float = 0.7
    var skin = ""
    
    //ENDPUBLIC PROPERTIES
     
    fileprivate var group = UIMotionEffectGroup()
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        coordinator.addCoordinatedAnimations( {
                if self.isFocused {
                    self.layer.zPosition = 1
                    let scale = 1 + (CGFloat(0.01) * CGFloat(self.selectionPercentageScale))
                    let shadow = CGFloat(0.01) * CGFloat(self.selectionPercentageShadow)
                    
                    self.transform = CGAffineTransform(scaleX: scale, y: scale)
                    self.layer.shadowOffset = CGSize(width: 0,height: (self.frame.height * shadow))
                    self.layer.shadowRadius = self.shadowRadius
                    self.layer.shadowOpacity = self.shadowOpacity
                    
                    let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
                    horizontal.minimumRelativeValue = -self.selectionMotionEffectAmount
                    horizontal.maximumRelativeValue = self.selectionMotionEffectAmount
                    
                    let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
                    vertical.minimumRelativeValue = -self.selectionMotionEffectAmount
                    vertical.maximumRelativeValue = self.selectionMotionEffectAmount
                    
                    self.group.motionEffects = [horizontal, vertical]
                    self.addMotionEffect(self.group)
                    
                    if self.skin == "light" {
                        if let label = self.viewWithTag(1001) as? UILabel {
                            label.textColor = UIColor.white
                        }
                        if let label = self.viewWithTag(1000) as? UILabel {
                            label.textColor = UIColor.white
                        }
                    }
                }
                else {
                    self.layer.zPosition = 0
                    self.transform = CGAffineTransform.identity
                    self.layer.shadowOffset = CGSize.zero
                    self.layer.shadowRadius = 0
                    self.layer.shadowOpacity = 0
                    
                    if self.skin == "light" {
                        if let label = self.viewWithTag(1001) as? UILabel {
                            label.textColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1)
                        }
                        if let label = self.viewWithTag(1000) as? UILabel {
                            label.textColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1)
                        }
                    }
                    
                    self.removeMotionEffect(self.group)
                }
            },
            completion: nil)
    }
}
