//
//  PlayerMenuViewController.swift
//  AppleTV
//
//  Created by Corrado Amatore on 22/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit

class PlayerMenuViewController: BaseViewController {
    @IBOutlet weak var awayTeamLabel: UILabel!
    @IBOutlet weak var awayTeamImage: UIImageView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var homeTeamImage: UIImageView!
    @IBOutlet weak var homeTeamLabel: UILabel!
    @IBOutlet weak var awayScorersStackView: UIStackView!
    @IBOutlet weak var homeScorersStackView: UIStackView!

    @IBOutlet weak var upMenuLabel: UILabel!
    @IBOutlet weak var upMenuImage: UIImageView!
    @IBOutlet weak var rightMenuLabel: UILabel!
    @IBOutlet weak var rightMenuImage: UIImageView!
    @IBOutlet weak var downMenuLabel: UILabel!
    @IBOutlet weak var downMenuImage: UIImageView!
    @IBOutlet weak var leftMenuImage: UIImageView!
    @IBOutlet weak var leftMenuLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }
    

    // MARK: - METHODS

    func showPanel(){
        self.view.alpha = 0
        
        hideMenu()
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.view.alpha = 1
            }, completion:nil)
    }
    
    func hidePanel(){
        self.view.alpha = 0
    }
    
    func showMenuUp(){
        showMenuItem(upMenuLabel,image: upMenuImage)
    }
    
    func showMenuDown(){
        showMenuItem(downMenuLabel,image: downMenuImage)
    }
    
    func showMenuRight(){
        showMenuItem(rightMenuLabel,image: rightMenuImage)
    }
    
    func showMenuLeft(){
        showMenuItem(leftMenuLabel,image: leftMenuImage)
    }
    
    func showMenuItem(_ label: UILabel!, image: UIImageView){
        label.alpha = 1
        image.alpha = 1
        label.transform = label.transform.scaledBy(x: 1.3, y: 1.3)
        image.transform = image.transform.scaledBy(x: 1.5, y: 1.5)
    }
    
    func hideMenu(){
        hideMenuItem(upMenuLabel,image: upMenuImage)
        hideMenuItem(downMenuLabel,image: downMenuImage)
        hideMenuItem(rightMenuLabel,image: rightMenuImage)
        hideMenuItem(leftMenuLabel,image: leftMenuImage)
    }
    
    func hideMenuItem(_ label: UILabel!, image: UIImageView){
        label.alpha = 0
        image.alpha = 0.5
        label.transform = CGAffineTransform.identity
        image.transform = CGAffineTransform.identity
        
    }
    
    func scoreUpdate(_ match: Match, traslations: Translations, teamLogoUrl: String){
        
        for view in self.homeScorersStackView.subviews {
            view.removeFromSuperview()
        }
        for view in self.awayScorersStackView.subviews {
            view.removeFromSuperview()
        }
        
        let home: String? =  traslations.teams[String(match.HomeTeamId)]?.name
        let away: String? =  traslations.teams[String(match.AwayTeamId)]?.name
        
        homeTeamLabel.text = home?.uppercased()
        awayTeamLabel.text = away?.uppercased()
        
        let imgHome = Utility.GetCachedImage("lineup_team_" + String(match.HomeTeamId), url: teamLogoUrl.replacingOccurrences(of: "{TEAMID}", with: String(match.HomeTeamId)))
        
        let imgAway = Utility.GetCachedImage("lineup_team_" + String(match.AwayTeamId), url: teamLogoUrl.replacingOccurrences(of: "{TEAMID}", with: String(match.AwayTeamId)))
        
        homeTeamImage.image = imgHome
        awayTeamImage.image = imgAway
        
        // SCORE
        var scoreAway: String? = String(match.Results.AwayGoals)
        var scoreHome: String? = String(match.Results.HomeGoals)
        
        if (scoreHome == nil) {
            scoreHome = "0"
        }
        if (scoreAway == nil) {
            scoreAway = "0"
        }
        
        let score = scoreHome! + "-" + scoreAway!
        self.scoreLabel.text = score
        
        //SCORERS
        var index = 0
        if(match.Results.Scorers.HomeGoals.count>0){
            for item in match.Results.Scorers.HomeGoals {
                let offset = index * 30
                let homeScorersLabel = UILabel()
                homeScorersLabel.frame.origin = CGPoint(x: 0, y: offset)
                homeScorersLabel.frame.size = CGSize(width: homeScorersStackView.frame.width, height: 40)
                homeScorersLabel.textColor = UIColor.lightGray
                homeScorersLabel.font = UIFont(name: "Dosis-Regular", size: 25)
                homeScorersLabel.textAlignment = NSTextAlignment.right
                
                
                if(traslations.players[String(item.PlayerId)] != nil){
                    homeScorersLabel.text = (traslations.players[String(item.PlayerId)]?.webname)! + " " + String(item.Minute)  + "'"
                }else{
                    homeScorersLabel.text = String(item.Minute)  + "'"
                }
                
                homeScorersLabel.text = homeScorersLabel.text!.uppercased()
                homeScorersStackView.addSubview(homeScorersLabel)
                index = index + 1
            }
        }
        index = 0
        if(match.Results.Scorers.AwayGoals.count>0){
            for item in match.Results.Scorers.AwayGoals {
                let offset = index * 30
                let awayScorersLabel = UILabel()
                awayScorersLabel.frame.origin = CGPoint(x: 0, y: offset)
                awayScorersLabel.frame.size = CGSize(width: awayScorersStackView.frame.width, height: 40)
                awayScorersLabel.textColor = UIColor.lightGray
                awayScorersLabel.font = UIFont(name: "Dosis-Regular", size: 25)
                awayScorersLabel.textAlignment = NSTextAlignment.left
                
                if(traslations.players[String(item.PlayerId)] != nil){
                    awayScorersLabel.text = (traslations.players[String(item.PlayerId)]?.webname)! + " " + String(item.Minute)  + "'"
                }else{
                    awayScorersLabel.text = String(item.Minute)  + "'"
                }
                
                awayScorersLabel.text = awayScorersLabel.text!.uppercased()
                awayScorersStackView.addSubview(awayScorersLabel)
                index = index + 1
            }
        }
        
        
    }

}
