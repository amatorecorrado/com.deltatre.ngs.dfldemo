//
//  EventModel.swift
//  AppleTV
//
//  Created by Corrado Amatore on 06/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public struct EventModel{
    public var id: Int?
    public var descr: String?
    public var text: String?
    public var code: Int?
    public var minute: Int?
    public var playerFrom: Int?
    public var playerTo: Int?
    public var subCode: Int?
    public var type: Int?
    public var tag: String?
    public var time: Date
    public var teamTo: Int?
    public var teamFrom: Int?
    public var phase: Int?
    public var isValidated: Bool?
    public var injuryMinute: Int?
    
    public init() {
        /*    self.id = nil
         self.descr = ""
         self.code = 0
         self.minute = 0
         self.playerFrom = 0
         self.playerTo = 0
         self.subCode = 0
         self.type = 0
         self.tag = ""
         self.teamTo = 0
         self.teamFrom = 0
         self.phase = 0
         self.isValidated = false
         self.injuryMinute = 0
         */
        self.time = Date.init(timeIntervalSince1970: 0)
        
    }
}
