//
//  LiveViewController.swift
//  AppleTV
//
//  Created by Corrado Amatore on 15/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit
import VideoPlayerFeatured
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class LiveViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var matchesCollectionView: UICollectionView!
    @IBOutlet weak var backMatchesImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
   
    @IBOutlet weak var spinnerMatches: UIActivityIndicatorView!
    
    var schedule = Schedule()
    
//    var cup: Int = 0
    var cupSeason: Int = 0
    var season: Int = 0
    var roundId: Int = 0
    var matchDay: Int = 0
    var session: Int = 0
    var matchId: Int = 0
    var offset = 0
    var matchDayScheduleUrl = URL(string: "")
    //var teamLogoBaseUrl = ""
    var matchdays: [MatchdayModel] = []
    var livematches: [Match] = []
    var isFirstLoading = true
    var selectedX = 0
    var cupInfoUrl = URL(string: "")
    var translationData: Translations?
    
    var selectedbutton: AdvancedButton? = nil
    var selectedMD = 0
    var selectedSession = 0
    var selectedRoundName: String = ""
    fileprivate var selectedDate: String? = nil
    var cellViewSize: CGSize?
    
     var parameters = ParameterModel()

    var repeatingTimer = RepeatingTimer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        spinnerMatches.hidesWhenStopped = true
        spinnerMatches.alpha = 1
        spinnerMatches.startAnimating()
        load()
        let nibCell = UINib(nibName: "MatchCollectionViewCell",bundle: nil)
        self.matchesCollectionView.register(nibCell, forCellWithReuseIdentifier: "matchCell")
        
        cellViewSize = (Bundle.main.loadNibNamed("MatchCollectionViewCell", owner: self, options: nil)?.first as AnyObject).size
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if self.matchesCollectionView.indexPathsForSelectedItems?.count > 0 {
            self.matchesCollectionView.deselectItem(at: (self.matchesCollectionView.indexPathsForSelectedItems?.first)!, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func getMatches(round: Int, matchday: Int, date: String) {
        DispatchQueue.global(qos: .background).async(execute: {
           
            DataProvider.sharedInstance.setParameters(self.viewConfiguration!)
            
            let params = self.viewConfiguration?.getParameters()
            self.schedule = DataProvider.sharedInstance.getSchedule(cupcode: (params?.CupCode)!, season: Int((params?.Season)!)!)
            //self.livematches = self.FilterOnlyLiveMatchesToday(schedule: self.schedule)
            
            self.livematches = self.schedule.Matches
            
//            if(date != ""){
//                self.selectedDate = date
//            }
//
//            if(self.livematches.count > 0){
//                self.schedule.Matches = self.livematches
//            }
//            self.schedule.Matches = self.FilterMatchesBySession(self.session, matches: self.schedule.Matches)
            
            DispatchQueue.main.sync(execute: {
                
                self.drawTitle(self.livematches)
                
                self.matchesCollectionView.reloadData()
                
                
                self.spinnerMatches.stopAnimating()
                
                NSLog("dispatched getMatches")
            })
            
            
            
        })
    }
    
    
    func drawTitle(_ livematches: [Match]) {
        
        if(livematches.count > 0){
            titleLabel.text = "MATCHES LIST"
        }else{
            titleLabel.text = "NO MATCHES"
        }
        
    }
    
    func getCalendar(){
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async(execute: {
            
            let cup = self.viewConfiguration!.parameters["cup"]
            let season = self.viewConfiguration!.parameters["season"]
            let controller = CalendarController(feedUrl: self.viewConfiguration!.feeds["feedMatchdays"]!, cup: cup!, season: season!)
            let _matchdays = controller.get()
            
            
            DispatchQueue.main.sync(execute: {
                
                //                _ = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(self.drawCalendar), userInfo: nil, repeats: false)
                self.matchdays = _matchdays
                //self.drawCalendar()
                
                NSLog("dispatched getCalendar")
            })
        })
        
        
    }
    
//    func getCupInfo() {
//        let jsonData = try? Data(contentsOf: self.cupInfoUrl!)
//
//        if (jsonData?.count > 0) {
//            do {
//                let json = try JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments)
//                Utility.deserializeCupInfo((json as? Array<Dictionary<String, AnyObject>>)!, cup: cup, cupSeason: cupSeason, season: season, roundId: &roundId, matchDay: &matchDay, session: &session)
//            } catch {
//                NSLog("error serializing JSON: \(error)")
//            }
//        }
//    }
    
//    func getTranslation()
//    {
//        if let url: String? = self.viewConfiguration!.feeds["feedTranslations"]!.replacingOccurrences(of: "{CUPID}", with: String(self.cup)).replacingOccurrences(of: "{SEASONID}", with: String(self.season)){
//            translationData = Translations()
//            translationData = translationData!.getTranslations(url!)
//        }
//    }
    
    func load() {
//        cup = Int((viewConfiguration?.parameters["cup"])!)!
        cupSeason = Int((viewConfiguration?.parameters["cupSeason"])!)!
        season = Int((viewConfiguration?.parameters["season"])!)!
        cupInfoUrl = URL(string: (viewConfiguration?.feeds["feedCupConfiguration"])!)!
        
//        teamLogoBaseUrl = self.viewConfiguration!.images.getValue("teamLogo")
        
        titleLabel.text = ""
        
        if self.viewConfiguration!.parameters["skin"] != nil && self.viewConfiguration!.parameters["skin"]! == "light" {
            titleLabel.textColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1)
        } else if self.viewConfiguration!.parameters["skin"] != nil && self.viewConfiguration!.parameters["skin"]! == "white" {
            titleLabel.textColor = UIColor.white
        }
        
//        getCupInfo()
//
//        getTranslation()
        
        
        DispatchQueue.main.async {
            self.repeatingTimer.intervalSeconds = 60
            self.repeatingTimer.timer.setEventHandler(handler: { [weak self] in
                // called every so often by the interval we defined above
                if let round = self?.roundId, let matchday = self?.matchDay{
                    self?.getMatches(round: round,matchday: matchday, date: "")
                }
            })
            self.repeatingTimer.resume()
        }
        
        var backgroundImage = ImagesUtility.sharedInstance.getImage(self.viewConfiguration!.images.getValue("background"))
        if backgroundImage == nil {
            backgroundImage = Utility.GetCachedImage("app_background_matchesview", url: self.viewConfiguration!.images.getValue("background"))
        }
        backMatchesImage.image = backgroundImage
        
        
        //self.view.addSubview(initView)
        //splashImageView!.removeFromSuperview()
    }
    
    func FilterOnlyLiveMatchesToday(schedule: Schedule) -> [Match]{
        var livematches: [Match] = []
        var todaymatches: [Match] = []
        
        let userCalendar = Calendar.current
        let hourMinuteComponents: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
        let datetime = (userCalendar as NSCalendar).components(
            hourMinuteComponents,
            from: Date())
        
        let datematchstr: String = String(describing: datetime.year!)+String(format: "%02d", datetime.month!)+String(format: "%02d", datetime.day!)
        
        
        if(datematchstr != ""){
            todaymatches = self.FilterMatchesByDate(date: datematchstr, matches: schedule.Matches)
        }
        for match in todaymatches
        {
            if(match.isLive()){
                livematches.append(match)
            }
        }
        return todaymatches
    }
    
    func FilterMatchesByDate(date: String, matches: [Match]) -> [Match]{
        var res: [Match] = []
        
        for match: Match in matches
        {
            let datematchstr = Date.init(dateTimeStringWithoutFormat: date)
            if let dt = date as? String{
            //if(date == datematchstr){
                res.append(match)
            //}
            }
        }
        return res
    }
    
    func setTitleAndSubTitle(){
        let dateTimeTmp = Date.init(dateTimeStringWithoutFormat: selectedDate!)
        //let userCalendar = NSCalendar.currentCalendar()
        //let dateComponents: NSCalendarUnit = [.Year, .Month, .Day]
        //let datetime = userCalendar.components(
        //    dateComponents,
        //    fromDate: dateTimeTmp)
        //let month = datetime.month
        
        //let df: NSDateFormatter = NSDateFormatter()
        //let monthName: String = df.monthSymbols[(month - 1)].uppercaseString
        
        titleLabel.text = Utility.setDateLabel(dateTimeTmp)
        
    }
    
    func FilterMatchesBySession(_ session: Int, matches: [Match]) -> [Match]{
        var res: [Match] = []
        
        for match: Match in matches
        {
            if(match.Session == session){
                res.append(match)
            }
        }
        return res
    }
    
//    func  setTeamImage(_ teamId: String) -> UIImage?{
//        
//        
//        let imgUrl = self.teamLogoBaseUrl.replacingOccurrences(of: "{TEAMCODE}", with: "\(teamId)")
//        let img = Utility.GetCachedImage("matches_team_" + String(teamId), url: imgUrl)
//        if (img != nil) {
//            //let imgView = UIImageView(image: Utility.newImageFromImage(img!, scaledToSize: CGSize(width: imagesWidth, height: imagesHeight), withAplha: 1))
//            return img;
//        }
//        return nil
//    }
    
    
    //MARK: CollectionView Datasource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.schedule.Matches.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "matchCell", for: indexPath) as! MatchCollectionViewCell
        
        let match = schedule.Matches[indexPath.row]
        cell.matchId = match.MatchId
        
        cell.homeTeamLabel.text = match.HomeTeamName.uppercased()
        cell.awayTeamLabel.text = match.AwayTeamName.uppercased()
        cell.homeTeamImage.image = Utility.GetCachedImage("matches_team_" + String(match.HomeTeamId), url: match.HomeTeamLogoUrl) //setTeamImage(match.HomeTeamCode)
        cell.awayTeamImage.image = Utility.GetCachedImage("matches_team_" + String(match.AwayTeamId), url: match.AwayTeamLogoUrl) //setTeamImage(match.AwayTeamCode)
        cell.timeScoreLabel.text = Utility.setTime(match.DateCET as Date)
        
        if self.viewConfiguration!.parameters["skin"] != nil && self.viewConfiguration!.parameters["skin"]! == "light" {
            cell.homeTeamLabel.textColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1)
            cell.awayTeamLabel.textColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1)
            cell.timeScoreLabel.textColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1)
        }
        
        let backCell = Utility.GetCachedImage("matches_background_singlematch", url: self.viewConfiguration!.images.getValue("background_second"))
        
        cell.backCellImage.image = backCell
        
        
        MatchCollectionViewCell.drawMatchStatus(cell, item: match, videoDataUrl: self.viewConfiguration!.feeds["feedVideodata"]!, skin: (self.viewConfiguration!.parameters["skin"] != nil) ? self.viewConfiguration!.parameters["skin"]! : "")
        

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

        if isFirstLoading {
            cell.alpha = 0
            UIView.animate(withDuration: 0.3, delay: 0.1 * Double(indexPath.row), options: UIViewAnimationOptions.curveEaseOut, animations: { cell.alpha = 1 }, completion: nil)
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellViewSize!
    }
    
    //MARK: CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "matchCell", for: indexPath) as! MatchCollectionViewCell
        
        NSLog("MatchButton tag --> " + String(cell.tag))
        isFirstLoading = false
        
        let item = schedule.Matches[indexPath.row]
        //var item = self.schedule.getMatch(match.MatchId)
        var videoId = ""
        if(item.isLive()){
            videoId = item.Videos.Live[0].VideoId
        }else if(item.isScheduled()){
            videoId = item.Videos.Scheduled[0].VideoId
        }else if(item.isReplay()){
            videoId = item.Videos.Replay[0].VideoId
        }
        
        
        if(videoId.count > 0){
            let videoData = VideoDataController().getVideoData(self.viewConfiguration!.feeds["feedVideodata"]!, videoId: String(videoId))
            if (videoData.videoUrl.isEmpty == false) {
                
//                var dictAdditionalData: [String: String] = [:]
//                dictAdditionalData["videoUrl"] = videoData.videoUrl
//                dictAdditionalData["matchId"] = String(item.MatchId)
//                
//                Utility.timerStop(&getmatchesTimer)
//                
//                let urlString = viewConfiguration?.actions["player"]
//                if(urlString != nil){
//                    ConfiguratorController.sharedInstance.action(urlString!,additionalData: dictAdditionalData)
//                }
                
                var dictAdditionalData: [String: String] = [:]
                dictAdditionalData["videoUrl"] = videoData.videoUrl
                dictAdditionalData["currentVideoId"] = videoData.videoId
                dictAdditionalData["matchId"] = String(item.MatchId)
//                dictAdditionalData["videoUrl"] = "http://vod-i.ngs.deltatre.net/5da5cab0-9979-498c-82dc-b950e2d66681/4b2caff7-1f83-4bf4-959f-f665fe0c1b65.ism/manifest(format=m3u8-aapl)"
                
                let urlString = viewConfiguration?.actions["player"]
                if(urlString != nil){
                    
                    Configurator.shared.navigationController = ConfiguratorController.sharedInstance.navigationController
                    Configurator.shared.action(url: urlString!, additionalData: dictAdditionalData as AnyObject)
                    
                    //ConfiguratorController.sharedInstance.action(urlString!, additionalData: dictAdditionalData as AnyObject)
                }
                
            }
        }
        
    }
    
}
