//
//  PlayerItem.swift
//  AppleTV
//
//  Created by Corrado Amatore on 04/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation


open class PlayerModel {
    open var ID: Int?
    open var BibNumber: Int?
    open var OfficialName: String?
    open var OfficialSurname: String?
    open var Role: Int?
    open var `Type`: String?
    //public var ShortName: String?
    open var ThumbSmall: String?
    open var ThumbMedium: String?
    open var ThumbLarge: String?
    
    //public var PhotoURL: String?
    open var IsTeam: Bool!
    open var IsCaptain: Bool!
    open var IsGoalkeeper: Bool!
    
//    public var IsYellow: Bool!
//    public var IsDoubleYellow: Bool!
//    public var IsRed: Bool!
//    public var IsSubstitutionIN: Bool!
//    public var IsSubstitutionOUT: Bool!
//    public var SubstitutionMinuteIN: Int!
//    public var SubstitutionMinuteOUT: Int!
    
    open var events: [EventModel]?
    
    public init() {
        self.ID = -1
        self.BibNumber = 0
        self.OfficialName = ""
        self.OfficialSurname = ""
        self.ThumbLarge = ""
        self.ThumbMedium = ""
        self.ThumbSmall = ""
        self.IsTeam = false
        self.IsCaptain = false
        self.IsGoalkeeper = false
//        self.IsYellow = false
//        self.IsDoubleYellow = false
//        self.IsRed = false
//        self.IsSubstitutionIN = false
//        self.IsSubstitutionOUT = false
//        self.SubstitutionMinuteIN = 0
//        self.SubstitutionMinuteOUT = 0
        self.events = nil
    }
    
    open func isLineup() -> Bool{
         return (self.Type == "lineup")
    }
    
    open func isBench() -> Bool{
        return (self.Type == "bench")
    }
}
