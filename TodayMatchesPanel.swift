//
//  TodayMatchesPanel.swift
//  AppleTV
//
//  Created by Corrado Amatore on 07/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

protocol TodayMatchesPanelDelegate{
    func matchSelected(_ videoId: Int, videoUrl: String, transform: Bool, newMatch: Match)
    func goToNewMatch(_ newMatch: Match, videoData: VideoDataModel)

}

class TodayMatchesPanel: BaseViewController{
    
    var delegate: TodayMatchesPanelDelegate?
    
    var matchesScrollView = UIScrollView()
    var otherMatchHorizontalLineLabel = UILabel()
    let titleMatchesLabel = UILabel()
    let warningLabel = UILabel()
    let spinnerOtherMatches = UIActivityIndicatorView(activityIndicatorStyle: .white)
    let screenRect = UIScreen.main.bounds
    var schedule = Schedule()
    
    var videoDataUrl: String = ""
    var selectedSecondPlayerMatchID: Int = -1
    var matchesCount = 0
    var firstMatchID = 0
    var teamLogoBaseUrl: String = ""
    var isLastMatch = false
    
    
    var translationData: Translations?
    var cup:Int = 0
    var season:Int = 0
    var roundId: Int = 0
    var matchDay: Int = 0
    var matchId: Int = 0
    var actualDate: Date?
    var skin: String = ""
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    init(cup:Int, season:Int, roundId: Int, matchDay: Int, matchId: Int, teamLogoBaseUrl: String, videoDataUrl: String, skin: String){
        super.init(nibName: nil, bundle: nil)
        self.cup = cup
        self.season = season
        self.roundId = roundId
        self.matchDay = matchDay
        self.matchId = matchId
        self.teamLogoBaseUrl = teamLogoBaseUrl
        self.videoDataUrl = videoDataUrl
        self.skin = skin
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        
        self.spinnerOtherMatches.frame = CGRect(x: self.screenRect.width / 2 - 20, y: 120, width: 40, height: 40)
       
        DispatchQueue.main.async {
            self.view.addSubview(self.spinnerOtherMatches)
            self.spinnerOtherMatches.startAnimating()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleMatches(_:)), name: NSNotification.Name(rawValue: "matchesDataReady"), object: nil)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.schedule = DataProvider.sharedInstance.getMatches(cup, season: season, roundId: roundId, matchDay: matchDay, matchId: matchId, actualDate: actualDate!)
        
        translationData = DataProvider.sharedInstance.getTranslation(cup, season: season)
        
        DispatchQueue.main.async {
            self.drawMatches()
        }
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)?) {
        NSLog("Dismiss panel animated")
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "matchesDataReady"), object: nil)
        
        
    }
    
    
    func drawMatches() {
        for item in matchesScrollView.subviews {
            item.removeFromSuperview()
        }
        isLastMatch = false
        if (schedule.Matches.count > 0) {
            warningLabel.alpha = 0
            matchesCount = 0
            for item:Match in schedule.Matches {
                
                if(matchesCount == 0){
                    
                    
                    firstMatchID = item.MatchId
                    
                    let fontTitle = UIFont.init(name: "Dosis-SemiBold", size: 32)
                    let title = NSMutableAttributedString(string: "MATCHDAY \(item.MatchDay): ", attributes: [NSFontAttributeName: fontTitle!])
                    
                    
                    let userCalendar = Calendar.current
                    let hourMinuteComponents: NSCalendar.Unit = [.year, .month, .day]
                    let datetime = (userCalendar as NSCalendar).components(
                        hourMinuteComponents,
                        from: item.DateCET as Date)
                    
                    let df: DateFormatter = DateFormatter()
                    let monthName: String = df.monthSymbols[(datetime.month! - 1)].uppercased()
                    
                    let fontDate = UIFont.init(name: "Dosis-Regular", size: 32)
                    let date = NSMutableAttributedString(string: "\(datetime.day!) \(monthName) \(datetime.year!)", attributes: [NSFontAttributeName: fontDate!])
                    
                    let result = NSMutableAttributedString()
                    result.append(title)
                    result.append(date)
                    
                    let titleMatchesLabelFrame = CGRect(x: 0, y: 20, width: screenRect.width, height: 100)
                    
                    
                    
                    titleMatchesLabel.frame = titleMatchesLabelFrame
                    titleMatchesLabel.tag = -6969
                    titleMatchesLabel.attributedText = result
                    //titleMatchesLabel.font = UIFont.init(name: "Dosis-Bold", size: 40)
                    titleMatchesLabel.textAlignment = NSTextAlignment.center
                    titleMatchesLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
                    titleMatchesLabel.lineBreakMode = NSLineBreakMode.byClipping
                    self.view.addSubview(titleMatchesLabel)
                    
                }else if(matchesCount == schedule.Matches.count-1){
                    isLastMatch = true
                }
                
                drawMatch(item,isAdd: false, isUpdate: false, view: UIView())
                matchesCount += 1
            }
            self.matchesScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            
            matchesScrollView.frame = CGRect(x: 70, y: 70, width: screenRect.width - 100, height: 220)
            let calculateWidth = CGFloat(integerLiteral: matchesCount) * 270
            matchesScrollView.contentSize = CGSize(width: calculateWidth, height: 220)
            matchesScrollView.autoresizingMask = UIViewAutoresizing.flexibleWidth
            matchesScrollView.isScrollEnabled = true
            matchesScrollView.clipsToBounds = false
            
            let otherMatchHorizontalLineLabelFrame = CGRect(x: 70, y: 80, width: screenRect.width - 140, height: 50)
            otherMatchHorizontalLineLabel = UILabel(frame: otherMatchHorizontalLineLabelFrame)
            otherMatchHorizontalLineLabel.font = UIFont.init(name: "Dosis-Regular", size: 20)
            otherMatchHorizontalLineLabel.textAlignment = NSTextAlignment.left
            otherMatchHorizontalLineLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
            otherMatchHorizontalLineLabel.lineBreakMode = NSLineBreakMode.byClipping
            
            
            let bottomBorder = CALayer()
            bottomBorder.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3).cgColor
            bottomBorder.frame = CGRect(x: 0, y: otherMatchHorizontalLineLabel.frame.height-6, width: screenRect.width - 140, height: 1)
            otherMatchHorizontalLineLabel.layer.addSublayer(bottomBorder)
            self.view.addSubview(otherMatchHorizontalLineLabel)
            
            self.view.addSubview(matchesScrollView)
            
            UserExperienceUtility.fadeInSubviews(matchesScrollView, duration: 0.2)

            
            if (spinnerOtherMatches.isAnimating) {
                spinnerOtherMatches.stopAnimating()
            }
            
        }else{
            
            warningLabel.alpha = 1
            warningLabel.frame = CGRect(x: self.screenRect.width / 2 - 150, y: 120, width: 300, height: 40)
            warningLabel.font = UIFont.init(name: "Dosis-Medium", size: 28)
            warningLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
            warningLabel.text = String("Today matches coming soon...").uppercased()
            self.view.addSubview(warningLabel)
            
            if (spinnerOtherMatches.isAnimating) {
                spinnerOtherMatches.stopAnimating()
            }
            
            return
        }
        
    }
    
    func drawMatch(_ item: Match, isAdd: Bool, isUpdate: Bool, view: UIView){
        let buttonWidth =  CGFloat(270)
        let imagesWidth = CGFloat(integerLiteral: 36)
        let imagesHeight = CGFloat(integerLiteral: 36)
        
        let offset = CGFloat(matchesCount) * (buttonWidth)
        
        let buttonFrame = CGRect(x: offset, y: 70, width: buttonWidth-20, height: 130)
        
        //        let dayLabelFrame = CGRect(x: offset, y: 0, width: 270, height: 50)
        let verticalLineLabelFrame = CGRect(x: offset + 260, y: 15, width: 2, height: 100)
        
        let imagesTeamHomeFrame = CGRect(x: 5, y: 10, width: imagesWidth, height: imagesHeight)
        let labelTeamHomeFrame = CGRect(x: imagesWidth+15, y: 3, width: 180, height: 50)
        
        let timeScoreLabelFrame = CGRect(x: imagesWidth+15, y: 40, width: 181, height: 50)
        let statusLabelFrame = CGRect(x: imagesWidth+120, y: 40, width: 80, height: 50)
        
        let imagesTeamAwayFrame = CGRect(x: 5, y: 82, width: imagesWidth, height: imagesHeight)
        let labelTeamAwayFrame = CGRect(x: imagesWidth+15, y: 80, width: 180, height: 50)
        
        let button = CustomButton()
        button.frame = buttonFrame
        button.tag = item.MatchId
        button.backgroundColorSelected(UIColor.white,Alpha: 0.1)
        //button.layer.cornerRadius = 18
        //button.adjustsImageWhenHighlighted = true
        
        let userCalendar = Calendar.current
        let hourMinuteComponents: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
        let datetime = (userCalendar as NSCalendar).components(
            hourMinuteComponents,
            from: item.DateCET as Date)
        
        let verticalLineLabel = UILabel(frame: verticalLineLabelFrame)
        verticalLineLabel.text = ""
        verticalLineLabel.font = UIFont.init(name: "Dosis-Regular", size: 20)
        verticalLineLabel.textAlignment = NSTextAlignment.left
        verticalLineLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        verticalLineLabel.lineBreakMode = NSLineBreakMode.byClipping
        if(isLastMatch != true){
            matchesScrollView.addSubview(verticalLineLabel)
        }
        
        let rightBorder = CALayer()
        rightBorder.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3).cgColor
        rightBorder.frame = CGRect(x: verticalLineLabel.frame.width-8, y: 70, width: 1, height: verticalLineLabel.frame.height)
        verticalLineLabel.layer.addSublayer(rightBorder)
        
        
        let imgUrlLogoHome = self.teamLogoBaseUrl.replacingOccurrences(of: "{TEAMID}", with: "\(item.HomeTeamId)")
        let imgHome = Utility.GetCachedImage("matches_team_" + String(item.HomeTeamId), url: imgUrlLogoHome)
        if (imgHome != nil) {
            let imgView = UIImageView(image: Utility.newImageFromImage(imgHome!, scaledToSize: CGSize(width: imagesWidth, height: imagesHeight), withAplha: 1))
            imgView.frame = imagesTeamHomeFrame
            //imgView.adjustsImageWhenAncestorFocused = true
            imgView.contentMode = .scaleAspectFill
            imgView.layer.cornerRadius = imgView.frame.size.width/2
            imgView.clipsToBounds = true
            button.addSubview(imgView)
        }
        
        let nameTeamHomeLabel = UILabel(frame: labelTeamHomeFrame)
        var home: String?
        if translationData?.teams[String(item.HomeTeamId)] != nil{
            home =  translationData?.teams[String(item.HomeTeamId)]?.name
        }else{
            home = item.HomeTeamName
        }
        nameTeamHomeLabel.text = home!.uppercased()
        nameTeamHomeLabel.font = UIFont.init(name: "Dosis-Medium", size: 20)
        nameTeamHomeLabel.textAlignment = NSTextAlignment.left
        nameTeamHomeLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        nameTeamHomeLabel.lineBreakMode = NSLineBreakMode.byClipping
        button.addSubview(nameTeamHomeLabel)
        
        let imgUrlLogoAway = self.teamLogoBaseUrl.replacingOccurrences(of: "{TEAMID}", with: "\(item.AwayTeamId)")
        let imgAway = Utility.GetCachedImage("matches_team_" + String(item.AwayTeamId), url: imgUrlLogoAway)
        if (imgAway != nil) {
            let imgView = UIImageView(image: Utility.newImageFromImage(imgAway!, scaledToSize: CGSize(width: imagesWidth, height: imagesHeight), withAplha: 1))
            imgView.frame = imagesTeamAwayFrame
            //imgView.adjustsImageWhenAncestorFocused = true
            imgView.contentMode = .scaleAspectFill
            imgView.layer.cornerRadius = imgView.frame.size.width/2
            imgView.clipsToBounds = true
            button.addSubview(imgView)
        }
        
        let nameTeamAwayLabel = UILabel(frame: labelTeamAwayFrame)
        var away: String?
        if translationData?.teams[String(item.AwayTeamId)] != nil{
            away =  translationData?.teams[String(item.AwayTeamId)]?.name
        }else{
            away = item.AwayTeamName
        }
        
        nameTeamAwayLabel.text = away!.uppercased()
        nameTeamAwayLabel.font = UIFont.init(name: "Dosis-Medium", size: 20)
        nameTeamAwayLabel.textAlignment = NSTextAlignment.left
        nameTeamAwayLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        nameTeamAwayLabel.lineBreakMode = NSLineBreakMode.byClipping
        button.addSubview(nameTeamAwayLabel)
        
        let timeStr = String(format: "%02d",datetime.hour!) + String(":") + String(format: "%02d",datetime.minute!)
        
        let timeScoreLabel = UILabel(frame: timeScoreLabelFrame)
        timeScoreLabel.tag = 2000
        timeScoreLabel.text = String("\(timeStr)").uppercased()
        timeScoreLabel.font = UIFont.init(name: "Dosis-Bold", size: 20)
        timeScoreLabel.textAlignment = NSTextAlignment.left
        timeScoreLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        timeScoreLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        timeScoreLabel.numberOfLines = 0
        button.addSubview(timeScoreLabel)
        
        let statusLabel = UILabel(frame: statusLabelFrame)
        statusLabel.tag = 3000
        statusLabel.font = UIFont.init(name: "Dosis-Bold", size: 20)
        statusLabel.textAlignment = NSTextAlignment.center
        statusLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        statusLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        statusLabel.numberOfLines = 0
        button.addSubview(statusLabel)
        
        drawMatchStatus(button, item: item)
        
        matchesScrollView.addSubview(button)
        
        
    }
    
    func drawMatchStatus(_ button: CustomButton,item: Match){
        let timeScoreLabel: UILabel = (button.viewWithTag(2000) as? UILabel)!
        let statusLabel: UILabel = (button.viewWithTag(3000) as? UILabel)!

        statusLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        statusLabel.layer.cornerRadius = 7
        statusLabel.layer.masksToBounds = true
        //statusLabel.backgroundColor = nil
        
        let status: Match.Status = item.getStatus(videoDataUrl)
        
        if(status == Match.Status.live){
            statusLabel.alpha = 1
            statusLabel.text = "LIVE"
            statusLabel.backgroundColor = UIColor.red
            timeScoreLabel.text = String(item.Results.HomeGoals) + String("-")+String(item.Results.AwayGoals)
            button.isUserInteractionEnabled = true
            button.addTarget(self, action: #selector(self.handleMatchButton(_:)), for: UIControlEvents.allEvents)
        }else if(status == Match.Status.replay){
            statusLabel.alpha = 1
            statusLabel.text = "REPLAY"
            timeScoreLabel.text = String(item.Results.HomeGoals) + String("-")+String(item.Results.AwayGoals)
            button.isUserInteractionEnabled = true
            button.addTarget(self, action: #selector(self.handleMatchButton(_:)), for: UIControlEvents.allEvents)
        }else if(status == Match.Status.future){
            statusLabel.alpha = 0
            button.isUserInteractionEnabled = false
        }

        
    }
    
    func updateMatches(){
        self.matchesCount = 0
        self.firstMatchID = DataProvider.sharedInstance.oldSchedules.Matches[0].MatchId
        
        if(self.matchesScrollView.subviews.count>0 && self.firstMatchID == DataProvider.sharedInstance.newSchedules.Matches[0].MatchId){
            //for item:Match in _schedule.Matches {
            let matches = DataProvider.sharedInstance.newSchedules.Matches
            for i in 0..<matches.count {
                let item = matches[i]
                
                if(self.matchesScrollView.viewWithTag(item.MatchId) != nil){
                    
                    let button: CustomButton = (self.matchesScrollView.viewWithTag(item.MatchId) as? CustomButton)!
                    if(self.matchesCount == 0){
                        self.firstMatchID = item.MatchId
                        
                        
                        let fontTitle = UIFont.init(name: "Dosis-SemiBold", size: 32)
                        let title = NSMutableAttributedString(string: "MATCHDAY \(item.MatchDay): ", attributes: [NSFontAttributeName: fontTitle!])
                        
                        
                        let userCalendar = Calendar.current
                        let hourMinuteComponents: NSCalendar.Unit = [.year, .month, .day]
                        let datetime = (userCalendar as NSCalendar).components(
                            hourMinuteComponents,
                            from: Date())
                        
                        let df: DateFormatter = DateFormatter()
                        let monthName: String = df.monthSymbols[(datetime.month! - 1)].uppercased()
                        
                        let fontDate = UIFont.init(name: "Dosis-Regular", size: 32)
                        let date = NSMutableAttributedString(string: "\(datetime.day!) \(monthName) \(datetime.year!)", attributes: [NSFontAttributeName: fontDate!])
                        
                        let result = NSMutableAttributedString()
                        result.append(title)
                        result.append(date)
                        
                        self.titleMatchesLabel.attributedText = result
                    }
                    
                    self.drawMatchStatus(button,item: item)
                    self.matchesCount += 1
                }
            }
        }else{
            drawMatches()
            //_ = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(self.drawMatches), userInfo: nil, repeats: false)
        }
    }
    
    
    /*
     //----- REFRESH SCORE --------
     
     
     if(self.actualMatchObj != nil){
     var scoreAway: String? = String(self.actualMatchObj!.Results.AwayGoals)
     var scoreHome: String? = String(self.actualMatchObj!.Results.HomeGoals)
     
     if (scoreHome == nil) {
     scoreHome = "0"
     }
     if (scoreAway == nil) {
     scoreAway = "0"
     }
     
     //let fontTeam = UIFont.init(name: "Dosis-SemiBold", size: 40)
     //let fontPlayers = UIFont.init(name: "Dosis-Medium", size: 36)
     let home: String? =  self.translationData?.teams[String(self.actualMatchObj!.HomeTeamId)]?.name
     let away: String? =  self.translationData?.teams[String(self.actualMatchObj!.AwayTeamId)]?.name
     let scoreTeam = home!.uppercaseString + "  " + scoreHome! + " - " + scoreAway! + "  " + away!.uppercaseString
     //let scorePlayers = NSMutableAttributedString(string: "FERNANDO (RMA) 20'", attributes: [NSFontAttributeName: fontPlayers!, NSForegroundColorAttributeName: UIColor.lightGrayColor()])
     //scoreTeam.appendAttributedString(scorePlayers)
     self.scoreLabel.font = UIFont.init(name: "Dosis-SemiBold", size: 40)
     self.scoreLabel.text = scoreTeam
     
     
     */
    
    
    func handleMatchButton(_ sender: UIButton!) {
        
        NSLog("MatchButton tag --> " + String(sender.tag))
        
        
        let matchId = sender.tag
        
        let newMatch = self.schedule.getMatch(matchId)
        
        
        var videoId = 0
        if(newMatch!.isLive()){
            videoId = newMatch!.Videos.Live[0].VideoId
        }else if(newMatch!.isScheduled()){
            videoId = newMatch!.Videos.Scheduled[0].VideoId
        }else if(newMatch!.isReplay()){
            videoId = newMatch!.Videos.Replay[0].VideoId
        }
        
        if(videoId>0){
            let videoData = VideoDataController().getVideoData(videoDataUrl, videoId: String(videoId))
            if (videoData.videoUrl.isEmpty == false) {
                
                if(selectedSecondPlayerMatchID > -1 && selectedSecondPlayerMatchID != matchId){
                    selectedSecondPlayerMatchID = matchId
                    
                    self.delegate?.matchSelected(videoId, videoUrl: videoData.videoUrl, transform: false, newMatch: newMatch!)
                    
                }else if(selectedSecondPlayerMatchID != matchId){
                    selectedSecondPlayerMatchID = matchId
                    
                    self.delegate?.matchSelected(videoId, videoUrl: videoData.videoUrl, transform: true, newMatch: newMatch!)
                    
                    
                }else{
                    
                    
                    selectedSecondPlayerMatchID = -1
                    
                    self.delegate?.goToNewMatch(newMatch!, videoData: videoData)
                    
                }
            }}
    }
    
    
    
    // MARK: -
    
    // MARK: Notifications data ready
    func handleMatches(_ notification: Notification){
        if(String(describing: notification.name) == "matchesDataReady"){
            NSLog("UPDATE MATCHES")
            updateMatches()
        }
    }
}


