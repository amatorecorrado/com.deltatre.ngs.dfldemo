//
//  VideoClipModel.swift
//  AppleTV
//
//  Created by Corrado Amatore on 14/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public struct VideoClipModel{
    
    public var UrlMP4: String?
    public var UrlHLS: String?
    public var ID: String?
    public var operation: String?
    public var version: String?
    public var VideoFeed: String?
    public var Target: String?
    public var Duration: String?
    public var Codec: String?
    public var Bitrate: String?
    public var Language: String?
    public var ThumbnailUrl: String?
    public var Camera: CameraModel?
    
    public init(){
        Camera = CameraModel()
    }
}
