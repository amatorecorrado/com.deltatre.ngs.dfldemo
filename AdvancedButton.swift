//
//  AdvancedButton.swift
//  AppleTV
//
//  Created by Corrado Amatore on 15/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation


import Foundation
import UIKit


class AdvancedButton: UIButton
{
    var IDString: String = ""
    internal var initialBackgroundColour: UIColor!
    fileprivate var group = UIMotionEffectGroup()
    fileprivate var _backgroundColorSelected: UIColor! = UIColor.black.withAlphaComponent(0)
    fileprivate var _alphaColorSelected: Float! = 0.0
    
    func backgroundColorSelected(_ SelectedColor: UIColor, Alpha: Float){
        _backgroundColorSelected = SelectedColor
        _alphaColorSelected = Alpha
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        coordinator.addCoordinatedAnimations(
            {
                if self.isFocused
                {
                    let back = self._backgroundColorSelected.withAlphaComponent(CGFloat(self._alphaColorSelected))                    
                    self.backgroundColor = back
                    
                    let amount = 10
                    
                    let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
                    horizontal.minimumRelativeValue = -amount
                    horizontal.maximumRelativeValue = amount
                    
                    let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
                    vertical.minimumRelativeValue = -amount
                    vertical.maximumRelativeValue = amount
                    
                    
                    self.group.motionEffects = [horizontal, vertical]
                    self.addMotionEffect(self.group)
                    
                }
                else
                {
                    self.backgroundColor = self.initialBackgroundColour
                    self.removeMotionEffect(self.group)
                }
            },
            completion: nil)
    }

    
}
