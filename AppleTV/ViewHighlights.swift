//
//  ViewHighlights.swift
//  AppleTVNative
//
//  Created by Federico Bortoluzzi on 04/05/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit
import Kingfisher
import VideoPlayerFeatured

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class ViewHighlights: BaseViewController {
    
    //UIVIEWS
    let initView = UIView()
    let featuredScrollView = UIScrollView()
    let verticalScrollView = UIScrollView()
    let spinnerSplash = UIActivityIndicatorView(activityIndicatorStyle: .white)
    var splashImageView = UIImageView()
    let screenRect = UIScreen.main.bounds
    //var translationData: Translations?
    
    var HIGHLIGHTS_MAX_ITEMS:Int = 30
    var lastmatchid = 0

    var videos: [VideoModel]? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ngs_analytics.setView("HighlightsView")
        //GATracker.sharedInstance.screenView("HighlightsView", customParameters: nil)
        
        //self.view.userInteractionEnabled = true
        
        self.view.isUserInteractionEnabled = true
        self.view.backgroundColor = UIColor(red: 10/255.0, green: 59/255.0, blue: 107/255.0, alpha: 1)

        
        initView.frame = self.view.layer.bounds
        initView.isUserInteractionEnabled = true
        loadImages()
        let splashSpinnerFrame = CGRect(x: screenRect.width/2 - 50, y: ((screenRect.height/5)*4), width: 100, height: 100)
        spinnerSplash.frame = splashSpinnerFrame
        spinnerSplash.center = self.view.center
        spinnerSplash.startAnimating()
        
        initView.addSubview(spinnerSplash)
        self.view.addSubview(initView)
//        if videos?.count == 0 {
//            self.load()
//        }
//        else {
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) {
//                self.load()
//                self.measureEnd()
//            }
//        }
        
        DispatchQueue.main.async() {
            self.load()
            self.measureEnd()
        }

    }
    
    func load(){

        // PRELOAD ON HOME VIEW
        self.measureEnd()
        
        if(videos == nil) {
            let videoController = VideoController(_urlVideoList: self.viewConfiguration!.feeds["feedVideosByCategory"]!, _urlMatch: self.viewConfiguration!.feeds["feedMatch"]!, _feedVideoData: self.viewConfiguration!.feeds["feedVideodata"]!, _cupCode: self.viewConfiguration!.parameters["cupCode"]!, _season: self.viewConfiguration!.parameters["season"]!)
            videos = videoController.getVideoList(maxItems: HIGHLIGHTS_MAX_ITEMS)
        }
        
        self.measureEnd()
        
        let verticalScrollViewFrame = CGRect(x: 0, y: 0, width: screenRect.width, height: screenRect.height)
        
        verticalScrollView.frame = verticalScrollViewFrame
        //verticalScrollView.layoutMargins = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
        verticalScrollView.autoresizingMask = UIViewAutoresizing.flexibleWidth
        verticalScrollView.isScrollEnabled = true
        verticalScrollView.clipsToBounds = false
        
        
        let imgBgFrame = CGRect(x: 0, y: 0, width: 1920, height: 800)
        
        let img = Utility.GetCachedImage("highlights_featured_backgroundarea", url: self.viewConfiguration!.images.getValue("background_third"))

        let imgBgView = UIImageView(frame: imgBgFrame)
        imgBgView.image = img
        //imgBgView.layer.zPosition = 5
        imgBgView.contentMode = .topLeft
        imgBgView.tag = 100
        //verticalScrollView.addSubview(imgBgView)
        //initView.addSubview(imgBgView)
        
        
        let titleLabelFrame = CGRect(x: -325, y: 745, width: 1000, height: 50)
        let titleLabel = UILabel(frame: titleLabelFrame)
        
        if let title = self.viewConfiguration?.parameters.getValue("title"){
            titleLabel.text = title.uppercased()
        }else{
            titleLabel.text = String("Highlights").uppercased()
        }
        
        titleLabel.font = UIFont.init(name: "Dosis-Bold", size: 40)
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.textColor = (self.viewConfiguration!.parameters["skin"] != nil && self.viewConfiguration!.parameters["skin"]! == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        titleLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        titleLabel.numberOfLines = 0
        verticalScrollView.addSubview(titleLabel)
        
        let featuredScrollViewFrame = CGRect(x: 0, y: 0, width: screenRect.width, height: 725)
        featuredScrollView.frame = featuredScrollViewFrame
        featuredScrollView.autoresizingMask = UIViewAutoresizing.flexibleWidth
        featuredScrollView.isScrollEnabled = true
        featuredScrollView.clipsToBounds = false
       
        
        DispatchQueue.main.async { 
            self.drawRowsHighlights()
            
            if(self.spinnerSplash.isAnimating){
                self.spinnerSplash.stopAnimating()
            }
            
            
            self.verticalScrollView.alpha = 0.0
            self.initView.addSubview(self.verticalScrollView)
            UIView.animate(withDuration: 0.2, animations: {
                self.verticalScrollView.alpha = 1.0
            })
            
            self.view.addSubview(self.initView)
            self.splashImageView.removeFromSuperview()
        }
    }
    
    func drawRowsHighlights(){
        
    //FEATURED ROW
        var until = 3
        if let videos = videos, videos.count < 3 {
            until = videos.count
        }
        let videofeatured = videos![0..<until]
        var i = 0
        for  videof in videofeatured{
            drawFeaturedVideo(videof, index: i)
            i += 1
        }
    
        featuredScrollView.contentSize = CGSize(width: (1800*3), height: 700)
//        let contentOffsetX = CGFloat(featuredScrollView.contentSize.width/2) - (featuredScrollView.bounds.size.width/2)
        featuredScrollView.contentOffset = CGPoint(x: 2420,y: 0)
        
        featuredScrollView.alpha = 0.0
        verticalScrollView.addSubview(featuredScrollView)
        UIView.animate(withDuration: 0.2, animations: {
            self.featuredScrollView.alpha = 1.0
        }) 
        
    //HIGHLIGHTS ROWS
        var videohighlights: [VideoModel] = []
        if let videos = videos, videos.count > 3{
            videohighlights = Array(videos[3...videos.count-1])
        }else{
            videohighlights = videos!
        }
        
        var j = 0
        for  videoh in videohighlights{
            drawVideoItem(videoh, index: j)
            j += 1
        }
        
        var yShift = Int(j / 5) + 1
        if (j % 5) > 0
        {
            yShift = yShift + 1
        }
        
        verticalScrollView.contentSize = CGSize(width: screenRect.width, height: CGFloat((yShift * 220) + 820))
        verticalScrollView.contentOffset = CGPoint(x: 0,y: 0)
    
    }
    
    func drawFeaturedVideo(_ video: VideoModel, index: Int){
        
        let imagesWidth = 1700
        let imagesHeight = 620
        
        let xShift = index * (imagesWidth + 50)
        
        let buttonFrame = CGRect(x: xShift + 50, y: 40, width: imagesWidth+30, height: 650)
        let imagesFrame = CGRect(x: 30, y: 15, width: imagesWidth, height: imagesHeight)
        //let titleFrame = CGRect(x: 0, y: 470, width: imagesWidth+30, height: 100)
        
        let button = HighlightButton()
        button.frame = buttonFrame
        button.tag = index
        button.selectionPercentageShadow = 2
        button.selectionPercentageScale = 3
        button.selectionMotionEffectAmount = 2
        button.adjustsImageWhenHighlighted = true
        button.addTarget(self, action: #selector(self.watchVideo(_:)), for: UIControlEvents.allEvents)
        
        let imgView = HighlightImage(frame: imagesFrame)
        //let imgUrl = self.viewConfiguration!.images.getValue("background_second") + String(index + 1) + ".png"
        //let img = Utility.GetCachedImage("highlights_featured_default_" + String(index), url: video.video_thumb_large!)

//        if(img != nil){
//            imgView.image = Utility.newImageFromImage(img!, scaledToSize: CGSize(width: imagesWidth, height: imagesHeight), withAplha: 1)
//        }
        var thumb_url = ""

        if let large = video.video_thumb_large, large.count>0{
            thumb_url = large
        }else if let medium = video.video_thumb, medium.count>0{
            thumb_url = medium
        }else if let small = video.video_thumb_small, small.count>0{
            thumb_url = small
        }
        
        let url = URL(string: thumb_url)
        imgView.kf.setImage(with: url)
        
        imgView.adjustsImageWhenAncestorFocused = true
        imgView.layer.shadowColor = UIColor.black.cgColor
        imgView.layer.shadowOffset = CGSize.zero
        imgView.layer.shadowRadius = 7
        imgView.layer.shadowOpacity = 0.6
        imgView.contentMode = .scaleAspectFill
        imgView.tag = 100
        button.addSubview(imgView)
        
//        let titleLabel = UILabel(frame: titleFrame)
//        if(translationData != nil && translationData?.teams != nil){
//            let teamHome: String! = translationData?.teams[String(video.match.HomeTeamId)]!.name
//            let teamAway: String! = translationData?.teams[String(video.match.AwayTeamId)]!.name
//            let titleStr =  teamHome + " " + String(video.match.Results.HomeGoals) + " - " + String(video.match.Results.AwayGoals) + " " + teamAway
//            titleLabel.text = titleStr
//        }else{
//            let titleStr = String(video.match.HomeTeamName) + " " + String(video.match.Results.HomeGoals) + " - " + String(video.match.Results.AwayGoals) + " " + String(video.match.AwayTeamName)
//            titleLabel.text = titleStr
//        }
        //titleLabel.text = String("England 1 - 2 Island").uppercaseString
        //let attributes = [
//            NSBackgroundColorAttributeName : UIColor.whiteColor(),
//            NSStrokeColorAttributeName : UIColor.blackColor(),
//            NSStrokeWidthAttributeName : 3.0]
//        
//        var title = NSAttributedString(string: titleStr.uppercaseString, attributes: attributes) //1
//        titleLabel.attributedText = title
//        titleLabel.font = UIFont.init(name: "Dosis-Bold", size: 50)
//        titleLabel.textAlignment = NSTextAlignment.Center
//        titleLabel.layer.shadowColor = UIColor.blackColor().CGColor
//        titleLabel.layer.shadowOffset = CGSizeZero
//        titleLabel.layer.shadowRadius = 3
//        titleLabel.layer.shadowOpacity = 0.9
//        titleLabel.textColor = (self.viewConfiguration!.parameters["skin"] != nil && self.viewConfiguration!.parameters["skin"]! == "light") ? UIColor.init(red: 51, green: 51, blue: 51, alpha: 100) : UIColor.whiteColor()
//        titleLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
//        titleLabel.numberOfLines = 0
        //button.addSubview(titleLabel)
        
        featuredScrollView.addSubview(button)
    }
    
    
    
    func drawVideoItem(_ video: VideoModel, index: Int){
//        
//        if(lastmatchid == video.match_id){
//            return
//        }else{
//            lastmatchid = video.match_id!
//        }
        
        let imagesWidth = 300
        let imagesHeight = 170
        
        let xIndex = index % 5
        
        let xShift = xIndex * (imagesWidth + 70)
        //if(xIndex == 0){
        //    xShift = xShift + 50
        //}
        
        let yShift = Int(index / 5) * (imagesHeight + 90)

        
        let buttonFrame = CGRect(x: xShift + 70, y: yShift + 820, width: imagesWidth + 50, height: imagesHeight + 60)
        let imagesFrame = CGRect(x: 10, y: 10, width: imagesWidth, height: imagesHeight)
        let titleFrame = CGRect(x: 10, y: imagesHeight + 20, width: imagesWidth, height: 50)
        let scoreFrame = CGRect(x: 135, y: imagesHeight - 62, width: imagesWidth, height: 50)

        
        let button = HighlightButton()
        if self.viewConfiguration!.parameters["skin"] != nil {
            button.skin = self.viewConfiguration!.parameters["skin"]!
        }
        button.frame = buttonFrame
        button.tag = index
        button.selectionPercentageScale = 7
        button.adjustsImageWhenHighlighted = true
        button.addTarget(self, action: #selector(self.watchVideo(_:)), for: UIControlEvents.allEvents)
        
        let imgView = HighlightImage(frame: imagesFrame)
        //let imgView = UIImageView(frame: imagesFrame)
        
        //let imgUrl = NSURL(string: "http://feedpublisher-core.ngs.deltatre.net/thumbs/2018002.jpg")

        //let imgUrl = String(self.viewConfiguration!.images["thumbMatch"]!).replacingOccurrences(of: "{MATCHID}", with: "\(video.match_id!)")
        
        let url = URL(string: video.video_thumb!)
        imgView.kf.setImage(with: url)
        /*
        let img = Utility.GetCachedImage("highlights_matchthumb_" + String(video.match_id!), url: imgUrl)
        if(img != nil){
            
            imgView.image = Utility.newImageFromImage(img!, scaledToSize: CGSize(width: imagesWidth, height: imagesHeight), withAplha: 1)
        }
         */
        imgView.adjustsImageWhenAncestorFocused = true
        imgView.contentMode = .scaleAspectFill
        imgView.tag = 100
        button.addSubview(imgView)
        
        let scoreLabel = UILabel(frame: scoreFrame)
        scoreLabel.tag = 1001
        let score = String(video.match.Results.HomeGoals) + " - " + String(video.match.Results.AwayGoals)
        scoreLabel.text = score
        scoreLabel.font = UIFont.init(name: "Dosis-Bold", size: 30)
        scoreLabel.textAlignment = NSTextAlignment.left
        scoreLabel.textColor = (self.viewConfiguration!.parameters["skin"] != nil && self.viewConfiguration!.parameters["skin"]! == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        scoreLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        scoreLabel.numberOfLines = 0
        //button.addSubview(scoreLabel)
        
        let titleLabel = UILabel(frame: titleFrame)
        titleLabel.tag = 1000
        //titleLabel.text = String("Italy 2 - 0 Spain")
        //let title = String(video.match.HomeTeamName) + " " + String(video.match.Results.HomeGoals) + " - " + String(video.match.Results.AwayGoals) + " " + String(video.match.AwayTeamName)
        
        let teamHome: String! = video.match.HomeTeamName
        let teamAway: String! = video.match.AwayTeamName
        let title = teamHome + " vs " + teamAway
        titleLabel.text = video.video_title
        
        titleLabel.font = UIFont.init(name: "Dosis-Medium", size: 24)
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.textColor = (self.viewConfiguration!.parameters["skin"] != nil && self.viewConfiguration!.parameters["skin"]! == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        titleLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        titleLabel.numberOfLines = 0
        button.addSubview(titleLabel)

        verticalScrollView.addSubview(button)
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    @objc func watchVideo(_ sender: UIButton!) {
        NSLog("watchVideo tag --> " + String(sender.tag))
        
        let index = sender.tag
        var video = videos![index]
        let viewPlayer = ViewPlayerSimple()
//        viewPlayer.MatchID = video.match_id
        
        let videoidstr: String = String(video.video_id!)
        video.videoData = VideoDataController().getVideoData(self.viewConfiguration!.feeds["feedVideodata"]!, videoId: videoidstr)
        
        viewPlayer.VideoUrl = video.videoData.videoUrl
        
        ConfiguratorController.sharedInstance.videoPlayerSimple = viewPlayer
        let urlString = viewConfiguration?.actions["player_simple"]
        if(urlString != nil){
            ConfiguratorController.sharedInstance.action(urlString!,additionalData: nil)
        }
//        viewPlayer.SeekTime = "0"
//        viewPlayer.MatchId = video.match_id!
//        viewPlayer.VideoStartTime = item.VideoStartTime!
//        viewPlayer.roundId = self.roundId
//        viewPlayer.matchDay = self.matchDay
//        viewPlayer.session = self.session
        
        //self.presentViewController(viewPlayer, animated: true, completion: nil)
        
    }
    
    func loadImages() {
//        let splashFrame = CGRect(x: 0, y: 0, width: screenRect.width, height: screenRect.height)
//        splashImageView = HighlightImage(frame: splashFrame)
//        
//        let splashImage = ImagesUtility.sharedInstance.getImage(self.viewConfiguration!.images.getValue("backgroundSplashScreen"))
//        if splashImage == nil {
//            let imgUrl = NSURL(string: self.viewConfiguration!.images.getValue("backgroundSplashScreen"))
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
//                let imgData = NSData(contentsOfURL: imgUrl!)
//                if(imgData != nil){
//                    let img = UIImage(data: imgData!)
//                    if(img != nil){
//                        dispatch_async(dispatch_get_main_queue()) {
//                            self.splashImageView.image = img!
//                            self.initView.addSubview(self.splashImageView)
//                        }
//                    }
//                }
//            }
//        }
//        self.splashImageView.image = splashImage!
//        self.initView.addSubview(self.splashImageView)
        
        let imgBgFrame = CGRect(x: 0, y: 0, width: 1920, height: 1080)
        let backgroundImage = ImagesUtility.sharedInstance.getImage(self.viewConfiguration!.images.getValue("background"))
//        if backgroundImage == nil {
//            backgroundImage = Utility.GetCachedImage("app_background_matchesview", url: self.viewConfiguration!.images.getValue("background"))
//        }
        let imgBgView = UIImageView(frame: imgBgFrame)
        imgBgView.image = backgroundImage
        imgBgView.contentMode = .scaleToFill
        imgBgView.tag = 100
        initView.addSubview(imgBgView)
    }
}
