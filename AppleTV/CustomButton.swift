//
//  CustomButton.swift
//  AppleTV
//
//  Created by Corrado Amatore on 08/06/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit


class CustomButton: UIButton
{
    internal var initialBackgroundColour: UIColor!
    fileprivate var group = UIMotionEffectGroup()
    fileprivate var _backgroundColorSelected: UIColor! = UIColor.black
    fileprivate var _alphaColorSelected: Float! = 0.3
    
    func backgroundColorSelected(_ SelectedColor: UIColor, Alpha: Float){
        _backgroundColorSelected = SelectedColor
        _alphaColorSelected = Alpha
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        coordinator.addCoordinatedAnimations(
            {
                if self.isFocused
                {
                    UIView.animate(withDuration: 0.6 ,
                        animations: {
                            let scale = 1 + (CGFloat(0.01) * CGFloat(8))
                            let shadow = CGFloat(0.01) * CGFloat(8)
                            
                            self.transform = CGAffineTransform(scaleX: scale, y: scale)
                            self.layer.shadowOffset = CGSize(width: 0,height: (self.frame.height * shadow))
                            self.layer.shadowRadius = 7
                            self.layer.shadowOpacity = 0.7
                        },
                        completion: { finish in
                            
                    })
                    
                    let back = self._backgroundColorSelected.withAlphaComponent(CGFloat(self._alphaColorSelected))
                    self.backgroundColor = back
                    
                    let amount = 10
                    
                    let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
                    horizontal.minimumRelativeValue = -amount
                    horizontal.maximumRelativeValue = amount
                    
                    let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
                    vertical.minimumRelativeValue = -amount
                    vertical.maximumRelativeValue = amount
                    
                    
                    self.group.motionEffects = [horizontal, vertical]
                    self.addMotionEffect(self.group)
                    
                }
                else
                {
                    UIView.animate(withDuration: 0.6, animations: {
                        self.transform = CGAffineTransform.identity
                        self.layer.shadowOffset = CGSize.zero
                        self.layer.shadowRadius = 0
                        self.layer.shadowOpacity = 0
                    })

                    self.backgroundColor = self.initialBackgroundColour
                    self.removeMotionEffect(self.group)
                }
            },
            completion: nil)
    }
}
