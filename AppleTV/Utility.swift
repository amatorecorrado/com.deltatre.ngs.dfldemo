//
//  Utility.swift
//  AppleTVNative
//
//  Created by Federico Bortoluzzi on 27/04/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit
import SystemConfiguration


open class Utility {
    
    static open func newImageFromImage(_ image:UIImage, scaledToSize:CGSize, withAplha: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(scaledToSize, false, 0.0);
        image.draw(in: CGRect(x: 0, y: 0, width: scaledToSize.width, height: scaledToSize.height), blendMode: CGBlendMode.normal, alpha: withAplha)
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
 
    static open func getMatchIdSplitted(_ matchId: Int) -> [String] {
        var matchIdArray: [String] = [String]()
        
        var matchIdString = String(matchId)
        if (matchIdString.characters.count > 5) {
            matchIdArray.append((matchIdString as NSString).substring(to: 2))
            matchIdString = String(matchIdString.characters.dropFirst(2))
            matchIdArray.append((matchIdString as NSString).substring(to: 2))
            matchIdString = String(matchIdString.characters.dropFirst(2))
            matchIdArray.append(matchIdString)
        }
        
        return matchIdArray;
    }
    
    static open func deserializeCupInfo(_ json: Array<Dictionary<String, AnyObject>>, cup: Int, cupSeason: Int, season: Int, roundId: inout Int, matchDay: inout Int, session: inout Int) {
        for item: Dictionary<String, AnyObject> in json {
            if let cupTmp = item["cup"] as? Int, let cupSeasonTmp = item["cupSeason"] as? Int, let seasonTmp = item["season"] as? Int {
                if (cupTmp == cup && cupSeasonTmp == cupSeason && seasonTmp == season) {
                    if let roundIdTmp = item["roundid"] as? Int {
                        roundId = roundIdTmp
                    }
                    if let matchDayTmp = item["matchday"] as? Int {
                        matchDay = matchDayTmp
                    }
                    if let sessionTmp = item["session"] as? Int {
                        session = sessionTmp
                    }
                    break
                }
            }
        }
    }
    
    
    //Image GET from network or memory
    fileprivate static var imagesDictionary: Dictionary<String, UIImage>? = [:]
    
    static open func GetCachedImage(_ key: String, url: String)->UIImage?{
        if(key.isEmpty || key.count == 0 || url.isEmpty || url.count == 0){
            return nil
        }
        if(imagesDictionary != nil && imagesDictionary![key] != nil){
            return imagesDictionary![key]
        }else{
            let imgUrl = URL(string: url)
            let imgData = try? Data(contentsOf: imgUrl!)
            if(imgData != nil){
                let img = UIImage(data: imgData!)
                imagesDictionary![key] = img
                return img
            }
        }
        return nil
    }
    
    static open func ResetChacheImage(){
        imagesDictionary = [:]
    }
    
    
    static open func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        let isReachable = flags == .reachable
        let needsConnection = flags == .connectionRequired
        
        return isReachable && !needsConnection
        
    }
    
//    static func timerStop(_ timer: inout Timer?){
//        if timer != nil {
//            timer!.invalidate()
//            timer = nil
//        }
//    }
    
    static func setDateLabel(_ date: Date) -> String{
        let userCalendar = Calendar.current
        let dateComponents: NSCalendar.Unit = [.year, .month, .day]
        let datetime = (userCalendar as NSCalendar).components(
            dateComponents,
            from: date)
        let month = datetime.month
        
        let df: DateFormatter = DateFormatter()
        let monthName: String = df.monthSymbols[(month! - 1)].uppercased()
        return String(describing: datetime.day!) + " " + monthName + " " + String(describing: datetime.year!)
    }
    
    static func setDateValue(_ date: Date) -> String{
        let userCalendar = Calendar.current
        let hourMinuteComponents: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
        let datetime = (userCalendar as NSCalendar).components(
            hourMinuteComponents,
            from: date)
        if let year = datetime.year, let month = datetime.month, let day = datetime.day {
            return String(year)+String(format: "%02d", month)+String(format: "%02d", day)
        }
        
        return ""
    }
    
    static func setTime(_ date: Date) -> String{
        let userCalendar = Calendar.current
        let hourMinuteComponents: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
        let datetime = (userCalendar as NSCalendar).components(
            hourMinuteComponents,
            from: date)
        
        let timeStr = String(format: "%02d",datetime.hour!) + String(":") + String(format: "%02d",datetime.minute!)
        
        return String("\(timeStr)").uppercased()
    }
    
    func viewWithIDString(_ idstring: String, containerView: UIView) -> UIView?{
        for view in containerView.subviews{
            if let advancedButton = view as? AdvancedButton{
                if(advancedButton.IDString == idstring){
                    return advancedButton
                }
            }else if let advancedButton = view as? HomepageButton{
                if(advancedButton.IDString == idstring){
                    return advancedButton
                }
            }
        }
        return nil
    }
}
