//
//  ViewMagazine.swift
//  AppleTVNative
//
//  Created by Federico Bortoluzzi on 04/05/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit

class ViewMagazine: UIViewController {

    var ngs_analytics = NGSAnalytics()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ngs_analytics.setView("MagazineView")
        // Do any additional setup after loading the view, typically from a nib.
        self.view.isUserInteractionEnabled = true
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "BackgroundMagazine")!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
