//
//  VideoListViewController.swift
//  AppleTV
//
//  Created by Corrado Amatore on 20/11/2017.
//  Copyright © 2017 Deltatre. All rights reserved.
//

import UIKit

class VideoListViewController: BaseViewController {

    var topeditorialImage: UIImageView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var backimage: UIImageView!
    
//    var scroll = UIScrollView()
//    var backimage = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let img_url = viewConfiguration?.parameters.getValue("videolistfake_url")
        let url = URL(string: img_url!)
        let data = try? Data(contentsOf: url!)
        
        if data != nil {
            let image = UIImage( data: data!)
            backimage.image = image;
        }
        self.view.backgroundColor = UIColor(red:30/255.0, green: 1/255.0, blue: 3/255.0, alpha: 1.0)
        

        

        
        
//        let selectRecognizer = UITapGestureRecognizer(target: self, action: #selector(VideoListViewController.selectRecognizer(recognizer:)))
//        selectRecognizer.allowedPressTypes = [NSNumber(value: UIPressType.select.rawValue)]
//        self.view.addGestureRecognizer(selectRecognizer)
        
    }
    
    override func viewDidLayoutSubviews() {
        
        let button1 = UIButton(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 600))
        //button1.isUserInteractionEnabled = true
        button1.setTitleColor(UIColor.red, for: .focused)
        button1.addTarget(self,action:#selector(buttonClicked),
                          for:.allEvents)
        let button2 = UIButton(frame: CGRect(x: 0, y: 1080, width: UIScreen.main.bounds.width, height: 600))
        //button2.isUserInteractionEnabled = true
        button2.setTitleColor(UIColor.red, for: .focused)
        button2.addTarget(self,action:#selector(buttonClicked),
                          for:.allEvents)
        let button3 = UIButton(frame: CGRect(x: 0, y: 2160, width: UIScreen.main.bounds.width, height: 600))
        //button3.isUserInteractionEnabled = true
        button3.setTitleColor(UIColor.red, for: .focused)
        button3.addTarget(self,action:#selector(buttonClicked),
                          for:.allEvents)
        
        let button4 = UIButton(frame: CGRect(x: 0, y: 1800, width: UIScreen.main.bounds.width, height: 600))
        //button4.isUserInteractionEnabled = true
        button4.setTitleColor(UIColor.red, for: .focused)
        button4.addTarget(self,action:#selector(buttonClicked),
                          for:.allEvents)
        
        scroll.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 1080)
        scroll.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        scroll.isScrollEnabled = true
        //scroll.isUserInteractionEnabled = true
        scroll.contentSize = CGSize(width: 1920, height: 2160)
        scroll.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        //scroll.autoresizingMask = .flexibleHeight
        //scroll.isDirectionalLockEnabled = true
        //scroll.clipsToBounds = true
        scroll.addSubview(backimage)
        scroll.addSubview(button1)
        scroll.addSubview(button2)
//        scroll.addSubview(button3)
//        scroll.addSubview(button4)
        //scroll.addSubview(stack)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func buttonClicked(sender:UIButton){
        
        let videoId: String = (self.viewConfiguration?.parameters.getValue("videoid"))!
        //let videoData = VideoDataController().getVideoData((self.viewConfiguration?.feeds["feedVideodata"])!, videoId: videoId)
        let videoData = VideoDataController().getVideoData("http://en.static.ngs.deltatre.net/feed/appletv/apps/hbs_demo/feeds/videos/{VIDEOID}.xml", videoId: videoId)
        if (videoData.videoUrl.isEmpty == false) {
            let matchid: String = (self.viewConfiguration?.parameters.getValue("matchid"))!
            
            var dictAdditionalData: [String: String] = [:]
            dictAdditionalData["videoUrl"] = videoData.videoUrl
            dictAdditionalData["matchId"] = matchid  //String(matchId)
        
            let urlString = self.viewConfiguration?.actions["player"]
            if(urlString != nil){
                ConfiguratorController.sharedInstance.action(urlString!, additionalData: dictAdditionalData as AnyObject)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
