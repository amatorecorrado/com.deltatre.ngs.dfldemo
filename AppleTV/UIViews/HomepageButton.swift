//
//  HomepageButton.swift
//  AppleTV
//
//  Created by Federico Bortoluzzi on 04/07/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

// Subclassed UIButton to have callbacks on focus events
class HomepageButton : UIButton {
    var IDString: String = ""
    var FocusCallback: ((String, Int) -> ())?
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocus(in: context, with: coordinator)
        
        if context.nextFocusedView == self {
            FocusCallback!("focus", self.tag)
        } else {
            FocusCallback!("focusOut", self.tag)
        }
    }
}
