//
//  AkamaiTokenizer.swift
//  AppleTV
//
//  Created by administrator on 26/07/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import CryptoSwift
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



open class AkamaiTokenizer {
    
    fileprivate static var AKAMAI_TIME: String = "http://time.akamai.com/?json"
    fileprivate static var AKAMAI_TOKEN: String     = "st={start}~exp={exp}~acl=/*"
    fileprivate static var AKAMAI_FULL_TOKEN: String = "hdnea=st={start}~exp={exp}~acl=/*~hmac={hmac}"
  
    
 
    open static func hmac (_ secret: String, message: String) -> String {
       
        let string = secret
        let chars = Array(string.characters)
        
        let numbers = stride(from: 0, to: chars.count, by: 2).map {
            UInt8(String(chars[$0 ..< $0+2]), radix: 16) ?? 0
        }
        
        let hmac: Array<UInt8> = try! HMAC(key: numbers, variant: .sha256).authenticate(Array(message.utf8))
        let data = Data(bytes: hmac)
        return data.toHexString()
        
    }
    
    
    open static func tokenize(_ url: String, secret: String) -> String
    {
        
        var time: String = ""
        var start: Int = 0
        var exp: Int = 0
        
        let jsonurl = URL(string: "http://time.akamai.com/?json")!
        let jsonData = try? Data(contentsOf: jsonurl)
        if (jsonData?.count > 0) {
            
            
             time = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue)! as String
             start = Int(time)!
             exp = start + 3600
            
        }

        let halfToken: String = AKAMAI_TOKEN.replacingOccurrences(of: "{start}", with: String(start)).replacingOccurrences(of: "{exp}",with: String(exp))
        
        let  hmacToken: String = hmac(secret,message: halfToken)
        
        let fullToken: String = AKAMAI_FULL_TOKEN.replacingOccurrences(of: "{start}",with: String(start)).replacingOccurrences(of: "{exp}",with: String(exp)).replacingOccurrences(of: "{hmac}",with: hmacToken)
        
        let finalUrl: String = url + "?" + fullToken
        
    return finalUrl
    }
    
    
}
