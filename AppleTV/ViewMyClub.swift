//
//  MyClub.swift
//  AppleTVNative
//
//  Created by Federico Bortoluzzi on 11/05/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit

class ViewMyClub: UIViewController {
    
    var ngs_analytics = NGSAnalytics()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ngs_analytics.setView("MyClubView")
        // Do any additional setup after loading the view, typically from a nib.
        self.view.isUserInteractionEnabled = true
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "BackgroundMyClub")!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
