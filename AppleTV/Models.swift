//
//  Models.swift
//  AppleTVNative
//
//  Created by Federico Bortoluzzi on 22/04/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


public struct Camera{
    public var ID: String?
    public var Code: String?
    public var Name: String?
    
    public init() {
        ID = nil
        Code = nil
        Name = nil
    }
    
    public init(ID: String, Code: String, Name: String) {
        self.ID = ID
        self.Code = Code
        self.Name = Name
    }
    
    func getCameraFromCode(_ code: String, cameras: [Camera]) -> (Camera){
        for cam in cameras {
            if(cam.Code ==  code){
                return cam
            }
        }
        return Camera()
    }
}

public struct ModelPlayByPlay {
    public var Score: ModelScoreItem?
    public var Items: [ModelPlayByPlayItem]?
    
    public init() {
        //Score = model
        Items = [ModelPlayByPlayItem]()
    }
    
    public init(score: ModelScoreItem, items: [ModelPlayByPlayItem]) {
        Score = score
        Items = items
    }
    
    public func getItemFromID(_ id: Int) -> (ModelPlayByPlayItem?){
        let res = ModelPlayByPlayItem()
        for item in self.Items! {
            if(item.Id == String(id)){
                return item
            }
        }
        return res
    }
    
    func getOnlyMulticam() -> (ModelPlayByPlay){
        var model = ModelPlayByPlay()
        model.Score = self.Score;
        for item in self.Items! {
            if(item.IsMulticam()){
                model.Items?.append(item)
            }
        }
        return model
    }
    
    static public func deserialize(_ json: Dictionary<String, AnyObject>, multiangleCameraBaseUrl: String, multiangleCameraCDN: String, vocabularyUrl: String, camerasUrl: String) -> (ModelPlayByPlay) {
        var results = ModelPlayByPlay()
        results.Items = [ModelPlayByPlayItem]()
        //var results: [ModelPlayByPlayItem] = []
        //var score: ModelScoreItem
        
        var dictEventType = Dictionary<String, AnyObject>()
        let jsonVoc = try? Data(contentsOf: URL(string: vocabularyUrl)!)
        if (jsonVoc?.count > 0) {
            do {
                let json = try JSONSerialization.jsonObject(with: jsonVoc!, options: .allowFragments) as? [String: Any]
                for item in (json?["events"] as? [Dictionary<String, AnyObject>])!{
                    
                    let key = item["key"] as! String
                    let text = item["text"] as! String
                    dictEventType[key] = text as AnyObject
                }
                
            } catch {
                NSLog("error serializing JSON dictionary events type: \(error)")
            }
        }
        var cameras = [Camera]()
        let jsonCameras = try? Data(contentsOf: URL(string: camerasUrl)!)
        if (jsonCameras?.count > 0) {
            do {
                let json = try JSONSerialization.jsonObject(with: jsonCameras!, options: .allowFragments) as? [String: Any]
                for item in (json?["cameras"] as? [Dictionary<String, AnyObject>])!{
                    let ID = item["id"] as! String
                    let Name = item["name"] as! String
                    let Code = item["code"] as! String
                    cameras.append(Camera(ID: ID, Code: Code, Name: Name))
                }
                
            } catch {
                NSLog("error serializing JSON dictionary events type: \(error)")
            }
        }
        
        var lastScoreDateTime = Date.init(timeIntervalSince1970: TimeInterval.init(1))
        
        for item in (json["c"] as? [Dictionary<String, AnyObject>])! {
            if let i = item["i"] as? Dictionary<String, AnyObject> {
                
                if (i["Type"] as? String == "SRM") {
                    if let body = i["Body"] as? Dictionary<String, AnyObject> {
                        var scoreA = ""
                        var scoreB = ""
                        var opponentNameA = ""
                        var opponentNameB = ""
                        var opponentCodeA = ""
                        var opponentCodeB = ""
                        var dateTime = Date.init()
                        
                        if let val = body["ScoreA"] as? String {
                            scoreA = val
                        }
                        if let val = body["ScoreB"] as? String {
                            scoreB = val
                        }
                        if let val = body["OpponentNameA"] as? String {
                            opponentNameA = val
                        }
                        if let val = body["OpponentNameB"] as? String {
                            opponentNameB = val
                        }
                        if let val = body["OpponentCodeA"] as? String {
                            opponentCodeA = val
                        }
                        if let val = body["OpponentCodeB"] as? String {
                            opponentCodeB = val
                        }
                        if let val = i["TimeCode"] as? String {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                            
                            dateTime = dateFormatter.date(from: (val as NSString).substring(to: 19))!
                        }
                        
                        if (dateTime.compare(lastScoreDateTime) == ComparisonResult.orderedDescending) {
                            results.Score = ModelScoreItem.init(scoreHome: scoreA, scoreAway: scoreB, homeTeamName: opponentNameA, awayTeamName: opponentNameB, homeTeamId: opponentCodeA, awayTeamId: opponentCodeB, dateTime: dateTime)
                            lastScoreDateTime = dateTime
                        }
                    }
                }

                
                if (i["Type"] as? String == "pbp") {
                    if let body = i["Body"] as? Dictionary<String, AnyObject> {
                        var id = ""
                        var timeCode = ""
                        var eventType = ""
                        var eventTypeName = ""
                        var gameTime = ""
                        var text = ""
                        var thumbnailUrl = ""
                        var videosList = [ModelVideoItem]()
                        
                        if let val = i["Id"] as? String {
                            id = val
                        }
                        if let val = i["TimeCode"] as? String {
                            timeCode = val
                        }
                        if let val = body["GameTime"] as? String {
                            gameTime = val
                        }
                        if let val = body["Type"] as? String {
                            eventType = val
                            if(dictEventType[val] != nil){
                                eventTypeName = dictEventType[val] as! String
                            }
                            
                        }
                        if let val = body["Text"] as? String {
                            text = val
                        }
                        
                        if let val = body["VideoRef"] as? String {
                            if (!val.isEmpty) {
                                let multiangleCameraUrl = URL(string: multiangleCameraBaseUrl.replacingOccurrences(of: "{MULTIANGLEREFID}", with: val))
                                let xmlData = try? Data(contentsOf: multiangleCameraUrl!)
                                if (xmlData?.count > 0) {
                                    let xmlParser = ModelVideoItemXmlParser(cameras: cameras)
                                    xmlParser.startParsing(xmlData!, multiangleCameraCDN: multiangleCameraCDN)
                                    videosList.append(contentsOf: xmlParser.results)
                                    
                                    if (xmlParser.results.count > 0) {
                                        thumbnailUrl = videosList[0].ThumbnailUrl!
                                    }
                                    
                                    
                                }
                            }
                        }
                        let model = ModelPlayByPlayItem(
                            id: id,
                            timeCode:  timeCode,
                            type:  i["Type"] as! String,
                            eventType: eventType,
                            eventTypeName: eventTypeName,
                            gameTime: gameTime,
                            text:  text,
                            thumbnailUrl: thumbnailUrl,
                            videos: videosList
                        )
                        
                        if (eventType != "FoulCommitted") {
                            results.Items!.insert(model, at: 0)
                        }
                    }
                }
            }
        }
        
        return results
    }
}

public struct ModelScoreItem {
    public let ScoreHome: String?
    public let ScoreAway: String?
    public let HomeTeamName: String?
    public let AwayTeamName: String?
    public let HomeTeamId: String?
    public let AwayTeamId: String?
    public let DateTime: Date?
    
    
    public init() {
        ScoreHome = ""
        ScoreAway = ""
        HomeTeamName = ""
        AwayTeamName = ""
        HomeTeamId = ""
        AwayTeamId = ""
        DateTime = Date()
    }
    
    public init(scoreHome: String, scoreAway: String, homeTeamName: String, awayTeamName: String, homeTeamId: String, awayTeamId: String, dateTime: Date) {
        ScoreHome = scoreHome
        ScoreAway = scoreAway
        HomeTeamName = homeTeamName
        AwayTeamName = awayTeamName
        HomeTeamId = homeTeamId
        AwayTeamId = awayTeamId
        DateTime = dateTime
    }
}

public struct ModelPlayByPlayItem {
    public let Id: String?
    public let TimeCode: String?
    public let `Type`: String?
    public let EventyType: String?
    public let EventTypeName: String?
    public let GameTime: String?
    public let Text: String?
    public let ThumbnailUrl: String?
    public let Videos: [ModelVideoItem]?
    static public var Cameras: [Camera]?
    
    public func IsMulticam() ->(Bool){
        return (self.ThumbnailUrl != "" && self.Videos != nil)
  //      return (self.ThumbnailUrl != "" && self.Videos != nil && (self.EventyType == "ShotOnPost" || self.EventyType == "ShotOnBar" || self.EventyType == "ShotOnTarget" || self.EventyType == "ShotWide" || self.EventyType == "Goal" || self.EventyType == "OwnGoal" || self.EventyType == "Substitution" || self.EventyType == "YellowCard" || self.EventyType == "RedCard" || self.EventyType == "Skill" || self.EventyType == "BigChance"))
    }
    
    public init(){
        Id = nil
        TimeCode = nil
        Type = nil
        EventyType = nil
        EventTypeName = nil
        GameTime = nil
        Text = nil
        ThumbnailUrl = nil
        Videos = nil
    }
    
    public init(id: String, timeCode: String, type: String, eventType: String, eventTypeName: String, gameTime: String, text: String, thumbnailUrl: String, videos: [ModelVideoItem]) {
        Id = id
        TimeCode = timeCode
        Type = type
        EventyType = eventType
        EventTypeName = eventTypeName
        GameTime = gameTime
        Text = text
        ThumbnailUrl = thumbnailUrl
        Videos = videos
    }
    
    static public func deserialize(_ json: Dictionary<String, AnyObject>, multiangleCameraBaseUrl: String, multiangleCameraCDN: String, vocabularyUrl: String, cameras: [Camera]) -> ([ModelPlayByPlayItem]) {
        var results: [ModelPlayByPlayItem] = []
        var dictEventType = Dictionary<String, AnyObject>()
        
        let jsonVoc = try? Data(contentsOf: URL(string: vocabularyUrl)!)
        if (jsonVoc?.count > 0) {
            do {
                let json = try JSONSerialization.jsonObject(with: jsonVoc!, options: .allowFragments) as? [String: Any]
                    for item in (json?["events"] as? [Dictionary<String, AnyObject>])!{
                        
                           let key = item["key"] as! String
                           let text = item["text"] as! String
                            dictEventType[key] = text as AnyObject
                    }
                
            } catch {
                NSLog("error serializing JSON dictionary events type: \(error)")
            }
        }
        
        
        for item in (json["c"] as? [Dictionary<String, AnyObject>])! {
            if let i = item["i"] as? Dictionary<String, AnyObject> {
                if (i["Type"] as? String == "pbp") {
                    if let body = i["Body"] as? Dictionary<String, AnyObject> {
                        var id = ""
                        var timeCode = ""
                        var eventType = ""
                        var eventTypeName = ""
                        var gameTime = ""
                        var text = ""
                        var thumbnailUrl = ""
                        var videosList = [ModelVideoItem]()
                        
                        if let val = i["Id"] as? String {
                            id = val
                        }
                        if let val = i["TimeCode"] as? String {
                            timeCode = val
                        }
                        if let val = body["GameTime"] as? String {
                            gameTime = val
                        }
                        if let val = body["Type"] as? String {
                            eventType = val
                            if(dictEventType[val] != nil){
                            eventTypeName = dictEventType[val] as! String
                            }
                            
                        }
                        if let val = body["Text"] as? String {
                            text = val
                        }
                        
                        if let val = body["VideoRef"] as? String {
                            if (!val.isEmpty) {
                                let multiangleCameraUrl = URL(string: multiangleCameraBaseUrl.replacingOccurrences(of: "{MULTIANGLEREFID}", with: val))
                                let xmlData = try? Data(contentsOf: multiangleCameraUrl!)
                                if (xmlData?.count > 0) {
                                    let xmlParser = ModelVideoItemXmlParser(cameras: cameras)
                                    xmlParser.startParsing(xmlData!, multiangleCameraCDN: multiangleCameraCDN)
                                    videosList.append(contentsOf: xmlParser.results)
                                    
                                    if (xmlParser.results.count > 0) {
                                        thumbnailUrl = videosList[0].ThumbnailUrl!
                                    }
                                }
                            }
                        }
                        
//                        if let val = body["ThumbnailURL"] as? String {
//                            thumbnailUrl = val
//                        }
//                        if let videos = body["videos"] as? Dictionary<String, AnyObject> {
//                            for video in (videos["video"] as? [Dictionary<String, AnyObject>])! {
//                                var cameraName = ""
//                                var vthumbnailUrl = ""
//                                var videoUrl = ""
//                                
//                                if let val = video["camId"] as? String {
//                                    cameraName = val
//                                }
//                                if let val = video["thumbnailUrl"] as? String {
//                                    vthumbnailUrl = val
//                                }
//                                if let videoSources = video["videoSources"] as? Dictionary<String, AnyObject> {
//                                    if let videoSource = videoSources["videoSource"] as? Dictionary<String, AnyObject> {
//                                        if let val = videoSource["uri"] as? String {
//                                            videoUrl = val
//                                        }
//                                    }
//                                }
//                                
//                                let model = ModelVideoItem(cameraName: cameraName, thumbnailUrl: vthumbnailUrl, videoUrl: videoUrl)
//                                videosList.append(model)
//                            }
//                        }
                        
                        let model = ModelPlayByPlayItem(
                            id: id,
                            timeCode:  timeCode,
                            type:  i["Type"] as! String,
                            eventType: eventType,
                            eventTypeName: eventTypeName,
                            gameTime: gameTime,
                            text:  text,
                            thumbnailUrl: thumbnailUrl,
                            videos: videosList
                        )
                        
                        if (eventType != "FoulCommitted") {
                            results.insert(model, at: 0)
                            //results.append(model)
                        }
                    }
                }
            }
        }
        
        return results
    }
}

open class ModelVideoItemXmlParser: NSObject, XMLParserDelegate {
    var xmlParser: XMLParser!
    var multiangleCameraCDN = ""
    var results: [ModelVideoItem] = []
    
    var currentParsedElement = ""
    var cameraName = ""
    var thumbnailUrl = ""
    var videoUrl = ""
    var _cameras: [Camera]

    public init(cameras: [Camera]){
        _cameras = cameras
    }
    
    open func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        self.currentParsedElement = elementName
    }
    
    open func parser(_ parser: XMLParser, foundCharacters string: String) {
      
        switch self.currentParsedElement {
        case "camId":
            self.cameraName = string
            break
        case "thumbnailUrl":
            self.thumbnailUrl = string.replacingOccurrences(of: "{n:ngs.domain.clip.p}", with: multiangleCameraCDN)
            break
        case "uri":
            self.videoUrl = string.replacingOccurrences(of: "{n:ngs.domain.clip.p}", with: multiangleCameraCDN)
            break
        default:
            break
        }
        
        
        if (!cameraName.isEmpty && !thumbnailUrl.isEmpty && !videoUrl.isEmpty) {
            self.results.append(ModelVideoItem(cameraName: self.cameraName, thumbnailUrl: self.thumbnailUrl, videoUrl: self.videoUrl, cameras: self._cameras))
            self.cameraName = ""
            self.thumbnailUrl = ""
            self.videoUrl = ""
        }
    }
    
    open func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        self.currentParsedElement = ""
    }
    
    open func startParsing(_ data: Data, multiangleCameraCDN: String) {
        self.multiangleCameraCDN = multiangleCameraCDN
        self.xmlParser = XMLParser(data: data)
        self.xmlParser.delegate = self
        
        if (!self.xmlParser.parse()) {
            let error = self.xmlParser.parserError;
            NSLog("error parsing XML: \(error)")
        }
    }
}

public struct ModelVideoItem {
    public var CameraName: String?
    public let ThumbnailUrl: String?
    public let VideoUrl: String?
    public var CameraObject: Camera
    
    public init(cameraName: String, thumbnailUrl: String, videoUrl: String, cameras: [Camera]) {
        
        CameraName = cameraName
        ThumbnailUrl = thumbnailUrl
        VideoUrl = videoUrl
        
        CameraObject = Camera().getCameraFromCode(cameraName,cameras: cameras)
        if(!(CameraObject.Name?.isEmpty)!){
            CameraName = CameraObject.Name
        }
    }
}




