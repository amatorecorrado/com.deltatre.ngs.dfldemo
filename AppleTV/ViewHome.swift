//
//  ViewController.swift
//  AppleTVNative
//
//  Created by Federico Bortoluzzi on 08/04/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit

class ViewHome: BaseViewController{
    //let initView = UIView()
    let stackView = UIStackView()
    let scrollView = UIScrollView()
    
    let spinnerSplash = UIActivityIndicatorView(activityIndicatorStyle: .white)
    
    var splashImageView: HighlightImage? = nil
    
    let imagesMinimizedWidth = CGFloat(integerLiteral: 640)
    let imagesMinimizedHeight = CGFloat(integerLiteral: 1080)
    
    let imagesMaximizedWidth = CGFloat(integerLiteral: 640)
    let imagesMaximizedHeight = CGFloat(integerLiteral: 1080)
    
    let appInitUrl = URL(string: Bundle.main.infoDictionary?["URL AppInit"] as! String)
    var appInitData = Data()
    
    var homepageUrl = URL(string: "")
    var splashScreenUrl = URL(string: "")
    var homepageData = Data()
    var homepage: Homepage = Homepage()
    var repeatingTimer = RepeatingTimer(intervalSeconds: 60)
    
    //var alertIsVisisble = false
    //var alert = UIAlertController()
    
    var translationData: Translations? = nil
    var videosHighlights: [VideoModel] = []
    var isFirstLoading: Bool = false
    var isHomeLoaded: Bool = false
    let screenRect = UIScreen.main.bounds
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        isFirstLoading = true

        self.view.isUserInteractionEnabled = true
        self.view.backgroundColor = UIColor.black
//        initView.frame = self.view.layer.bounds
//        initView.userInteractionEnabled = true
//        self.view.addSubview(initView)
        
        //getAppInit()
        
        let splashFrame = CGRect(x: 0, y: 0, width: screenRect.width, height: screenRect.height)
        splashImageView = HighlightImage(frame: splashFrame)
        let img =  UIImage(named: "LaunchImage")!
        splashImageView!.image = img
        self.view.addSubview(splashImageView!)

        if(isConnected) {
            loadHome()
        }
        
        startPolling()
    }
    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if(isFirstLoading){

        }

        isFirstLoading = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        repeatingTimer.deinitTimer()
        
    }
    
    
    func loadHome() {
        ngs_analytics.setView("HomeView")
        isHomeLoaded = true
        //getAppInit()
        load()
    }
    
    func load() {
        stackView.axis = UILayoutConstraintAxis.horizontal
        stackView.distribution = UIStackViewDistribution.fillEqually
        stackView.alignment = UIStackViewAlignment.top
        //stackView.spacing = CGFloat(1)
        
        scrollView.frame = CGRect(x: 0, y: 0, width: screenRect.width, height: screenRect.height)
//        scrollView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        if #available(tvOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            scrollView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
            // Fallback on earlier versions
        }
        scrollView.autoresizingMask = UIViewAutoresizing.flexibleWidth
        scrollView.isScrollEnabled = true
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
        scrollView.addSubview(stackView)
        self.view.addSubview(scrollView)
        
        Utility.ResetChacheImage()
        
        splashImageView?.removeFromSuperview()
        
        //ONLY TEMPORARY
        //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) {
        //    self.preLoadData()
        //}
    }

    
    override weak var preferredFocusedView: UIView? {
        
        for item in stackView.arrangedSubviews {
            return item
        }
        return super.preferredFocusedView
    }
    
    func startPolling() {
    if (viewConfiguration == nil) {
            showAlert()
        } else {
        
        
        self.drawHomepage()
//        timerHomepage.schedule(deadline: .now(), repeating: .seconds(60), leeway: .milliseconds(100))
//        timerHomepage.setEventHandler(handler: { [weak self] in
//            self?.drawHomepage()
//        })
//        timerHomepage.resume()
        
        
//        timerHomepage = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: Selector(self.drawHomepage), userInfo: nil, repeats: true)
//        timerHomepage.fire()
        }
    }


    func drawHomepage(){
        NSLog("updateHomepage")
        
        for item in stackView.arrangedSubviews {
            stackView.removeArrangedSubview(item)
            item.removeFromSuperview()
        }
        
        let sliceCount = viewConfiguration!.data.count
        stackView.frame = CGRect(x: scrollView.frame.origin.x, y: scrollView.frame.origin.y, width: CGFloat(sliceCount * Int(imagesMaximizedWidth) + (Int(stackView.spacing) * sliceCount)), height: screenRect.height)
        for i in 0...(sliceCount-1) {
            let urlConf = viewConfiguration!.data[String(i+1)]
            drawSlice(i,urlData: urlConf!)
        }
        
        scrollView.contentSize =  stackView.bounds.size
        self.setNeedsFocusUpdate()
        self.updateFocusIfNeeded()
    }
    
    func drawSlice(_ index: Int, urlData: String){
        
        let sectionConfiguration = ConfiguratorController.sharedInstance.read(urlData)
        
        let innerFrame = CGRect(x: 0, y: 0, width: imagesMaximizedWidth, height: imagesMaximizedHeight)
        let notSelectedFrame = CGRect(x: 10, y: 10, width: imagesMinimizedWidth, height: imagesMinimizedHeight)
        
        let button = HomepageButton(type: UIButtonType.custom) as HomepageButton
        button.tag = 100 + index
        button.frame = innerFrame
        button.adjustsImageWhenHighlighted = true
        button.IDString = urlData
        if let disabled = sectionConfiguration?.parameters.getValue("disabled"), disabled.lowercased() != "true"{
            button.addTarget(self, action: #selector(self.handleButton(_:)), for: UIControlEvents.allEvents)
        }
        button.FocusCallback = focusCallback
        
        let notSelectedView = UIView(frame: innerFrame)
        notSelectedView.tag = 300 + index
        if (index == 0) {
            notSelectedView.isHidden = true
        }
        if let homeSliceUrl = sectionConfiguration?.images.getValue("home_slice"), !homeSliceUrl.isEmpty{
            let notSelectedImage = ImagesUtility.sharedInstance.getImage(homeSliceUrl)
            
            let notSelectedImgView = UIImageView(image: notSelectedImage)
            
            notSelectedImgView.frame = notSelectedFrame
            notSelectedView.addSubview(notSelectedImgView)
        }
        
        let selectedView = UIView(frame: innerFrame)
        selectedView.tag = 200 + index
        if (index > 0) {
            selectedView.isHidden = true
        }
        
        let shadow = UIView(frame: innerFrame)
        shadow.layer.shadowColor = UIColor.black.cgColor
        shadow.layer.shadowOpacity = 0.5
        shadow.layer.shadowOffset = CGSize.zero
        shadow.layer.shadowRadius = 50
        shadow.layer.shadowPath = UIBezierPath(rect: shadow.bounds).cgPath
        shadow.layer.shouldRasterize = true
        selectedView.addSubview(shadow)
        
        let selectedImage = ImagesUtility.sharedInstance.getImage((sectionConfiguration?.images.getValue("home_slice_selected"))! )
        let selectedImgView = UIImageView(image: selectedImage)
        selectedImgView.tag = 2000 + index
        selectedImgView.frame = innerFrame
        selectedImgView.adjustsImageWhenAncestorFocused = true
        selectedImgView.contentMode = .scaleAspectFill
        selectedView.addSubview(selectedImgView)
        
        
        button.addSubview(selectedView)
        button.addSubview(notSelectedView)
        
        
        stackView.addArrangedSubview(button)
        
    }
    
    @objc func handleButton(_ sender: HomepageButton!) {
        // e.g. button and view tag is 101
        // selected view is 201
        // not selected view is 301
        
        NSLog("button tag --> " + String(sender.tag))
        
        let urlString = sender.IDString
        
        ConfiguratorController.sharedInstance.action(urlString, additionalData: nil)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func focusCallback(_ type: String, tag: Int) {
        // type: focus or focusout
        // e.g. button and view tag is 101
        // selected view is 201
        // not selected view is 301
        
        print(type + " --> " + String(tag))
        NSLog("")
        
        for view:UIView in stackView.subviews  {
            if (view.tag == tag) {
                stackView.bringSubview(toFront: view)
                for subView:UIView in view.subviews {
                    if (subView.tag != 0) {
                        if (subView.tag == tag + 100) {
                            if (type == "focus") {
                                UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                                    subView.isHidden = false
                                    }, completion: nil)
                                
                            } else {
                                UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                                    subView.isHidden = true
                                    }, completion: nil)
                            }
                        } else {
                            if (type == "focus") {
                                UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                                    subView.isHidden = true
                                    }, completion: nil)
                                
                            } else {
                                UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                                    subView.isHidden = false
                                    }, completion: nil)
                            }
                        }
                    }
                }
            }
        }
    }
    
    //ONLY FOR WORST PERFORMANCE OF BIG FEEDS
    
    //var appInitTypeHighlights: AppInitType? = nil
    
    func preLoadData(){
        //HIGHLIGHTS
        //appInitTypeHighlights = appInit.getType("HIGHLIGHTS")
        
        //videosHighlights = VideoController(_urlVideoList: self.viewConfiguration!.feeds["feedVideosByCategory"]!, _urlMatch: self.viewConfiguration!.feeds["feedMatch"]!, _feedVideoData: self.viewConfiguration!.feeds["feedVideodata"]!, _cupCode: self.viewConfiguration!.parameters["cup"]!, _season: self.viewConfiguration!.parameters["season"]!).getVideoList(maxItems: 30)
    }
    

    
}
