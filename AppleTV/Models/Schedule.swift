//
//  Schedule.swift
//  AppleTV
//
//  Created by Federico Bortoluzzi on 20/06/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public struct Schedule {
    public var Matches: Array<Match>
    public var PreferredVideoId: String
    
    public init() {
        self.Matches = Array<Match>()
        self.PreferredVideoId = ""
    }
    
    public func getMatch(_ id: Int) -> Match? {
        
        for item in self.Matches {
            if (item.MatchId == id) {
                return item
            }
        }
        return nil
    }
    
    static public func deserialize(_ json: Dictionary<String, AnyObject> , feedMatch: String) -> Schedule {
        
        var result = Schedule()
        var m = Match()
        if let modules = json["modules"] as? Dictionary<String, AnyObject>{
            if let matchVideos = modules["videos"] as? Array<AnyObject> {
                for match in matchVideos {
                    if let item = match as? Dictionary<String, AnyObject> {
                        if let mId = item["mId"] as? Int {
                            if (mId > 0) {
                                m = Match()
                                
                                let matchidstr = String(mId)
                                var jsonurlmatch = URL(string: "")
                                if let urlmat: String? = feedMatch.replacingOccurrences(of: "{MATCHID}", with: matchidstr){
                                    jsonurlmatch = URL(string: urlmat!)!
                                }
                                
                                if  let jsonDataMatch = try? Data(contentsOf: jsonurlmatch!), jsonDataMatch.count > 0 {
                                    if let json = try? JSONSerialization.jsonObject(with: jsonDataMatch as Data, options: .allowFragments) as? [String: AnyObject]{
                                        if let modules = json!["modules"] as? [String: AnyObject]{
                                            var comp: [String: AnyObject] = [:]
                                            var mInfo: [String: AnyObject] = [:]
                                            if let competitions = modules["competition"] as? Array<AnyObject>, let competition = competitions.first as? [String:AnyObject]{
                                                comp = competition
                                            }
                                            if let matchInfo = modules["matchInfo"] as? [String: AnyObject]{
                                                mInfo = matchInfo
                                            }
                                            m = Match.deserialize(competition: comp, matchInfo: mInfo)
                                            
                                        }
                                    }
                                }
                                m.MatchId = mId
                                
                                if let kind = item["kind"] as? String {
                                    switch kind.lowercased(){
                                    case "replay":
                                        m.Videos.Replay.append(MatchVideo.create(item))
                                        break
                                    case "live":
                                        m.Videos.Live.append(MatchVideo.create(item))
                                        break
                                    case "scheduled":
                                        m.Videos.Scheduled.append(MatchVideo.create(item))
                                        break
                                    default:
                                        break
                                    }
                                    
                                }
                                result.Matches.append(m)
                            }
                        }
                    }
                }
            }
        }
        
        
        var liveVideoId = ""
        var liveDateTime = Date()
        
        var replayVideoId = ""
        var replayDateTime = Date.init(timeIntervalSince1970: TimeInterval.init(1))
        
        for match in result.Matches {
            for item in match.Videos.Live {
                if (match.DateUCT.compare(liveDateTime) == ComparisonResult.orderedAscending) {
                    liveVideoId = item.VideoId
                    liveDateTime = match.DateUCT as Date
                }
            }
            for item in match.Videos.Replay {
                if (match.DateUCT.compare(replayDateTime) == ComparisonResult.orderedDescending) {
                    replayVideoId = item.VideoId
                    replayDateTime = match.DateUCT as Date
                }
            }
        }
        
        result.PreferredVideoId = (liveVideoId.count > 0) ? liveVideoId : (replayVideoId.count > 0) ? replayVideoId : ""
        
        return result;
    }
}
