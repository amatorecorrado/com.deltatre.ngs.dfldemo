//
//  ParameterModel.swift
//  VideoPlayerFeatured
//
//  Created by Corrado Amatore on 19/04/2018.
//  Copyright © 2018 deltatre. All rights reserved.
//

import Foundation

public struct ParameterModel{
    public var Cup: String?
    public var CupCode: String?
    public var CupSeason: String?
    public var Season: String?
    public var Matchday: String?
    public var MatchId: String?
    public var RoundId: String?
}
