//
//  AppInit.swift
//  AppleTV
//
//  Created by Federico Bortoluzzi on 29/06/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

open class AppInit {
    open var FeedHomepage: String?
    open var SplashScreenApp: String?
    open var imagesVersion: String?
    
    open var Types: Array<AppInitType>
    
    open var IsDemoMode: Bool
    
    public init() {
        self.FeedHomepage = ""
        self.Types = Array<AppInitType>()
        self.IsDemoMode = false
        self.imagesVersion = nil
    }
    
    open func getType(_ type: String) -> AppInitType {
        var result = AppInitType()
        
        for item in self.Types {
            if (item.Type == type) {
                result = item
            }
        }
        
        return result
    }

//    public func getTypeFromAlias(alias: String) -> AppInitType {
//        var result = AppInitType()
//        
//        for item in self.Types {
//            if (item.AliasOf == alias) {
//                result = item
//            }
//        }
//        
//        return result
//    }
    
    static open func deserialize(_ item: Dictionary<String, AnyObject>) -> AppInit {
        let result = AppInit.init();
        
        if let val = item["feedHomepage"] as? String {
            result.FeedHomepage = val
        }
        if let val = item["splashScreenApp"] as? String {
            result.SplashScreenApp = val
        }
        if let val = item["imagesVersion"] as? String {
            result.imagesVersion = val
            if (val != UserDefaults.standard.string(forKey: "imagesVersion")){
                //ImagesUtility.setup(true, useLocalStorage: true)
                
                //result.Images = ImagesUtility(forceDownload: true, useLocalStorage: true)
                UserDefaults.standard.set(val, forKey: "imagesVersion")
            }else{
                //ImagesUtility.setup(false, useLocalStorage: true)
                //result.Images = ImagesUtility(forceDownload: false, useLocalStorage: true)
            }
        }
        if let val = item["IS_DEMO_MODE"] as? Bool {
            result.IsDemoMode = val
        }
        
        if let val = item["types"] as? Array<AnyObject> {
            result.Types = AppInitType.deserialize(val)
        }
        
        
        return result
    }
}

open class AppInitType {
    open var `Type`: String
    open var AliasOf: String
    
    open var Cup: Int
    open var CupSeason: Int
    open var Season: Int
    
    open var Background: String
    open var BackgroundMatches: String
    open var BackgroundSecond: String
    open var BackgroundThird: String
    
    open var BackgroundNotifications: String
    open var BackgroundMulticam: String
    open var BackgroundLineups: String
    open var BackgroundSplashScreen: String
    
    open var UrlEventsIcons_commentary: String
    open var UrlEventsIcons_lineup: String
    open var UrlEventsIcons_multicam: String
    
    open var FeedVocabulary: String
    open var FeedVocabularyCamera: String
    open var FeedCupConfiguration: String
    open var FeedMatchdays: String
    open var FeedMatchdaySchedule: String
    open var FeedMultiangles: String
    open var FeedEvents: String
    open var FeedLineups: String
    open var FeedClips: String
    
    open var FeedPlayerThumb: String
    open var FeedTeamLogo: String
    
    open var FeedVideodata: String
    open var FeedVideosByCategory: String
    open var FeedMatch: String
    open var CdnMultiangleCamera: String
    open var FeedEventsNGS: String
    open var FeedTranslations: String
    
    open var actionTypeDetail: String
    open var actionTypeAliasDetail: String
    
    //var storedImages: Dictionary <String, UIImage>?
    
    public init() {
        self.Type = ""
        self.AliasOf = ""
        
        self.Cup = 0
        self.CupSeason = 0
        self.Season = 0
        
        
        
        self.Background = ""
        self.BackgroundMatches = ""
        self.BackgroundNotifications = ""
        self.BackgroundMulticam = ""
        self.BackgroundLineups = ""
        self.BackgroundSplashScreen = ""
        self.BackgroundSecond = ""
        self.BackgroundThird = ""
        
        self.UrlEventsIcons_commentary = ""
        self.UrlEventsIcons_lineup = ""
        self.UrlEventsIcons_multicam = ""
        
        self.FeedVocabulary = ""
        self.FeedVocabularyCamera = ""
        self.FeedCupConfiguration = ""
        self.FeedMatchdays = ""
        self.FeedMatchdaySchedule = ""
        self.FeedMultiangles = ""
        self.FeedEvents = ""
        self.FeedLineups = ""
        self.FeedClips = ""
        self.FeedMatch = ""
        self.FeedVideosByCategory = ""
        
        self.FeedPlayerThumb = ""
        self.FeedTeamLogo = ""
        
        self.FeedVideodata = ""
        self.CdnMultiangleCamera = ""
        self.FeedTranslations = ""
        self.FeedEventsNGS = ""
        
        self.actionTypeDetail = ""
        self.actionTypeAliasDetail = ""
        
        //if self.storedImages == nil {
        //    self.storedImages = Dictionary()
        //}
    }

    static open func deserialize(_ items: Array<AnyObject>) -> Array<AppInitType> {
        var result = Array<AppInitType>();
        
        for item in items {
            let model = AppInitType()
        
            if let val = item["type"] as? String {
                model.Type = val
            }
            if let val = item["aliasOf"] as? String {
                model.AliasOf = val
            }
            if let val = item["cup"] as? Int {
                model.Cup = val
            }
            if let val = item["cupSeason"] as? Int {
                model.CupSeason = val
            }
            if let val = item["season"] as? Int {
                model.Season = val
            }
            if let val = item["background"] as? String {
                model.Background = val
            }
            if let val = item["backgroundMatches"] as? String {
                model.BackgroundMatches = val
            }
            if let val = item["backgroundNotifications"] as? String {
                model.BackgroundNotifications = val
            }
            if let val = item["backgroundMulticam"] as? String {
                model.BackgroundMulticam = val
            }
            if let val = item["backgroundLineups"] as? String {
                model.BackgroundLineups = val
            }
            if let val = item["events_icons_commentary"] as? String {
                model.UrlEventsIcons_commentary = val
            }
            if let val = item["events_icons_lineup"] as? String {
                model.UrlEventsIcons_lineup = val
            }
            if let val = item["events_icons_multicam"] as? String {
                model.UrlEventsIcons_multicam = val
            }
            if let val = item["feedVocabulary"] as? String {
                model.FeedVocabulary = val
            }
            if let val = item["feedVocabularyCamera"] as? String {
                model.FeedVocabularyCamera = val
            }
            if let val = item["feedCupConfiguration"] as? String {
                model.FeedCupConfiguration = val
            }
            if let val = item["feedMatchdays"] as? String {
                model.FeedMatchdays = val
            }
            if let val = item["feedMatchdaySchedule"] as? String {
                model.FeedMatchdaySchedule = val
            }
            if let val = item["feedMultiangles"] as? String {
                model.FeedMultiangles = val
            }
            if let val = item["feedEvents"] as? String {
                model.FeedEvents = val
            }
            if let val = item["feedLineups"] as? String {
                model.FeedLineups = val
            }
            if let val = item["feedClips"] as? String {
                model.FeedClips = val
            }
            if let val = item["feedPlayerThumb"] as? String {
                model.FeedPlayerThumb = val
            }
            if let val = item["feedTeamLogo"] as? String {
                model.FeedTeamLogo = val
            }
            if let val = item["feedVideodata"] as? String {
                model.FeedVideodata = val
            }
            if let val = item["feedVideosByCategory"] as? String {
                model.FeedVideosByCategory = val
            }
            if let val = item["feedMatch"] as? String {
                model.FeedMatch = val
            }
            if let val = item["cdnMultiangleCamera"] as? String {
                model.CdnMultiangleCamera = val
            }
            if let val = item["backgroundSplashScreen"] as? String {
                model.BackgroundSplashScreen = val
            }
            if let val = item["feedEventsNGS"] as? String {
                model.FeedEventsNGS = val
            }
            if let val = item["feedTranslations"] as? String {
                model.FeedTranslations = val
            }
            if let val = item["background_second"] as? String {
                model.BackgroundSecond = val
            }
            if let val = item["background_third"] as? String {
                model.BackgroundThird = val
            }
            
            if let val = item["action_type_detail"] as? String {
                model.actionTypeDetail = val
            }
            if let val = item["action_type_alias_detail"] as? String {
                model.actionTypeAliasDetail = val
            }
            
            result.append(model)
        }
        
        return result
    }
}
