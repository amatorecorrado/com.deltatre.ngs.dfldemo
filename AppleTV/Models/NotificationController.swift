//
//  Notification.swift
//  AppleTV
//
//  Created by Corrado Amatore on 27/06/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


public struct EventController
{
    //private var urlFeedEvents = "http://en.static.ngs.deltatre.net/feedApp/demo/match={MATCHID}/events.json"
    
    //private var _urlFeedEvents = "http://en.static.dev.ngs.deltatre.net/appletv/dev/2018002_Event.json"
    fileprivate var _urlFeedEvents = ""
    fileprivate var _translations: Translations?
    
    public init (urlFeedEvents: String, translations: Translations){
        _urlFeedEvents = urlFeedEvents
        _translations = translations
    }
    
    
    public func getNotifications(_ matchId: Int) -> ([EventModel]) {
        var results: [EventModel] = []
        var notification: EventModel
        var jsonurl = URL(string: "")
        
        if let url: String? = _urlFeedEvents.replacingOccurrences(of: "{MATCHID}", with: String(matchId)){
            jsonurl = URL(string: url!)!
        }
        let jsonData = try? Data(contentsOf: jsonurl!)
        if (jsonData?.count > 0) {
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as? [String: Any]
                for notifications in (json?["events"] as? [Dictionary<String, AnyObject>])! {
                    var timeSet:Bool = false
                    
                    notification = EventModel()
                    if let val = notifications["id"] as? Int {
                        notification.id = val
                    }
                    if let val = notifications["code"] as? Int {
                        notification.code = val
                    }
                    if let val = notifications["minute"] as? Int {
                        notification.minute = val
                    }
                    if let val = notifications["playerFrom"] as? Int {
                        notification.playerFrom = val
                    }
                    if let val = notifications["playerTo"] as? Int {
                        notification.playerTo = val
                    }
                    if let val = notifications["subCode"] as? Int {
                        notification.subCode = val
                    }
                    if let val = notifications["type"] as? Int {
                        notification.type = val
                    }
                    if let val = notifications["tag"] as? String {
                        notification.tag = val
                    }
                    if let val = notifications["time"] as? String {
                        notification.time = Date.init(dateTimeString: val)
                        timeSet = true;
                    }
                    if let val = notifications["teamTo"] as? Int {
                        notification.teamTo = val
                    }
                    if let val = notifications["teamFrom"]! as? Int {
                        notification.teamFrom = val
                    }
                    if let val = notifications["phase"] as? Int {
                        notification.phase = val
                    }
                    if let val = notifications["isValidated"] as? Bool {
                        notification.isValidated = val
                    }
                    if let val = notifications["injuryMinute"] as? Int {
                        notification.injuryMinute = val
                    }
                    if let val = notifications["descr"] as? String {
                        notification.descr = val
                        notification.text = TranslateEventText(notification)
                    }
                    
                    if notification.id != nil && notification.id != 0 && notification.code != nil && notification.code > 0 && notification.descr != nil  && notification.descr?.characters.count > 0 && timeSet {
                        results.append(notification)
                    }
                }
                
            } catch {
                NSLog("error serializing JSON: \(error)")
            }
        }
        
        return results
    }
    
    public static func GetEventById(_ id: Int, events: [EventModel])->EventModel?{
        for event in events{
            if(event.id == id){
                return event
            }
        }
        return EventModel()
    }
    
    fileprivate func TranslateEventText(_ event: EventModel) -> String{
        if(event.tag == nil){
            return event.descr!
        }
        var tmpTextTranslate = _translations!.generictags[event.tag!]
        
        if tmpTextTranslate != nil && tmpTextTranslate!.range(of: "@@player1") != nil && event.playerFrom != nil {
            if(event.playerFrom != nil){
                let tmpPlayerToTranslate = _translations!.players[event.playerFrom!.description]
            
                if(tmpPlayerToTranslate != nil){
                    tmpTextTranslate = tmpTextTranslate!.replacingOccurrences(of: "@@player1", with: (tmpPlayerToTranslate?.webname)!)
                }
            }
        }
        
        if tmpTextTranslate != nil && tmpTextTranslate!.range(of: "@@player2") != nil {
            if(event.playerTo != nil){
                let tmpPlayerFromTranslate = _translations!.players[event.playerTo!.description]
            
                if(tmpPlayerFromTranslate != nil){
                    tmpTextTranslate = (tmpTextTranslate!.replacingOccurrences(of: "@@player2", with: (tmpPlayerFromTranslate?.webname)!))
                }
            }
        }
        if tmpTextTranslate != nil && tmpTextTranslate!.range(of: "@@team1") != nil {
            if(event.teamFrom != nil){
                let tmpTeamFromTranslate = _translations!.teams[event.teamFrom!.description]
                
                if(tmpTeamFromTranslate != nil){
                    
                    tmpTextTranslate = (tmpTextTranslate!.replacingOccurrences(of: "@@team1", with: (tmpTeamFromTranslate?.name)!))
                }
            }
        }
        if tmpTextTranslate != nil && tmpTextTranslate!.range(of: "@@team2") != nil {
            if(event.teamTo != nil){
                let tmpTeamToTranslate = _translations!.teams[event.teamTo!.description]
            
                if(tmpTeamToTranslate != nil){
                    tmpTextTranslate = (tmpTextTranslate!.replacingOccurrences(of: "@@team2", with: (tmpTeamToTranslate?.name)!))
                }
            }
        }
        if tmpTextTranslate != nil && tmpTextTranslate!.range(of: "@@teamID") != nil {
            if(event.teamFrom != nil){
                let tmpTeamFromTranslate = _translations!.teams[event.teamFrom!.description]
            
                if(tmpTeamFromTranslate != nil){
                    tmpTextTranslate = (tmpTextTranslate!.replacingOccurrences(of: "@@teamID", with: (tmpTeamFromTranslate?.name)!))
                }
            }
        }
        
        if tmpTextTranslate != nil && tmpTextTranslate != "" {
            return tmpTextTranslate!
        }else{
            return event.descr!
        }
    }

}

