//
//  Match.swift
//  AppleTV
//
//  Created by Federico Bortoluzzi on 22/06/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


open class VideoDataController: NSObject, XMLParserDelegate{

    fileprivate var videoData: VideoDataModel
    fileprivate var xmlParser: XMLParser!
    fileprivate var currentParsedElement = ""
    
    
    override public init(){
            //super.init()
            videoData = VideoDataModel()
    }
    
    open func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        self.currentParsedElement = elementName
        
        if (elementName == "event") {
            for (i,item) in attributeDict.enumerated() {
                if (String(item.0).lowercased() == "id") {
                    videoData.eventId = item.1
                }
            }
        } else if (elementName == "videoSource") {
            for (i,item) in attributeDict.enumerated() {
                if (String(item.0).lowercased() == "format") {
                    videoData.format = "HLS"
                }
            }
        }
    }
    
    open func parser(_ parser: XMLParser, foundCharacters string: String) {
        if (self.currentParsedElement == "uri") {
            videoData.videoUrl = string
        } else if (self.currentParsedElement == "timeCodeIn") {
            let dateTimeTmp = Date.init(dateTimeStringWithoutFormat: string)
            
            let dateStringFormatter = DateFormatter()
            dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateStringFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateStringFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
            videoData.timeCodeIn = dateStringFormatter.string(from: dateTimeTmp)
        }
    }
    
    open func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        self.currentParsedElement = ""
        videoData.format = ""
    }
    
    open func startParsing(_ data: Data) {
        self.xmlParser = XMLParser(data: data)
        self.xmlParser.delegate = self
        
        if (!self.xmlParser.parse()) {
            let error = self.xmlParser.parserError;
            NSLog("error parsing XML: \(error)")
        }
    }
    
    open func getVideoData(_ feedVideoData: String,videoId: String) -> VideoDataModel {
        
        let xmlData = try? Data(contentsOf: URL(string: feedVideoData.replacingOccurrences(of: "{VIDEOID}", with: videoId))!)
        if (xmlData?.count > 0) {
            self.startParsing(xmlData!)
        }
        videoData.videoId = videoId
        return videoData
    }
}
