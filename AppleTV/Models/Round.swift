//
//  Round.swift
//  AppleTV
//
//  Created by Corrado Amatore on 23/06/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



public struct Round {
    public var RoundID: Int?
    public var RoundCode: String?
    public var RoundName: String?
    public var DateFormatted: String?
    public var MatchDays: [Int]?
    
    public init() {}
    public init(roundID: Int, roundCode: String, roundName: String, dateFormatted: String) {
        
        RoundID = roundID
        RoundCode = roundCode
        RoundName = roundName
        DateFormatted = dateFormatted
    }
    
    public func getRounds() -> ([Round]) {
        var results: [Round] = []
        var matchdayids: [Int] = []
        var matchdayid: Int = 0
        var round: Round
        var roundBefore = Round()
        var jsonurl = URL(string: "")
        let cup = Bundle.main.infoDictionary?["Cup"] as! String
        let season = Bundle.main.infoDictionary?["Season"] as! String
        
        if let url: String? = (Bundle.main.infoDictionary?["URL Matchdays"] as! String).replacingOccurrences(of: "{CUP}", with: String(cup)).replacingOccurrences(of: "{SEASON}", with: String(season)){
            jsonurl = URL(string: url!)!
        }
        let jsonData = try? Data(contentsOf: jsonurl!)
        if (jsonData?.count > 0) {
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as? [String: Any]
                for matchday in (json?["matchdays"] as? [Dictionary<String, AnyObject>])! {
                    
                    round = Round()
                    round.RoundID = matchday["round_id"] as? Int
                    round.RoundCode = matchday["round_code"] as? String
                    round.RoundName = matchday["round_n"] as? String
                    round.DateFormatted = matchday["date_string_formatted"] as? String
                    matchdayid=(matchday["matchday_id"] as? Int!)!
                    
                    if(round.RoundID != roundBefore.RoundID){
                        matchdayids=[]
                        matchdayids.append(matchdayid)
                        round.MatchDays = matchdayids
                        roundBefore = round
                        results.append(round)
                        
                    }else{
                        matchdayids.append(matchdayid)
//                        var oldRound = round.getRound(roundBefore.RoundID!, rounds: results)
 //                       oldRound.MatchDays = matchdayids
                    }
                }
                
            } catch {
                NSLog("error serializing JSON: \(error)")
            }
        }
        return results
    }
    
    public func getRound(_ roundid: Int, rounds: [Round], matchids: [Int]) -> Round{
        for item in rounds {
            if(item.RoundID == roundid){
                return item
            }
        }
        return Round()
    }
    
    public func updateRoundMatchIDs(_ roundid: Int, rounds: [Round], matchids: [Int]){
        for item in rounds {
            if(item.RoundID == roundid){
   //             item.MatchDays = matchids
                return
            }
        }
        return
    }
    
}
