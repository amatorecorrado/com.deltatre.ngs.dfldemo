//
//  Match.swift
//  AppleTV
//
//  Created by Federico Bortoluzzi on 20/06/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import VideoPlayerFeatured

public struct Match {
    
    public enum Status {
        case live
        case replay
        case future
    }
    
    public var MatchId: Int
    public var IdCup: Int
    public var Season: Int
    public var Status: Int
    public var StatusDescription: String
    public var DateUCT: NSDate
    public var DateCET: NSDate
    public var HomeTeamId: Int
    public var HomeTeamName: String
    public var HomeTeamCode: String
    public var HomeTeamLogoUrl: String = ""
    public var AwayTeamId: Int
    public var AwayTeamName: String
    public var AwayTeamCode: String
    public var AwayTeamLogoUrl: String = ""
    public var MatchDay: Int
    public var RoundId: Int
    public var RoundName: String
    public var RoundCode: String
    public var GroupId: Int
    public var GroupName: String
    public var Results: MatchResults
    public var Session: Int
    public var Videos: MatchVideos
    
    public init() {
        self.MatchId = 0
        self.IdCup = 0
        self.Season = 0
        self.Status = 0
        self.StatusDescription = ""
        self.DateUCT = NSDate.init(timeIntervalSince1970: 0)
        self.DateCET = NSDate.init(timeIntervalSince1970: 0)
        self.HomeTeamId = 0
        self.HomeTeamName = ""
        self.HomeTeamCode = ""
        self.AwayTeamId = 0
        self.AwayTeamName = ""
        self.AwayTeamCode = ""
        self.MatchDay = 0
        self.RoundId = 0
        self.RoundName = ""
        self.RoundCode = ""
        self.GroupId = 0
        self.GroupName = ""
        self.Results = MatchResults.init()
        self.Session = 0
        self.Videos = MatchVideos.init()
    }
    
    public init(matchId: Int, idCup: Int, season: Int, status: Int, statusDescription: String, dateUCT: NSDate, dateCET: NSDate, homeTeamId: Int, homeTeamName: String, homeTeamCode: String, awayTeamId: Int, awayTeamName: String, awayTeamCode: String, matchDay: Int, roundId: Int, roundName: String, roundCode: String, groupId: Int, groupName: String, results: MatchResults, session: Int, videos: MatchVideos) {
        self.MatchId = matchId
        self.IdCup = idCup
        self.Season = season
        self.Status = status
        self.StatusDescription = statusDescription
        self.DateUCT = dateUCT
        self.DateCET = dateCET
        self.HomeTeamId = homeTeamId
        self.HomeTeamName = homeTeamName
        self.HomeTeamCode = homeTeamCode
        self.AwayTeamId = awayTeamId
        self.AwayTeamName = awayTeamName
        self.AwayTeamCode = awayTeamCode
        self.MatchDay = matchDay
        self.RoundId = roundId
        self.RoundName = roundName
        self.RoundCode = roundCode
        self.GroupId = groupId
        self.GroupName = groupName
        self.Results = results
        self.Session = session
        self.Videos = videos
    }
    
    static public func deserialize(competition: Dictionary<String, AnyObject>,matchInfo: Dictionary<String, AnyObject>) -> Match {
        var result = Match.init();
        
        if let val = competition["id"] as? Int {
            result.IdCup = val
        }
        if let val = competition["season"] as? Int {
            result.Season = val
        }
        if let val = matchInfo["matchId"] as? Int {
            result.MatchId = val
        }
        
        if let val = matchInfo["status"] as? Int {
            result.Status = val
        }
        if let val = matchInfo["statusdescr"] as? String {
            result.StatusDescription = val
        }
        if let val = matchInfo["dateUTC"] as? String {
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssxxxxx"
            //dateFormatter.locale = Locale.init(identifier: "en_US_POSIX")
            
            let dateObj = dateFormatter.date(from: val)
            
            result.DateUCT = dateObj as! NSDate
        }
        if let val = matchInfo["dateCET"] as? String {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssxxxxx"
            //dateFormatter.locale = Locale.init(identifier: "en_US_POSIX")
            
            let dateObj = dateFormatter.date(from: val)
            
            
            result.DateCET = dateObj as! NSDate
        }
        if let homeTeam = matchInfo["homeTeam"] as? Dictionary<String, AnyObject> {
            if let val = homeTeam["id"] as? Int {
                result.HomeTeamId = val
            }
            if let val = homeTeam["name"] as? String {
                result.HomeTeamName = val
            }
            if let val = homeTeam["code"] as? String {
                result.HomeTeamCode = val
            }
            if let val = homeTeam["logo"] as? String {
                result.HomeTeamLogoUrl = val
            }
        }
        if let awayTeam = matchInfo["awayTeam"] as? Dictionary<String, AnyObject> {
            if let val = awayTeam["id"] as? Int {
                result.AwayTeamId = val
            }
            if let val = awayTeam["name"] as? String {
                result.AwayTeamName = val
            }
            if let val = awayTeam["code"] as? String {
                result.AwayTeamCode = val
            }
            if let val = awayTeam["logo"] as? String {
                result.AwayTeamLogoUrl = val
            }
        }
        if let val = matchInfo["matchDay"] as? Int {
            result.MatchDay = val
        }
        if let val = matchInfo["roundID"] as? Int {
            result.RoundId = val
        }
        if let val = matchInfo["roundName"] as? String {
            result.RoundName = val
        }
        if let val = matchInfo["groupID"] as? Int {
            result.GroupId = val
        }
        if let val = matchInfo["session"] as? Int {
            result.Session = val
        }
        if let val = matchInfo["groupName"] as? String {
            result.GroupName = val
        }
        if let val = matchInfo["results"] as? Dictionary<String, AnyObject> {
            result.Results = MatchResults.deserialize(item: val)
        }
        
        return result
    }
    
    func getStatus(videoDataUrl: String) -> Match.Status{
        if(self.isLive()){
            let videoId = self.Videos.Live[0].VideoId
            let videoData = VideoDataController().getVideoData(videoDataUrl, videoId: String(videoId))
            if((videoData.videoUrl.characters.count)>0){
                return Match.Status.live
            }else{
                return Match.Status.future
            }
        }else if(self.isScheduled()){
            let videoId = self.Videos.Scheduled[0].VideoId
            let videoData = VideoDataController().getVideoData(videoDataUrl, videoId: String(videoId))
            if((videoData.videoUrl.characters.count)>0){
                return Match.Status.live
            }else{
                return Match.Status.future
            }
        }else if(self.isReplay()){
            return Match.Status.replay
        }else{
            return Match.Status.future
        }
    }
    
    static func playMatch(videoData:VideoDataModel, matchId: Int, viewConfiguration: ConfigurationModel){
        
        var dictAdditionalData: [String: String] = [:]
        dictAdditionalData["videoUrl"] = videoData.videoUrl
        dictAdditionalData["matchId"] = String(matchId)
        dictAdditionalData["currentVideoId"] = videoData.videoId
        
        let urlString = viewConfiguration.actions["player"]
        if(urlString != nil){
//            Configurator.shared.navigationController = ConfiguratorController.sharedInstance.navigationController
//            Configurator.shared.action(url: urlString!, additionalData: dictAdditionalData as AnyObject)
        ConfiguratorController.sharedInstance.action(urlString!, additionalData: dictAdditionalData as AnyObject)
        }
    }
    
    
    
    //    public func isScore()-> (Bool){
    //        if(self.Status == 0 || self.Status == 2 || self.Status == 3 || self.Status == 4 || self.Status == 26 || self.Status == 2){
    //            return true
    //        }
    //        return false
    //    }
    
    public func isLive() -> (Bool){
        if(self.Videos.Live.count>0 && self.Videos.Live[0].VideoId.count>0){
            return true
        }
        return false
    }
    
    public func isScheduled() -> (Bool){
        if(self.Videos.Scheduled.count>0 && self.Videos.Scheduled[0].VideoId.count > 0){
            //if(self.Status == 3 && self.Videos.Live.count>0 && self.Videos.Live[0].VideoId > 0){
            return true
        }
        return false
    }
    
    public func isReplay() -> (Bool){
        if(self.Videos.Replay.count>0 && self.Videos.Replay[0].VideoId.count > 0){
            return true
        }
        return false
    }
}

public struct MatchResults {
    public var WinnerTeamId: Int
    public var HomeGoals: Int
    public var AwayGoals: Int
    public var HomeAggregateGoals: Int
    public var AwayAggregateGoals: Int
    public var HomePenaltyGoals: Int
    public var AwayPenaltyGoals: Int
    public var ReasonWin: Int
    public var HasAggregate: Bool
    public var Scorers: MatchResult
    
    public init() {
        self.WinnerTeamId = 0
        self.HomeGoals = 0
        self.AwayGoals = 0
        self.HomeAggregateGoals = 0
        self.AwayAggregateGoals = 0
        self.HomePenaltyGoals = 0
        self.AwayPenaltyGoals = 0
        self.ReasonWin = 0
        self.HasAggregate = false
        self.Scorers = MatchResult()
    }
    
    public init(winnerTeamId: Int, homeGoals: Int, awayGoals: Int, homeAggregateGoals: Int, awayAggregateGoals: Int, homePenaltyGoals: Int, awayPenaltyGoals: Int, reasonWin: Int, reasonWinTag: String, hasAggregate: Bool, scorers: MatchResult) {
        self.WinnerTeamId = winnerTeamId
        self.HomeGoals = homeGoals
        self.AwayGoals = awayGoals
        self.HomeAggregateGoals = homeAggregateGoals
        self.AwayAggregateGoals = awayAggregateGoals
        self.HomePenaltyGoals = homePenaltyGoals
        self.AwayPenaltyGoals = awayPenaltyGoals
        self.ReasonWin = reasonWin
        self.HasAggregate = hasAggregate
        self.Scorers = scorers
    }
    
    static public func deserialize(item: Dictionary<String, AnyObject>) -> MatchResults {
        var result = MatchResults.init();
        
        if let val = item["winnerTeamID"] as? Int {
            result.WinnerTeamId = val
        }
        if let val = item["aggregateScoreAway"] as? Int {
            result.AwayAggregateGoals = val
        }
        if let val = item["aggregateScoreHome"] as? Int {
            result.HomeAggregateGoals = val
        }
        if let val = item["penaltyScoreAway"] as? Int {
            result.AwayPenaltyGoals = val
        }
        if let val = item["penaltyScoreHome"] as? Int {
            result.HomePenaltyGoals = val
        }
        if let val = item["scoreAway"] as? Int {
            result.AwayGoals = val
        }
        if let val = item["scoreHome"] as? Int {
            result.HomeGoals = val
        }
        if let val = item["winReason"] as? Int {
            result.ReasonWin = val
        }
        if let val = item["hasAggregate"] as? Bool {
            result.HasAggregate = val
        }
        result.Scorers = MatchResult.deserialize(item: item)
        
        return result
    }
}

public struct MatchResult {
    public var HomeGoals: Array<MatchScore>
    public var AwayGoals: Array<MatchScore>
    
    public init() {
        self.HomeGoals = Array<MatchScore>()
        self.AwayGoals = Array<MatchScore>()
    }
    
    public init(homeGoals: Array<MatchScore>, awayGoals: Array<MatchScore>) {
        self.HomeGoals = homeGoals
        self.AwayGoals = awayGoals
    }
    
    static public func deserialize(item: Dictionary<String, AnyObject>) -> MatchResult {
        var result = MatchResult.init();
        
        if let val = item["homeGoals"] as? Array<AnyObject> {
            result.HomeGoals = MatchScore.deserialize(items: val)
        }
        if let val = item["awayGoals"] as? Array<AnyObject> {
            result.AwayGoals = MatchScore.deserialize(items: val)
        }
        
        return result
    }
}

public struct MatchScore {
    public var PlayerId: Int
    public var PlayerWebName: String
    public var Minute: Int
    public var Phase: Int
    public var InjuryMinute: Int
    
    public init() {
        self.PlayerId = 0
        self.PlayerWebName = ""
        self.Minute = 0
        self.Phase = 0
        self.InjuryMinute = 0
    }
    
    public init(playerId: Int, playerWebName: String, minute: Int, phase: Int, injuryMinute:  Int) {
        self.PlayerId = playerId
        self.PlayerWebName = playerWebName
        self.Minute = minute
        self.Phase = phase
        self.InjuryMinute = injuryMinute
    }
    
    static public func deserialize(items: Array<AnyObject>) -> Array<MatchScore> {
        var result = Array<MatchScore>();
        
        for item in items {
            var model = MatchScore();
            
            if let val = item["playerID"] as? Int {
                model.PlayerId = val
            }
            if let val = item["playerWebName"] as? String {
                model.PlayerWebName = val
            }
            if let val = item["matchMinute"] as? Int {
                model.Minute = val
            }
            if let val = item["phase"] as? Int {
                model.Phase = val
            }
            if let val = item["InjuryMinute"] as? Int {
                model.InjuryMinute = val
            }
            
            result.append(model)
        }
        
        return result
    }
}
public struct MatchVideos {
    public var Scheduled: Array<MatchVideo>
    public var Live: Array<MatchVideo>
    public var Replay: Array<MatchVideo>
    
    public init() {
        self.Scheduled = Array<MatchVideo>()
        self.Live = Array<MatchVideo>()
        self.Replay = Array<MatchVideo>()
    }
    
    public init(scheduled: Array<MatchVideo>, live: Array<MatchVideo>, replay: Array<MatchVideo>) {
        self.Scheduled = scheduled
        self.Live = live
        self.Replay = replay
    }
    
    static public func deserialize(_ item: Dictionary<String, AnyObject>) -> MatchVideos {
        var result = MatchVideos();
        
        if let val = item["scheduled"] as? Array<AnyObject> {
            result.Scheduled = MatchVideo.deserialize(val)
        }
        if let val = item["livevideo"] as? Array<AnyObject> {
            result.Live = MatchVideo.deserialize(val)
        }
        if let val = item["replay"] as? Array<AnyObject> {
            result.Replay = MatchVideo.deserialize(val)
        }
        
        return result
    }
}

public struct MatchVideo {
    public var VideoId: String = ""
    public var desc: String = ""
    public var title: String = ""
    public var kind: String = ""
    public var vStatus: String = ""
    public var matchId: String = ""
    public var is360: Bool = false
    
    public init() {
    }
    
    public init(videoId: String) {
        self.VideoId = videoId
    }
    
    static public func deserialize(_ items: Array<AnyObject>) -> Array<MatchVideo> {
        var result = Array<MatchVideo>();
        
        for item in items {
            var model = MatchVideo();
            
            if let val = item["video_id"] as? String {
                model.VideoId = val
            }
            
            result.append(model)
        }
        
        return result
    }
    
    static public func create(_ item: Dictionary<String,AnyObject>)-> MatchVideo{
        var result = MatchVideo()
        
        if let val = item["vId"] as? String {
            result.VideoId = val
        }
        if let val = item["desc"] as? String {
            result.desc = val
        }
        if let val = item["title"] as? String {
            result.title = val
        }
        if let val = item["vStatus"] as? String {
            result.vStatus = val
        }
        if let val = item["kind"] as? String {
            result.kind = val
        }
        if let val = item["is360"] as? Bool {
            result.is360 = val
        }
    return result
    }
    
}


