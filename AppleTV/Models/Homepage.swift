//
//  Homepage.swift
//  AppleTV
//
//  Created by Federico Bortoluzzi on 29/06/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public struct Homepage {
    public var Items: Array<HomepageItem>
    
    public init() {
        self.Items = Array<HomepageItem>()
    }
    
    static public func deserialize(_ item: Dictionary<String, AnyObject>) -> Homepage {
        var result = Homepage.init();
        
        if let val = item["items"] as? Array<AnyObject> {
            result.Items = HomepageItem.deserialize(val)
        }
        
        return result
    }
}

public struct HomepageItem {
    public var `Type`: String
    public var Image: String
    public var SelectedImage: String
    public var Title: String
    public var Description: String
    
    public var VideoUrl: String
    public var SeekTime: String
    public var MatchId: Int
    public var VideoStartTime: String
    
    public init() {
        self.Type = ""
        self.Image = ""
        self.SelectedImage = ""
        self.Title = ""
        self.Description = ""
        self.VideoUrl = ""
        self.SeekTime = ""
        self.MatchId = 0
        self.VideoStartTime = ""
        
    }
    
    public init(type: String, image: String, selectedImage: String, title: String, description: String, videoUrl: String, seekTime: String, matchId: Int, videoStartTime: String, feedTranslation: String) {
        self.Type = type
        self.Image = image
        self.SelectedImage = selectedImage
        self.Title = title
        self.Description = description
        self.VideoUrl = videoUrl
        self.SeekTime = seekTime
        self.MatchId = matchId
        self.VideoStartTime = videoStartTime
    }
    
    static public func deserialize(_ items: Array<AnyObject>) -> Array<HomepageItem> {
        var results = Array<HomepageItem>()
        
        for item in items {
            var model = HomepageItem()

            if let val = item["type"] as? String {
                model.Type = val
            }
            if let val = item["image"] as? String {
                model.Image = val
            }
            if let val = item["selectedImage"] as? String {
                model.SelectedImage = val
            }
            if let val = item["title"] as? String {
                model.Title = val
            }
            if let val = item["description"] as? String {
                model.Description = val
            }
            if let val = item["videoUrl"] as? String {
                model.VideoUrl = val
            }
            if let val = item["seekTime"] as? String {
                model.SeekTime = val
            }
            if let val = item["matchId"] as? Int {
                model.MatchId = val
            }
            if let val = item["videoStartTime"] as? String {
                model.VideoStartTime = val
            }
            
            results.append(model)
        }
        
        return results
    }
}
