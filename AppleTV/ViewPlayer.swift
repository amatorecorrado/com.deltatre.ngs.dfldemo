//
//  ViewPlayer.swift
//  AppleTVNative
//
//  Created by Federico Bortoluzzi on 15/04/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

//
//  ViewController.swift
//  AppleTVNative
//
//  Created by Federico Bortoluzzi on 08/04/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer

class ViewPlayer: BaseViewController, NotificationPanelDelegate, TodayMatchesPanelDelegate, MulticamPanelDelegate
{
    // MARK: -
    
    // MARK: Define variables
    
    var cup: Int = 0
    var cupSeason: Int = 0
    var season: Int = 0
    //var roundId: Int = 0
    //var matchDay: Int = 0
    //var session: Int = 0
    var matchId: Int = 0
    
    var currentVideoId: Int = 0
    var videoUrl: String  = ""
    var seekTime: String = ""
    var videoStartTime: String = ""
    var counterPenalty = 1
    
    var cupInfoUrl = URL(string: "")
    var matchIdSplitted: [String] = [String]()
    var matchDayScheduleUrl = URL(string: "")
    var lineupUrl = URL(string: "")
    
    var videoData = VideoDataModel()
    var actualMatchObj: Match? = Match()
    
    var selectedPlayByPlay: String = ""
    var selectedCameraCurrent: String = ""
    //var selectedSecondPlayerMatchID: Int = -1
    
    var homeTeamName: String = ""
    var awayTeamName: String = ""
    
    var canCloseTheView = true
    
    var isPictureInPicture = false
    var isTwoPlayerView = false
    
    let touchToleranceLight = CGFloat(integerLiteral: 125)
    let touchToleranceMax = CGFloat(integerLiteral: 300)
    
    let screenRect = UIScreen.main.bounds
    var initView = UIView()
    var avPlayerView_MAIN = AVPlayerViewController()
    var avPlayer_MAIN = AVQueuePlayer()
    
    var avPlayerTempWidth = CGFloat(0)
    var menuViewController = PlayerMenuViewController()
    var scoreLabel = UILabel()
    
    var backgroundOtherMatchesImage = UIImage()
    var backgroundNotificationsImage = UIImage()
    var backgroundMulticamImage = UIImage()
    var backgroundLineupsImage = UIImage()
    let titleMatchesLabel = UILabel()
    
    var touchCheckpointX = CGFloat()
    var touchCheckpointY = CGFloat()
    
    var menuOtherMatchesIsActive = false
    var viewOtherMatchesIsActive = false
    var menuNotificationsIsActive = false
    var viewNotificationsIsActive = false
    var menuMultiangleCameraIsActive = false
    var viewMultiangleCameraIsActive = false
    var viewMultiangleCameraHighlightedIsActive = false
    var menuLineupsIsActive = false
    var viewLineupsIsActive = false
    
    let avPlayerView_SECONDARY = AVPlayerViewController()
    var avPlayer_SECONDARY = AVQueuePlayer()
    var dictMultiangleCameraOrder = Dictionary<String, VideoClipModel>()
    var dictMultiangleCameraAvPlayerQueue = Dictionary<String, AVPlayerItem>()
    
    var mainPlayerFrameStore: CGRect? = nil
    var secondaryPlayerFrameStore: CGRect? = nil
    var mainPlayerAnchorPointStore: CGPoint? = nil
    var secondaryPlayerAnchorPointStore: CGPoint? = nil
    
    var playerContainerView = UIView()
    
    var translationData: Translations?
    
    var notificationPanel = NotificationPanel()
    var lineupsPanel = LineupsPanel()
    var todayMatchesPanel = TodayMatchesPanel()
    var multicamPanel = MulticamPanel()
    
    // MARK: -
    
    // MARK: View Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        ngs_analytics_setView(videoUrl,matchId: String(matchId),viewName: "PlayerMatchView")
        
        
        
        
        //TEST
        //isLive = true
        
        // Do any additional setup after loading the view, typically from a nib.
        self.view.isUserInteractionEnabled = true
        
        //GetTranslation()
        
        //UCL
        //        roundId = 2000634
        //        matchDay = 1
        //        session = 1
        //        matchId = 2015671
        
        InitData()
//        if actualMatchObj!.isLive() && (actualMatchObj?.RoundId == 0 || actualMatchObj?.MatchDay == 0) {
//            getCupInfo()
//        }
        
        //UCL
        //        roundId = 2000634
        //        matchDay = 1
        //        session = 1
        //        matchId = 2015671
        
        
        let backRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backRecognizer(_:)))
        self.view.addGestureRecognizer(backRecognizer)
        
        backRecognizer.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue as Int)]
        
        self.GetTranslation()
        
        matchDayScheduleUrl = URL(string: (viewConfiguration?.feeds["feedMatchdaySchedule"]!.replacingOccurrences(of: "{CUP}", with: String(cup)).replacingOccurrences(of: "{SEASON}", with: String(season)).replacingOccurrences(of: "{ROUNDID}", with: String(describing: actualMatchObj?.RoundId)).replacingOccurrences(of: "{MATCHDAY}", with: String(describing: actualMatchObj?.MatchDay)))!)!
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        let urlBackgroundMatches = URL(string: (viewConfiguration?.images.getValue("backgroundMatches"))!)
        let dataBackgroundMatches = try? Data(contentsOf: urlBackgroundMatches!)
        UIImage(data: dataBackgroundMatches!)?.draw(in: self.view.bounds)
        backgroundOtherMatchesImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        let urlBackgroundNotifications = URL(string: (viewConfiguration?.images.getValue("backgroundNotifications"))!)
        let dataBackgroundNotifications = try? Data(contentsOf: urlBackgroundNotifications!)
        UIImage(data: dataBackgroundNotifications!)?.draw(in: self.view.bounds)
        backgroundNotificationsImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        let urlBackgroundMulticam = URL(string: (viewConfiguration?.images.getValue("backgroundMulticam"))!)
        let dataBackgroundMulticam = try? Data(contentsOf: urlBackgroundMulticam!)
        UIImage(data: dataBackgroundMulticam!)?.draw(in: self.view.bounds)
        backgroundMulticamImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        let urlBackgroundLineups = URL(string: (viewConfiguration?.images.getValue("backgroundLineups"))!)
        let dataBackgroundLineups = try? Data(contentsOf: urlBackgroundLineups!)
        UIImage(data: dataBackgroundLineups!)?.draw(in: self.view.bounds)
        backgroundLineupsImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        initView.backgroundColor = UIColor.init(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1)
        initView.frame = self.view.layer.bounds
        initView.isUserInteractionEnabled = true
        
        
        avPlayer_MAIN.removeAllItems()
        avPlayerView_MAIN.removeFromParentViewController()
        avPlayerView_MAIN.view.alpha = 1
        avPlayerView_MAIN.view.frame = self.view.layer.bounds
        avPlayerView_MAIN.view.isUserInteractionEnabled = false
        avPlayerView_MAIN.showsPlaybackControls = true
        avPlayerView_MAIN.player = avPlayer_MAIN
        avPlayerView_MAIN.view.backgroundColor = UIColor.clear
        avPlayerView_MAIN.view.layer.shadowColor = UIColor.black.cgColor
        avPlayerView_MAIN.view.layer.shadowOpacity = 0.8
        avPlayerView_MAIN.view.layer.shadowOffset = CGSize.zero
        avPlayerView_MAIN.view.layer.shadowRadius = 25
        avPlayerView_MAIN.view.layer.shadowPath = UIBezierPath(rect: avPlayerView_MAIN.view.bounds).cgPath
        avPlayerView_MAIN.view.layer.shouldRasterize = false
        avPlayerView_MAIN.view.transform = CGAffineTransform.identity
        avPlayerView_MAIN.videoGravity = "AVLayerVideoGravityResize"
        avPlayerView_MAIN.view.clipsToBounds = false
        self.addChildViewController(avPlayerView_MAIN)
        avPlayerView_MAIN.view.isHidden = true
        initView.addSubview(avPlayerView_MAIN.view)
        
        let otherMatchesImgView = UIImageView(image: UIImage(named: "MockOtherMatches"))
        otherMatchesImgView.frame = CGRect(x: 0, y: 0, width: 1920, height: 1080)
        otherMatchesImgView.tag = 2000
        otherMatchesImgView.isHidden = true
        initView.addSubview(otherMatchesImgView)
        
        let notificationsImgView = UIImageView(image: UIImage(named: "MockNotifications"))
        notificationsImgView.frame = CGRect(x: 0, y: 0, width: 1920, height: 1080)
        notificationsImgView.tag = 3000
        notificationsImgView.isHidden = true
        initView.addSubview(notificationsImgView)
        
        let lineupsImgView = UIImageView(image: UIImage(named: "MockLineups"))
        lineupsImgView.frame = CGRect(x: 0, y: 0, width: 1920, height: 1080)
        lineupsImgView.tag = 1000
        lineupsImgView.isHidden = true
        initView.addSubview(lineupsImgView)
        
        avPlayer_SECONDARY.removeAllItems()
        avPlayerView_SECONDARY.removeFromParentViewController()
        avPlayerView_SECONDARY.view.alpha = 1
        avPlayer_SECONDARY.actionAtItemEnd = AVPlayerActionAtItemEnd.none
        NotificationCenter.default.addObserver(self, selector: #selector(avPlayerItemDidPlayToEndTimeNotification), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        avPlayerView_SECONDARY.view.isHidden = true
        //avPlayerView_SECONDARY_MAIN.view.frame = CGRect(x: (screenRect.width / 2) - (playerMultiangleCameraWidth / 2), y: 60, width: playerMultiangleCameraWidth / 100 * 30, height: playerMultiangleCameraHeight / 100 * 30)
        avPlayerView_SECONDARY.view.frame = CGRect(x: 0, y: 0, width: 1920, height: 1080)
        avPlayerView_SECONDARY.view.isUserInteractionEnabled = false
        avPlayerView_SECONDARY.showsPlaybackControls = true
        avPlayerView_SECONDARY.player = avPlayer_SECONDARY
        avPlayerView_SECONDARY.view.backgroundColor = UIColor.clear
        avPlayerView_SECONDARY.showsPlaybackControls = false
        avPlayerView_SECONDARY.view.transform = CGAffineTransform.identity
        avPlayerView_SECONDARY.videoGravity = "AVLayerVideoGravityResize"
        avPlayerView_SECONDARY.view.clipsToBounds = false
        avPlayerView_SECONDARY.view.transform = avPlayerView_SECONDARY.view.transform.scaledBy(x: 0.1, y: 0.1)
        
        self.view.addSubview(initView)
        
        InitAvPlayer()
        
        //drawScorePanel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        avPlayer_MAIN.addObserver(self, forKeyPath:"rate", options:.initial, context:nil)
        avPlayer_MAIN.addObserver(self, forKeyPath:"status", options:.initial, context:nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
      
        avPlayer_MAIN.removeObserver(self, forKeyPath: "rate")
        avPlayer_MAIN.removeObserver(self, forKeyPath: "status")
    }
    
    deinit {
        avPlayerView_MAIN.removeFromParentViewController()
        avPlayerView_SECONDARY.removeFromParentViewController()
        
        NSLog("deinit")
    }
    
    
    func drawScorePanel(){
        self.homeTeamName = (actualMatchObj?.HomeTeamName)!
        self.awayTeamName = (actualMatchObj?.AwayTeamName)!
        
        var scoreAway: String? = String(self.actualMatchObj!.Results.AwayGoals)
        var scoreHome: String? = String(self.actualMatchObj!.Results.HomeGoals)
        
        if (scoreHome == nil) {
            scoreHome = "0"
        }
        if (scoreAway == nil) {
            scoreAway = "0"
        }
        
        //let fontTeam = UIFont.init(name: "Dosis-SemiBold", size: 40)
        //let fontPlayers = UIFont.init(name: "Dosis-Medium", size: 36)
        let home: String? =  self.translationData?.teams[String(self.actualMatchObj!.HomeTeamId)]?.name
        let away: String? =  self.translationData?.teams[String(self.actualMatchObj!.AwayTeamId)]?.name
        let scoreTeam = home!.uppercased() + "  " + scoreHome! + " - " + scoreAway! + "  " + away!.uppercased()
        //let scorePlayers = NSMutableAttributedString(string: "FERNANDO (RMA) 20'", attributes: [NSFontAttributeName: fontPlayers!, NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        //scoreTeam.appendAttributedString(scorePlayers)
        self.scoreLabel.font = UIFont.init(name: "Dosis-SemiBold", size: 40)
        self.scoreLabel.text = scoreTeam
        
    }
    
    func GetTranslation()
    {
        translationData = DataProvider.sharedInstance.getMatchTranslation(cup, season: season)
    }
    
    func InitData(){
        
        // ---- DEFINE PROVIDER DATA
        DataProvider.sharedInstance.setParameters(self.viewConfiguration!)
        
        cup = Int(((viewConfiguration!.parameters["cup"]))!)!
        //cupSeason = Int((viewConfiguration?.parameters["cupSeason"])!)!
        season = Int((viewConfiguration?.parameters["season"])!)!
        cupInfoUrl = URL(string: (viewConfiguration?.feeds["feedCupConfiguration"])!)!
        
        //OVERRIDE PASSING VALUE WITH CONFIGURATION ONE
        let overrideMatchId = viewConfiguration?.parameters.getValue("matchId")
        if(overrideMatchId != nil && overrideMatchId != ""){
            self.matchId = Int((viewConfiguration!.parameters.getValue("matchId")))!
        }
        
        actualMatchObj = DataProvider.sharedInstance.getMatchData(cup, season: season, matchId: matchId)
        
        
        matchIdSplitted = Utility.getMatchIdSplitted(matchId)
        
    }
    
    
    internal func InitAvPlayer() {
        
        self.scoreLabel.text = ""
        
        
        if let url: String? = viewConfiguration!.feeds["feedLineups"]!.replacingOccurrences(of: "{MATCHID}", with: String(matchId)) {
            lineupUrl = URL(string: url!)!
        }
        
        if (currentVideoId > 0) {
            videoData = VideoDataController().getVideoData(viewConfiguration!.feeds["feedVideodata"]!, videoId: String(currentVideoId))
            if (videoData.videoUrl.isEmpty == false) {
                matchId = Int(videoData.eventId)!
                videoUrl = videoData.videoUrl
                seekTime = ""
                videoStartTime = videoData.timeCodeIn
            }
        }
        
        if (!videoUrl.isEmpty) {
            
            
            let urlTokenized: String = AkamaiTokenizer.tokenize(videoUrl, secret: secret)
            
            let url = URL(string: urlTokenized)
            let avAsset = AVAsset(url: url!)
            let avPlayerItem = AVPlayerItem(asset: avAsset)
            
            if(avPlayer_MAIN.rate != 0) {
                avPlayer_MAIN.pause()
                avPlayer_MAIN.currentItem?.seek(to: kCMTimeZero)
            }
            avPlayer_MAIN.removeAllItems()
            
            avPlayer_MAIN.insert(avPlayerItem, after: nil)

            if(actualMatchObj!.isLive()){
                //DataProvider.sharedInstance.fetchInterval = 60
                DataProvider.sharedInstance.fetchMatchData(self.cup, season: self.season, roundId: (actualMatchObj?.RoundId)!, matchDay: (actualMatchObj?.MatchDay)!, matchId: self.matchId, fetchInterval: 60)
                
            }else{
                //DataProvider.sharedInstance.fetchInterval = 0
                DataProvider.sharedInstance.fetchMatchData(self.cup, season: self.season, roundId: (actualMatchObj?.RoundId)!, matchDay: (actualMatchObj?.MatchDay)!, matchId: self.matchId) //,fetchInterval: 15
            }
            
            
        } else {
            //TO-DO: add alert when no videourl si present
        }
    }
    
    
    
    func backRecognizer(_ recognizer: UITapGestureRecognizer) {
        backActions()
    }
    
    func backActions(){
        NSLog("Menu Button Controller")
        
        if (canCloseTheView) {
            
            DataProvider.sharedInstance.dismissMatchData()
            
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                
                self.avPlayerView_MAIN.view.alpha = 0
                self.avPlayerView_SECONDARY.view.alpha = 0

                
                }, completion: {
                    (Value: Bool) in
                    self.avPlayer_MAIN.removeAllItems()
                    self.avPlayer_SECONDARY.removeAllItems()
                    
                    self.avPlayerView_MAIN.removeFromParentViewController()
                    self.avPlayerView_SECONDARY.removeFromParentViewController()
                    
                    NotificationCenter.default.removeObserver(self)
                    self.navigationController?.popViewController(animated: true)
            })
            
            
            
            

        }
        else {
            
            if (viewMultiangleCameraHighlightedIsActive) {
                multiangleRemove()
                goBackToMulticam()
                self.multicamPanel.multiangleCameraScrollView.isHidden = false
                viewMultiangleCameraHighlightedIsActive = false
                
            } else if (viewOtherMatchesIsActive) {
                todayMatchesPanel.view.removeFromSuperview()
                todayMatchesPanel.dismiss(animated: true, completion: nil)
                
                if(initView.subviews.contains(titleMatchesLabel)){
                    titleMatchesLabel.removeFromSuperview()
                }
                if (isTwoPlayerView) {
                    goBackMatches()
                    canCloseTheView = true
                    //goBackToPlayer()
                }
                else {
                    //SAVE ACTUAL STATE
                    mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
                    mainPlayerAnchorPointStore = CGPoint(x: 0, y: 0)
                    canCloseTheView = true
                    goBackToPlayer()
                }
                
            } else if (viewMultiangleCameraIsActive) {
                
                dictMultiangleCameraAvPlayerQueue = [:]
                multicamPanel.view.removeFromSuperview()
                multicamPanel.dismiss(animated: true, completion: nil)
                goBackToPlayer()
            } else if (viewLineupsIsActive) {
                lineupsPanel.view.removeFromSuperview()
                lineupsPanel.dismiss(animated: true, completion: nil)
                goBackToPlayer()
                
            } else if (viewNotificationsIsActive) {
                
                
                notificationPanel.view.removeFromSuperview()
                notificationPanel.dismiss(animated: true, completion: nil)
                
                if(isPictureInPicture){
                    goBackNotification()
                }else{
                    //SAVE ACTUAL STATE
                    mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
                    mainPlayerAnchorPointStore = CGPoint(x: 0, y: 0)
                    goBackToPlayer()
                    
                }
            }
            else {
                goBackToPlayer()
            }
        }
    }
    
    
    func goBackToMulticam(){
        
        multicamPanel.panelMultiAngleRemove()
        
        avPlayer_SECONDARY.pause()
        self.avPlayerView_SECONDARY.view.backgroundColor = UIColor.clear
        avPlayerView_MAIN.view.isHidden = false
        
        self.avPlayerView_MAIN.view.layer.zPosition = 0
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.avPlayerView_SECONDARY.view.alpha = 0
            
            }, completion: {
                    (Value: Bool) in
                self.avPlayerView_SECONDARY.view.removeFromSuperview()
                self.avPlayerView_SECONDARY.removeFromParentViewController()
                
        })
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            //self.avPlayerView_MAIN.view.layer.anchorPoint = CGPointMake(1,1)
            self.avPlayerView_MAIN.view.transform = self.avPlayerView_MAIN.view.transform.scaledBy(x: (self.mainPlayerFrameStore?.size.width)!/self.avPlayerView_MAIN.view.frame.size.width, y: (self.mainPlayerFrameStore?.size.height)!/self.avPlayerView_MAIN.view.frame.size.height)
            self.mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
            self.mainPlayerAnchorPointStore = CGPoint(x: 0,y: 0.5)
            self.avPlayerView_MAIN.view.frame.origin.x = (self.screenRect.width-self.avPlayerView_MAIN.view.frame.width) / 2
            self.avPlayerView_MAIN.view.frame.origin.y = 40
            
            
            }, completion: {
                (Value: Bool) in
                self.avPlayerView_SECONDARY.view.isHidden = true
                self.avPlayerView_SECONDARY.view.alpha = 1
                self.avPlayer_MAIN.play()
        })
        
        
    }
    
    func goBackNotification(){
        
        isPictureInPicture = false
        
        avPlayer_SECONDARY.pause()
        self.avPlayerView_SECONDARY.view.backgroundColor = UIColor.clear
        avPlayerView_MAIN.view.isHidden = false
        
        self.avPlayerView_SECONDARY.view.layer.zPosition = 0
        self.avPlayerView_MAIN.view.layer.zPosition = 1
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            //self.avPlayerView_SECONDARY.view.transform = CGAffineTransformScale(self.avPlayerView_SECONDARY.view.transform, 0.1, 0.1)
            self.avPlayerView_SECONDARY.view.alpha = 0
            }, completion: {
                (Value: Bool) in
                
                self.avPlayerView_SECONDARY.view.removeFromSuperview()
                self.avPlayerView_SECONDARY.removeFromParentViewController()
                
                
        })
        
        UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            //self.avPlayerView_MAIN.view.layer.anchorPoint = CGPointMake(0.1, 0.5)
            self.avPlayerView_MAIN.view.transform = self.avPlayerView_MAIN.view.transform.scaledBy(x: (self.mainPlayerFrameStore?.size.width)!/self.avPlayerView_MAIN.view.frame.size.width, y: (self.mainPlayerFrameStore?.size.height)!/self.avPlayerView_MAIN.view.frame.size.height)//
            self.avPlayerView_MAIN.view.frame.origin.x = 70
            self.avPlayerView_MAIN.view.frame.origin.y = (self.screenRect.height-self.avPlayerView_MAIN.view.frame.height) / 2
            
            
            }, completion: {
                (Value: Bool) in
                self.avPlayerView_SECONDARY.view.isHidden = true
                self.avPlayerView_SECONDARY.view.alpha = 1
                self.avPlayer_MAIN.play()
        })
        
        
        
        createNotificationPanel()
    }
    
    
    func goBackMatches(){
        
        isTwoPlayerView =  false
        
        avPlayer_SECONDARY.pause()
        self.avPlayerView_SECONDARY.view.backgroundColor = UIColor.clear
        avPlayerView_MAIN.view.isHidden = false
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            //self.avPlayerView_SECONDARY.view.transform = CGAffineTransformScale(self.avPlayerView_SECONDARY.view.transform, 0.1, 0.1)
            self.avPlayerView_SECONDARY.view.alpha = 0
            }, completion: {
                (Value: Bool) in
                
                
                self.avPlayerView_SECONDARY.view.removeFromSuperview()
                self.avPlayerView_SECONDARY.removeFromParentViewController()
                
                UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                    
                    //self.avPlayerView_MAIN.view.layer.anchorPoint = CGPointMake(1,1)
                    self.avPlayerView_MAIN.view.transform = self.avPlayerView_MAIN.view.transform.scaledBy(x: (self.mainPlayerFrameStore?.size.width)!/self.avPlayerView_MAIN.view.frame.size.width, y: (self.mainPlayerFrameStore?.size.height)!/self.avPlayerView_MAIN.view.frame.size.height)
                    self.mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
                    self.mainPlayerAnchorPointStore = CGPoint(x: 0,y: 0.5)
                    //self.avPlayerView_MAIN.view.frame.origin.x = (self.screenRect.width-self.avPlayerView_MAIN.view.frame.width) / 2
                    //self.avPlayerView_MAIN.view.frame.origin.y = 0
                    self.avPlayerView_MAIN.view.layer.zPosition = 0
                    
                    }, completion: {
                        (Value: Bool) in
                        self.avPlayerView_SECONDARY.view.isHidden = true
                        self.avPlayerView_SECONDARY.view.alpha = 1
                        self.avPlayer_MAIN.play()
                        
                        
                })
        })
        
    }
    
    
    func goBackToPlayer() {
        
        if (!avPlayerView_SECONDARY.view.isHidden) {
            avPlayerView_SECONDARY.view.isHidden = true
            avPlayerView_MAIN.view.isHidden = false
        }
        
        initView.viewWithTag(1000)?.isHidden = true
        initView.viewWithTag(2000)?.isHidden = true
        initView.viewWithTag(3000)?.isHidden = true
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.avPlayerView_MAIN.view.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
            self.avPlayerView_MAIN.view.frame = UIScreen.main.bounds
            }, completion:  {  finish in
                if (self.avPlayer_MAIN.rate == 0) {
                    self.avPlayer_MAIN.play()
                }
        })
        
        
        viewOtherMatchesIsActive = false
        viewNotificationsIsActive = false
        viewMultiangleCameraIsActive = false
        viewMultiangleCameraHighlightedIsActive = false
        viewLineupsIsActive = false
        
        canCloseTheView = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "rate") {
            NSLog("rate \(avPlayer_MAIN.rate)")
            if(avPlayer_MAIN.rate == 0.0) {
                avPlayerView_MAIN.view.isUserInteractionEnabled = true
            } else {
                avPlayerView_MAIN.view.isUserInteractionEnabled = false
            }
        }
        if (keyPath == "status") {
            NSLog("status \(avPlayer_MAIN.status.rawValue)")
            if(avPlayer_MAIN.status == AVPlayerStatus.unknown) {
                NSLog("AVPlayerStatus.Unknown")
            } else if(avPlayer_MAIN.status == AVPlayerStatus.failed) {
                NSLog("AVPlayerStatus.Failed")
            } else if(avPlayer_MAIN.status == AVPlayerStatus.readyToPlay) {
                NSLog("AVPlayerStatus.ReadyToPlay")
                //if(avPlayer_MAIN.status == AVPlayerStatus.ReadyToPlay && avPlayer_MAIN.currentItem?.status == AVPlayerItemStatus.ReadyToPlay) {
                if(!actualMatchObj!.isLive()){
                    seekTime = videoStartTime
                }
                if (seekTime.isEmpty) {
                    avPlayer_MAIN.currentItem?.seek(to: kCMTimeZero)
                } else {
                    let time = CMTimeMakeWithSeconds(Double(seekTime)!, avPlayer_MAIN.currentItem!.asset.duration.timescale)
                    avPlayer_MAIN.currentItem?.seek(to: time)
                }
                avPlayer_MAIN.play()
                avPlayerView_MAIN.view.isHidden = false
                
                if(self.avPlayer_MAIN.currentItem?.accessLog()?.events.first?.playbackType == "LIVE"){
                    
                    ngs_analytics_setEvent(videoUrl, matchId: String(matchId), moduleName: "VideoStream", eventName: "Live")
                }else if(self.avPlayer_MAIN.currentItem?.accessLog()?.events.first?.playbackType == "VOD")
                {
                    ngs_analytics_setEvent(videoUrl, matchId: String(matchId), moduleName: "VideoStream", eventName: "Vod")
                }
                
                print(avPlayer_MAIN.currentItem?.accessLog()?.events.first?.playbackType)
                //}
            }
        }
    }
    
    func avPlayerItemDidPlayToEndTimeNotification(_ notification: Notification) {
        NSLog("avPlayerItemDidPlayToEndTimeNotification")
        if (!avPlayerView_SECONDARY.view.isHidden) {
            print(avPlayer_SECONDARY.currentItem)
            let videoclip = cameraGetNextFromDictionary()
            //            let cameraname: String = videoclip.Camera!.Name!
            //multiangleCameraNameSelectedLabel.text = cameraname
            playMultiangleCameraAvPlayerAtIndex(videoclip.ID!)
            
        }else {
            print(avPlayer_MAIN.currentItem)
            avPlayer_MAIN.seek(to: kCMTimeZero)
            avPlayer_MAIN.play()
        }
        
        DispatchQueue.main.async {
            self.view.superview?.bringSubview(toFront: self.multicamPanel.view)
            self.multicamPanel.panelMultiAngleMenu.layer.zPosition = 1000
        }
    }
    
    
    func cameraGetNextFromDictionary() -> (VideoClipModel){
        var k: Int = 0;
        for (key, value) in dictMultiangleCameraOrder {
            if(value.ID == selectedCameraCurrent){
                if(key == String(dictMultiangleCameraOrder.count-1)){
                    k=0
                }else{
                    k = Int(key)! + 1
                }
                break
            }
        }
        
        return dictMultiangleCameraOrder[String(k)]!
    }
    
    func multicamGetDataFromDictionary(_ item: AVPlayerItem) -> (Int){
        var k: Int = 0;
        for (key, value) in dictMultiangleCameraAvPlayerQueue {
            if(value == item){
                k = Int(key)!
                break
            }
        }
        return k
    }
    
    override func pressesChanged(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        NSLog("pressChanged")
        
        let press = presses.first!
        NSLog("type --> " + String(press.type.rawValue))
    }
    
    override func pressesBegan(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        NSLog("press")
        
        let press = presses.first!
        NSLog("type --> " + String(press.type.rawValue))
    }
    
    override func pressesEnded(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        NSLog("pressEnded")
        
        let press = presses.first!
        NSLog("type --> " + String(press.type.rawValue))
        
        if (canCloseTheView && avPlayer_MAIN.rate != 0) {
            if (menuViewController.view.alpha != 0 && (!viewOtherMatchesIsActive && !viewNotificationsIsActive && !viewMultiangleCameraIsActive && !viewLineupsIsActive)) {
                if (menuOtherMatchesIsActive) {
                    ngs_analytics_setEvent(videoUrl, matchId: String(matchId), moduleName: "OpenPanel", eventName: "LiveMatches", linkID: "LiveMatches")
                    viewOtherMatches()
                }
                if (menuNotificationsIsActive) {
                    ngs_analytics_setEvent(videoUrl, matchId: String(matchId), moduleName: "OpenPanel", eventName: "Notifications", linkID: "Notifications")
                    viewNotifications()
                }
                if (menuMultiangleCameraIsActive) {
                    ngs_analytics_setEvent(videoUrl, matchId: String(matchId), moduleName: "OpenPanel", eventName: "Multicam", linkID: "Multicam")
                    viewMultiangleCamera()
                }
                if (menuLineupsIsActive) {
                    ngs_analytics_setEvent(videoUrl, matchId: String(matchId), moduleName: "OpenPanel", eventName: "Lineups", linkID: "Lineups")
                    viewLineups()
                }
            } else {
                goBackToPlayer()
            }
        }
    }
    
    override func pressesCancelled(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        NSLog("pressCancelled")
        
        let press = presses.first!
        NSLog("type --> " + String(press.type.rawValue))
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        NSLog("touch")
        NSLog("player rate --> \(avPlayer_MAIN.rate)")
        
        if (canCloseTheView && avPlayer_MAIN.rate != 0 && !viewOtherMatchesIsActive && !viewNotificationsIsActive && !viewLineupsIsActive) {
            let touch:UITouch = touches.first!
            let newLocation = touch.location(in: self.view)
            touchCheckpointX = newLocation.x
            touchCheckpointY = newLocation.y
            
            avPlayerView_MAIN.view.addSubview(self.menuViewController.view)
            self.menuViewController.scoreUpdate(actualMatchObj!, traslations: translationData!, teamLogoUrl: (viewConfiguration?.images.getValue("teamLogo"))!)
            self.menuViewController.showPanel()
            
            //opacityOverlay.frame = UIScreen.mainScreen().bounds
            resetMenu()
            

        }
        else if(avPlayer_SECONDARY.rate != 0 && viewMultiangleCameraIsActive){
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.multicamPanel.panelMultiAngleShow()
                
                }, completion: nil)
            
        }
        
        
        //super.touchesBegan(touches, withEvent:event)
        self.next?.touchesBegan(touches, with: event)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        //NSLog("touchMoved")
        
        if (canCloseTheView && avPlayer_MAIN.rate != 0 && !viewOtherMatchesIsActive && !viewNotificationsIsActive && !viewMultiangleCameraIsActive && !viewLineupsIsActive) {
            //NSLog("touchMoved --> OK")
            //var center = false
            var up = false
            var right = false
            var down = false
            var left = false
            
            let touch:UITouch = touches.first!
            let newLocation = touch.location(in: self.view)
            let prevLocation = touch.previousLocation(in: self.view)
            
            if (newLocation.x - prevLocation.x > touchToleranceLight) {
                NSLog("touchMove -> right")
                right = true
            } else if (prevLocation.x - newLocation.x > touchToleranceLight) {
                NSLog("touchMove -> left")
                left = true
            } else if (newLocation.x - touchCheckpointX > touchToleranceMax) {
                NSLog("touchMoveForced -> right")
                right = true
            } else if (touchCheckpointX - newLocation.x > touchToleranceMax) {
                NSLog("touchMoveForced -> left")
                left = true
            } else if (newLocation.y - prevLocation.y > touchToleranceLight) {
                NSLog("touchMove -> down")
                down = true
            } else if (prevLocation.y - newLocation.y > touchToleranceLight) {
                NSLog("touchMove -> top")
                up = true
            } else if (newLocation.y - touchCheckpointY > touchToleranceMax) {
                NSLog("touchMoveForced -> down")
                down = true
            } else if (touchCheckpointY - newLocation.y > touchToleranceMax) {
                NSLog("touchMoveForced -> top")
                up = true
            }
            
            if (menuOtherMatchesIsActive) {
                up = false
            }
            if (menuNotificationsIsActive) {
                right = false
            }
            if (menuMultiangleCameraIsActive) {
                down = false
            }
            if (menuLineupsIsActive) {
                left = false
            }
            
            if (up || right || down || left) {
                NSLog("touchMove --> new Direction --> up \(up) right \(right) down \(down) left \(left) ")
                
                resetMenu()
                
                if (up) {
                    menuViewController.showMenuUp()
                    
                    menuOtherMatchesIsActive = true
                } else if (down) {
                    menuViewController.showMenuDown()
                    menuMultiangleCameraIsActive = true
                } else if (right) {
                    menuViewController.showMenuRight()
                    menuNotificationsIsActive = true
                } else if (left) {
                    menuViewController.showMenuLeft()
                    menuLineupsIsActive = true
                }
                
                touchCheckpointX = newLocation.x
                touchCheckpointY = newLocation.y
            }
        }
        
        self.next?.touchesMoved(touches, with: event)
    }

    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        NSLog("touchEnd")
      
        if (canCloseTheView && menuViewController.view.alpha != 0) {
            resetMenu()
            UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.menuViewController.view.alpha = 0
                }, completion: {
                    finish in
                    self.menuViewController.view.removeFromSuperview()
            })
        }
        else if(viewMultiangleCameraIsActive && self.multicamPanel.panelMultiAngleMenu.alpha != 0){
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.multicamPanel.panelMultiAngleRemoveDelayed()
                self.multicamPanel.panelMultiAngleMenu.layer.zPosition = 1000
                }, completion: nil)
            
        }
        self.next?.touchesEnded(touches, with: event)
    }
    
    
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        NSLog("touchCancelled")
        
        if (canCloseTheView && menuViewController.view.alpha != 0) {
            resetMenu()
            UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.menuViewController.view.alpha = 0
                }, completion: {
                    finish in
                    self.menuViewController.view.removeFromSuperview()
            })
        }
        else if(viewMultiangleCameraIsActive && self.multicamPanel.panelMultiAngleMenu.alpha != 0){
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.multicamPanel.panelMultiAngleRemoveDelayed()
                self.multicamPanel.panelMultiAngleMenu.layer.zPosition = 1000
                }, completion: nil)
            
        }
        
        self.next?.touchesCancelled(touches, with: event)
    }
    
    func resetMenu() {
        NSLog("resetMenu")

        menuViewController.hideMenu()
        
        menuOtherMatchesIsActive = false
        menuNotificationsIsActive = false
        menuMultiangleCameraIsActive = false
        menuLineupsIsActive = false
    }
    
    
    func viewOtherMatches() {
        viewOtherMatchesIsActive = true
        canCloseTheView = false
        menuViewController.view.alpha = 0
        self.multicamPanel.panelMultiAngleMenu.alpha = 0
        initView.backgroundColor = UIColor(patternImage: backgroundOtherMatchesImage)
        
        let oldFrame = self.avPlayerView_MAIN.view.frame
        self.avPlayerView_MAIN.view.layer.anchorPoint = CGPoint(x: 0.5, y: 0.9)
        self.avPlayerView_MAIN.view.frame = oldFrame
        
        //SAVE ACTUAL STATE
        mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
        mainPlayerAnchorPointStore = CGPoint(x: 0.92, y: 0.5)
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.avPlayerView_MAIN.view.transform = self.avPlayerView_MAIN.view.transform.scaledBy(x: 0.66, y: 0.66)
            
            self.initView.viewWithTag(2000)?.isHidden = false
            }, completion: nil)
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async {
            self.createTodayMatchesPanel()
        }
    }
    
    func createTodayMatchesPanel(){
        todayMatchesPanel = TodayMatchesPanel(cup: cup, season: season, roundId: (actualMatchObj?.RoundId)!, matchDay: (actualMatchObj?.MatchDay)!, matchId: matchId, teamLogoBaseUrl: viewConfiguration!.images.getValue("teamLogo"), videoDataUrl: viewConfiguration!.feeds["feedVideodata"]!, skin: (self.viewConfiguration!.parameters["skin"] != nil) ? self.viewConfiguration!.parameters["skin"]! : "")
        todayMatchesPanel.delegate = self
        todayMatchesPanel.view.alpha = 1
        todayMatchesPanel.actualDate = actualMatchObj?.DateCET
        DispatchQueue.main.async {
            self.initView.addSubview(self.todayMatchesPanel.view)
        }
    }
    
    
    
    func viewNotifications() {
        viewNotificationsIsActive = true
        canCloseTheView = false
        menuViewController.view.alpha = 0
        self.multicamPanel.panelMultiAngleMenu.alpha = 0
        initView.backgroundColor = UIColor(patternImage: backgroundNotificationsImage)
        
        let oldFrame = self.avPlayerView_MAIN.view.frame
        self.avPlayerView_MAIN.view.layer.anchorPoint = CGPoint(x: 0.1, y: 0.5)
        self.avPlayerView_MAIN.view.frame = oldFrame
        
        //SAVE ACTUAL STATE
        mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
        mainPlayerAnchorPointStore = CGPoint(x: 0.1, y: 0.5)
        
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.avPlayerView_MAIN.view.transform = self.avPlayerView_MAIN.view.transform.scaledBy(x: 0.67, y: 0.67)
            
            self.initView.viewWithTag(3000)?.isHidden = false
            }, completion: nil)
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async {
            self.createNotificationPanel()
        }
    }
    
    
    func createNotificationPanel(){
        notificationPanel = NotificationPanel(cup: cup, season: season, roundId: (actualMatchObj?.RoundId)!, matchDay: (actualMatchObj?.MatchDay)!, matchId: matchId, eventsIconsBaseUrl: viewConfiguration!.images.getValue("events_icons_commentary"), isLive: actualMatchObj!.isLive(), skin: (self.viewConfiguration!.parameters["skin"] != nil) ? self.viewConfiguration!.parameters["skin"]! : "")
        notificationPanel.delegate = self
        notificationPanel.view.alpha = 1
        DispatchQueue.main.async {
            self.initView.addSubview(self.notificationPanel.view)
        }
    }
    
    func viewMultiangleCamera() {
        viewMultiangleCameraIsActive = true
        canCloseTheView = false
        menuViewController.view.alpha = 0
        self.multicamPanel.panelMultiAngleMenu.alpha = 0
        initView.backgroundColor = UIColor(patternImage: backgroundMulticamImage)
        
        let oldFrame = self.avPlayerView_MAIN.view.frame
        self.avPlayerView_MAIN.view.layer.anchorPoint = CGPoint(x: 0.5, y: 0.1)
        self.avPlayerView_MAIN.view.frame = oldFrame
        
        //SAVE ACTUAL STATE
        mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
        mainPlayerAnchorPointStore = CGPoint(x: 0.5, y: 0.1)
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.avPlayerView_MAIN.view.transform = self.avPlayerView_MAIN.view.transform.scaledBy(x: 0.65, y: 0.65)
            }, completion: nil)
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async {
            self.createMulticamPanel()
        }
    }
    
    func createMulticamPanel(){
        multicamPanel = MulticamPanel(cup: cup, season: season, roundId: (actualMatchObj?.RoundId)!, matchDay: (actualMatchObj?.MatchDay)!, matchId: matchId, isLive: actualMatchObj!.isLive(), urlEventsIcons: viewConfiguration!.images.getValue("events_icons_multicam"), skin: (self.viewConfiguration!.parameters["skin"] != nil) ? self.viewConfiguration!.parameters["skin"]! : "")
        multicamPanel.delegate = self
        multicamPanel.view.alpha = 1
        
        DispatchQueue.main.async {
            self.initView.addSubview(self.multicamPanel.view)
        }
    }
    
    
    
    func playMultiangleCameraAvPlayerAtIndex(_ cameraid: String) {
        
        if (avPlayer_SECONDARY.rate != 0) {
            avPlayer_SECONDARY.pause()
            avPlayer_SECONDARY.currentItem?.seek(to: kCMTimeZero)
        }
        selectedCameraCurrent = cameraid
        avPlayer_SECONDARY.removeAllItems()
        if(dictMultiangleCameraAvPlayerQueue.count>0){
            if(dictMultiangleCameraAvPlayerQueue[String(cameraid)] != nil){
                avPlayer_SECONDARY.insert(dictMultiangleCameraAvPlayerQueue[String(cameraid)]!, after: nil)
                avPlayer_SECONDARY.play()
            }
        }

        
    }
    
    
    func viewLineups() {
        viewLineupsIsActive = true
        canCloseTheView = false
        menuViewController.view.alpha = 0
        self.multicamPanel.panelMultiAngleMenu.alpha = 0
        initView.backgroundColor = UIColor(patternImage: backgroundLineupsImage)
        //initView.backgroundColor = UIColor.blueColor()
        
        let oldFrame = self.avPlayerView_MAIN.view.frame
        self.avPlayerView_MAIN.view.layer.anchorPoint = CGPoint(x: 0.92, y: 0.5)
        self.avPlayerView_MAIN.view.frame = oldFrame
        
        //SAVE ACTUAL STATE
        mainPlayerFrameStore = self.avPlayerView_MAIN.view.frame
        mainPlayerAnchorPointStore = CGPoint(x: 0.92, y: 0.5)
        
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.avPlayerView_MAIN.view.transform = self.avPlayerView_MAIN.view.transform.scaledBy(x: 0.52, y: 0.52)
            
            self.initView.viewWithTag(1000)?.isHidden = false
            //self.spinnerLineups.startAnimating()
            
            
            }, completion: nil)
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async {
            self.createLineupsPanel()
        }
    }
    
    
    func createLineupsPanel(){
        lineupsPanel = LineupsPanel(cup: cup, season: season, roundId: (actualMatchObj?.RoundId)!, matchDay: (actualMatchObj?.MatchDay)!, matchId: matchId, urlEventsIcons: viewConfiguration!.images.getValue("events_icons_lineup"), skin: (self.viewConfiguration!.parameters["skin"] != nil) ? self.viewConfiguration!.parameters["skin"]! : "")
        lineupsPanel.view.alpha = 1
        
        DispatchQueue.main.async {
            self.initView.addSubview(self.lineupsPanel.view)
            self.lineupsPanel.view.isUserInteractionEnabled = true
        }
    }
    
    //////////////////////////////////////////////
    //////////// COMMON FUNCTIONS ////////////////   <--- <3
    //////////////////////////////////////////////
    
    
    
    func goToMatch(_ videoData:VideoDataModel, matchData: Match){
        
        DataProvider.sharedInstance.dismissMatchData()
        
        self.videoUrl = videoData.videoUrl
        self.matchId = Int(videoData.eventId)!
        self.viewConfiguration = viewConfiguration
        backActions()
        
        removePlayers()
        
        viewDidLoad()
        viewDidAppear(true)
        

//        
//        //UPDATE VIEW
//        InitData()
//        InitAvPlayer()

        
        
        //DataProvider.sharedInstance.dismissMatchData()
        //navigationController?.popViewControllerAnimated(false)
        
        //Match.playMatch(videoData, matchId: matchData.MatchId, viewConfiguration: viewConfiguration!)
        
    }
    
    
    func createPictureInPictureView(){
        
        var oldFrame = avPlayerView_MAIN.view.frame
        avPlayerView_MAIN.view.layer.anchorPoint = CGPoint(x: 0, y: 0)
        avPlayerView_MAIN.view.frame = oldFrame
        
        oldFrame = avPlayerView_SECONDARY.view.frame
        avPlayerView_SECONDARY.view.layer.anchorPoint = CGPoint(x: 0, y: 0)
        avPlayerView_SECONDARY.view.frame = oldFrame
        avPlayerView_SECONDARY.view.alpha = 0
        self.addChildViewController(avPlayerView_SECONDARY)
        initView.addSubview(avPlayerView_SECONDARY.view)
        
        mainPlayerFrameStore = avPlayerView_MAIN.view.frame
        mainPlayerAnchorPointStore = CGPoint(x: 0, y: 0)
        
        secondaryPlayerFrameStore = avPlayerView_SECONDARY.view.frame
        secondaryPlayerAnchorPointStore = CGPoint(x: 0, y: 0)
        
        avPlayerTempWidth = screenRect.width // self.avPlayerView_MAIN.view.frame.width
        self.avPlayerView_MAIN.view.layer.zPosition = 1000
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.avPlayerView_MAIN.view.frame.origin.x = 50
            self.avPlayerView_MAIN.view.frame.origin.y = 50
            
            if(!self.actualMatchObj!.isLive() && self.viewConfiguration!.parameters["IS_DEMO_MODE"]! == "false"){
                self.avPlayerView_MAIN.view.transform = self.avPlayerView_MAIN.view.transform.scaledBy(x: 0.0000001, y: 0.0000001)
            }else{
                self.avPlayerView_MAIN.view.transform = self.avPlayerView_MAIN.view.transform.scaledBy(x: 0.35, y: 0.35)
                
            }
            }, completion: {
                (Value: Bool) in
                
                self.avPlayerView_MAIN.view.isHidden = false
        })
        
        self.avPlayerView_SECONDARY.view.frame.origin.x = 0
        self.avPlayerView_SECONDARY.view.frame.origin.y = 0
        self.avPlayerView_SECONDARY.view.isHidden = false
        
        UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.avPlayerView_SECONDARY.view.alpha = 1
            
            self.avPlayerView_SECONDARY.view.transform = self.avPlayerView_SECONDARY.view.transform.scaledBy(x: self.avPlayerTempWidth / self.avPlayerView_SECONDARY.view.frame.width, y: self.avPlayerTempWidth / self.avPlayerView_SECONDARY.view.frame.width)
            self.avPlayer_SECONDARY.play()
            self.avPlayer_SECONDARY.isMuted = true
            
            
            }, completion: {
                (Value: Bool) in
        })
        
        
    }
    
    func createTwoLiveView(_ transform: Bool = true){
        avPlayerView_SECONDARY.view.layer.anchorPoint = CGPoint(x: 0, y: 0)
        avPlayerView_SECONDARY.view.frame = CGRect(x: 980, y: 420, width: 850, height: 531)
        avPlayerTempWidth = screenRect.width // self.avPlayerView_MAIN.view.frame.width
        
        self.addChildViewController(avPlayerView_SECONDARY)
        initView.addSubview(avPlayerView_SECONDARY.view)
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            if(transform){
                self.avPlayerView_MAIN.view.transform = self.avPlayerView_MAIN.view.transform.scaledBy(x: 0.5, y: 0.5)
                self.avPlayerView_MAIN.view.frame = CGRect(x: 90, y: 420, width: 850, height: 531)
            }
            
            }, completion: {
                (Value: Bool) in
                //self.avPlayerView_MAIN.view.frame = CGRect(x: 90, y: 420, width: 850, height: 531)
                self.avPlayerView_MAIN.view.isHidden = false
                self.avPlayerView_MAIN.view.layer.zPosition = 1000
                self.avPlayerView_SECONDARY.view.isHidden = false
                
                UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                    self.avPlayerView_SECONDARY.view.transform = self.avPlayerView_SECONDARY.view.transform.scaledBy(x: 1.0, y: 1.0)
                    self.avPlayer_SECONDARY.play()
                    self.avPlayer_SECONDARY.isMuted = true
                    }, completion: {
                        (Value: Bool) in
                        self.avPlayerView_SECONDARY.view.backgroundColor = UIColor.black
                })
                
        })
    }
    
    func createPlaylistAvPlayer_SECONDARY(_ dictQueue: inout Dictionary<String, AVPlayerItem>, ID: String, url: String, isFirst: Bool){
        
        //TOKENIZE URL
        
        let urlTokenized: String = AkamaiTokenizer.tokenize(url, secret: secret)
        
        let url = URL(string: urlTokenized)
        let avAsset = AVAsset(url: url!)
        let avPlayerItem = AVPlayerItem(asset: avAsset)
        
        dictQueue[ID] = avPlayerItem
        if(isFirst){
            selectedCameraCurrent = ID
            avPlayer_SECONDARY.insert(avPlayerItem, after: nil)
        }
    }
    
    func removePlayers(){
        
        
        UIView.animate(withDuration: 0.3, animations: {
            self.avPlayer_MAIN.pause()
            self.avPlayer_SECONDARY.pause()
            
            self.avPlayerView_MAIN.view.alpha = 0
            self.avPlayerView_SECONDARY.view.alpha = 0
            
            self.avPlayer_MAIN.removeAllItems()
            self.avPlayerView_MAIN.removeFromParentViewController()
            
            self.avPlayer_SECONDARY.removeAllItems()
            self.avPlayerView_SECONDARY.removeFromParentViewController()
        })
        
        
    }
    
    func drawMatchName(_ match: Match, origin: CGPoint){
        
        let home: String? =  translationData!.teams[String(match.HomeTeamId)]?.name
        let away: String? =  translationData!.teams[String(match.AwayTeamId)]?.name

        let homeTeamLabel = UILabel()
        let awayTeamLabel = UILabel()
        
        homeTeamLabel.text = home?.uppercased()
        awayTeamLabel.text = away?.uppercased()
        
        let imgHome = Utility.GetCachedImage("lineup_team_" + String(match.HomeTeamId), url: (viewConfiguration?.images.getValue("teamLogo").replacingOccurrences(of: "{TEAMID}", with: String(match.HomeTeamId)))!)
        
        let imgAway = Utility.GetCachedImage("lineup_team_" + String(match.AwayTeamId), url: (viewConfiguration?.images.getValue("teamLogo").replacingOccurrences(of: "{TEAMID}", with: String(match.AwayTeamId)))!)
        
        let homeTeamImage = UIImageView()
        let awayTeamImage = UIImageView()
        
        homeTeamImage.image = imgHome
        awayTeamImage.image = imgAway
        
    }
    
    func multiangleRemove(){
        //multiangleCameraSelectedScrollView.alpha = 0
        self.dictMultiangleCameraAvPlayerQueue = [:]
        avPlayer_SECONDARY.removeAllItems()
    }
    
    // MARK: -
    
    // MARK: Panel delegates
    func eventSelected(_ event: EventModel) {
        
        isPictureInPicture = true
        var videoId = 0
        if(actualMatchObj!.isLive()){
            videoId = actualMatchObj!.Videos.Live[0].VideoId
        }else if(actualMatchObj!.isScheduled()){
            videoId = actualMatchObj!.Videos.Scheduled[0].VideoId
        }else if(actualMatchObj!.isReplay()){
            videoId = actualMatchObj!.Videos.Replay[0].VideoId
        }
        
        
        if(videoId>0){
            videoData = VideoDataController().getVideoData(viewConfiguration!.feeds["feedVideodata"]!, videoId: String(videoId))
            if (videoData.videoUrl.isEmpty == false) {
                matchId = Int(videoData.eventId)!
                videoUrl = videoData.videoUrl
                seekTime = ""
                videoStartTime = videoData.timeCodeIn
                if(videoStartTime.characters.count>0 && String(describing: event.time).characters.count>0){
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                    
                    
                    let timecode = String(describing: event.time)
                    
                    let dateStringFormatter = DateFormatter()
                    dateStringFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateStringFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +SSSS"
                    if(videoId != 84916){
                        dateStringFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
                    }
                    let eventTimeDate = dateStringFormatter.date(from: timecode)
            
                    let startTimeDate = dateFormatter.date(from: (videoStartTime as NSString).substring(to: 19))
                    
                    let userCalendar = Calendar.current
                    let hourMinuteComponents: NSCalendar.Unit = [.hour, .minute, .second]
                    let timeDifference = (userCalendar as NSCalendar).components(
                        hourMinuteComponents,
                        from: startTimeDate!,
                        to: eventTimeDate!,
                        options: [])
                    
                    let diff = (timeDifference.hour! * 3600) + (timeDifference.minute! * 60) + timeDifference.second!
                    
                    var time = CMTimeMakeWithSeconds(1,1)
                    
                    var _dictMultiangleCameraAvPlayerQueue: Dictionary<String, AVPlayerItem> = [:]
                    createPlaylistAvPlayer_SECONDARY(&_dictMultiangleCameraAvPlayerQueue, ID: String(describing: event.id), url: videoData.videoUrl, isFirst: true)
                    
                    if(avPlayer_MAIN.currentItem?.accessLog()?.events.first?.playbackType == "LIVE"){
                        time = CMTimeMakeWithSeconds(Double(diff-10), 1)
                    }else{
                        time = CMTimeMakeWithSeconds(Double(diff-10), avPlayer_MAIN.currentItem!.asset.duration.timescale)
                    }
                    createPictureInPictureView()
                    
                    notificationPanel.view.alpha = 0
                    avPlayer_SECONDARY.seek(to: time)
                    
                }
                
                
            }
            
        }
    }
    
    func matchSelected(_ videoId: Int, videoUrl: String, transform: Bool, newMatch: Match)
    {
        var _dictMultiangleCameraAvPlayerQueue = Dictionary<String, AVPlayerItem>()
        avPlayer_SECONDARY.pause()
        avPlayer_SECONDARY.removeAllItems()
        
        createPlaylistAvPlayer_SECONDARY(&_dictMultiangleCameraAvPlayerQueue, ID: String(videoId), url: videoUrl, isFirst: true)
        
        createTwoLiveView(transform)
    }
    
    func goToNewMatch(_ newMatch: Match, videoData: VideoDataModel){
        avPlayer_MAIN.pause()
        avPlayer_SECONDARY.pause()
        avPlayer_MAIN.removeAllItems()
        avPlayer_SECONDARY.removeAllItems()
        goToMatch(videoData, matchData: newMatch)
        
        
        
        
        
        
        
        
        
        
    }
    
    func multicamSelected(_ cameras: ClipModel){
        avPlayer_SECONDARY.removeAllItems()
        
        viewMultiangleCameraHighlightedIsActive = true
        
        var i = 0;
        for item:VideoClipModel in cameras.Videos {
            if(item.UrlMP4 != nil){
                createPlaylistAvPlayer_SECONDARY(&dictMultiangleCameraAvPlayerQueue, ID: item.ID!, url: item.UrlMP4!, isFirst: i==0)
            }else if(item.UrlHLS != nil){
                createPlaylistAvPlayer_SECONDARY(&dictMultiangleCameraAvPlayerQueue, ID: item.ID!, url: item.UrlHLS!, isFirst: i==0)
            }else{
                return
            }
            dictMultiangleCameraOrder[String(i)] = item
            
            i += 1;
        }
        
        createPictureInPictureView()
        
    }
    
    func multiangleSelected(_ cameraid: String){
        
        self.addChildViewController(avPlayerView_SECONDARY)
        initView.addSubview(avPlayerView_SECONDARY.view)
        
        playMultiangleCameraAvPlayerAtIndex(cameraid)
    }
    
    func goBack(){
        self.goBackToMulticam()
    }
    
}
