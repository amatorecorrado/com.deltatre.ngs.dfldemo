//
//  NGSAnalytics.swift
//  AppleTV
//
//  Created by Corrado Amatore on 08/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

open class NGSAnalytics{

    var parameter_ViewName: String = ""
    var parameter_ApplicationID: String = ""
    var parameter_ApplicationName: String = ""
    var parameter_ApplicationVersion: String = ""
    
    var parameter_VideoUrl: String = ""
    var parameter_MatchID: String = ""
    
    var parameter_EventCategory: String = ""
    var parameter_EventAction: String = ""
    var parameter_EventLabel: String = ""
    
    var parameter_LinkID: String = ""
    

    var parameters: Dictionary<String, String>? = [:]
    
    
    public init(){
        parameter_ApplicationID = (Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String)!
        parameter_ApplicationName = (Bundle.main.infoDictionary!["CFBundleName"] as! String)
        parameter_ApplicationVersion = (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String)!
    }
    
    open func setView(_ ViewName: String){
        parameter_ViewName = ViewName
        getParameters()
        
        GATracker.sharedInstance.screenView(screenName: parameter_ViewName, customParameters: parameters)
    }
    
    open func setEvent(_ EventCategory: String, EventAction: String, EventLabel: String){
        
        parameter_EventCategory = EventCategory
        parameter_EventAction = EventAction
        parameter_EventLabel = EventLabel
        
        getParameters()
        
        
        GATracker.sharedInstance.event(category: parameter_EventCategory, action: EventAction, label: EventLabel, customParameters: parameters)
    }
    
    fileprivate func getParameters(){
        
        parameters!["t"] = "screenview"
        parameters!["ds"] = "app tvos"
        parameters!["dh"] = "appletv.deltatre.com"
        
        if(parameter_ViewName != ""){
            parameters!["cd"] = parameter_ViewName
            parameters!["dt"] = parameter_ViewName
            parameters!["dp"] = "/" + parameter_ViewName
        }
        if(parameter_VideoUrl != ""){
            parameters!["videourl"] = parameter_VideoUrl
        }
        if (parameter_MatchID != ""){
            parameters!["matchid"] = parameter_MatchID
            parameters!["ev"] = parameter_MatchID
        }
        if (parameter_ApplicationID != ""){
            parameters!["aid"] = parameter_ApplicationID
            parameters!["aiid"] = parameter_ApplicationID
        }
        if(parameter_ApplicationName != ""){
            parameters!["an"] = parameter_ApplicationName
        }
        if(parameter_ApplicationVersion != ""){
            parameters!["av"] = parameter_ApplicationVersion
        }
        if(parameter_EventCategory != ""){
            parameters!["ec"] = parameter_EventCategory
        }
        if(parameter_EventAction != ""){
            parameters!["ea"] = parameter_EventAction
        }
        if(parameter_EventLabel != ""){
            parameters!["el"] = parameter_EventLabel
        }
        if(parameter_LinkID != ""){
            parameters!["linkid"] = parameter_LinkID
        }
    }
    
}
