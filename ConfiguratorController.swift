//
//  Configurator.swift
//  AppleTV
//
//  Created by Corrado Amatore on 14/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit
import VideoPlayerFeatured

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


//private var _sharedInstanceConfigurator: ConfiguratorController!

class ConfiguratorController{

    var navigationController: UINavigationController?
    //var videoPlayer: ViewPlayer?
    var videoPlayerSimple: ViewPlayerSimple?
    var alert = UIAlertController()
    var configUrl: String = ""
    
    static let sharedInstance = ConfiguratorController()
    
    func action(_ url: String, additionalData: AnyObject?){       

            self.configUrl = url
            let config = read(url)
            if let config = config, let type = config.type , type.characters.count > 0 {
                singletonSetup(config)
                goToView(config,additionalData: additionalData)
            }else{
                let alert = UIAlertController.init(title: "Error loading", message: "Configuration view missing", preferredStyle: UIAlertControllerStyle.alert)
                self.navigationController?.pushViewController(alert, animated: true)
            }
        
        
    }
    
    func goToView(_ config: ConfigurationModel, additionalData: AnyObject?){

        switch config.type! {
        case "menu":
            let controller = ViewHome()
            
            NSLog("Dispatcher View --> Go to View Home")
            controller.viewConfiguration = config
            self.navigationController!.pushViewController(controller, animated: false)
            break
        case "videolist":
            let controller = VideoListViewController(nibName: "VideoListViewController", bundle: nil)
            NSLog("Dispatcher View --> Go to View Matches")
            controller.viewConfiguration = config
            self.navigationController!.pushViewController(controller, animated: true)
            break
        case "live":
            let controller = LiveViewController(nibName: "LiveViewController", bundle: nil)
            NSLog("Dispatcher View --> Go to View Live")
            controller.viewConfiguration = config
            self.navigationController!.pushViewController(controller, animated: true)
            break
        case "matches":
            let controller = MatchesViewController(nibName: "MatchesViewController", bundle: nil)
            NSLog("Dispatcher View --> Go to View Matches")
            controller.viewConfiguration = config
            self.navigationController!.pushViewController(controller, animated: true)
            break
        case "highlights":
            let controller = ViewHighlights()
            //viewHighlights.videos =  videosHighlights
            
            NSLog("Dispatcher View --> Go to View Highlights")
            controller.viewConfiguration = config
            self.navigationController!.pushViewController(controller, animated: true)
            break
        case "backgroundview":
            let viewBackground = ViewBackground()
            viewBackground.viewConfiguration = config
            NSLog("Dispatcher View --> Go to View Background")
            self.navigationController!.pushViewController(viewBackground, animated: true)
            break
        case "player":
            NSLog("Dispatcher View --> Go to View Player")
            
            var dict = additionalData as? [String: String]

            if (dict == nil){
                dict = [:]
                dict!["matchId"] = "108540" //108543
                dict!["videoUrl"] = "http://vod-i.ngs.deltatre.net/5da5cab0-9979-498c-82dc-b950e2d66681/4b2caff7-1f83-4bf4-959f-f665fe0c1b65.ism/manifest(format=m3u8-aapl)"
            }
            //dict!["videoUrl"] = "http://vod-i.ngs.deltatre.net/5da5cab0-9979-498c-82dc-b950e2d66681/4b2caff7-1f83-4bf4-959f-f665fe0c1b65.ism/manifest(format=m3u8-aapl)"
            if(dict != nil){
                let controller = Configurator.shared.actionVideoPlayer(url: self.configUrl, additionalData: dict as AnyObject) as! ViewPlayer
                
                self.navigationController!.pushViewController(controller, animated: true)
            }
            break
        case "player_simple":
            NSLog("Dispatcher View --> Go to View Player Simple")
            
            var dict = additionalData as? [String: String]

            let controller = Configurator.shared.actionVideoPlayerSimple(url: self.configUrl, additionalData: dict as AnyObject)
            self.navigationController!.pushViewController(videoPlayerSimple!, animated: true)

            break
            
        default:
            
            break
        }
        
    }
    
    func read(_ configUrl: String) -> ConfigurationModel?{
        let configurator = ConfigurationModel()
        let url = URL(string: configUrl)!
        let nsdata = try? Data(contentsOf: url)
        if (nsdata?.count > 0) {
            do {
                let json = try JSONSerialization.jsonObject(with: nsdata!, options: .allowFragments) as? [String: Any]
                
                if let val = json?["id"] as? String {
                    configurator.id = val
                }
                if let val = json?["type"] as? String {
                    configurator.type = val
                }
                if let val =  json?["parameters"]{
                    if(val != nil){
                        for param in (val as? [Dictionary<String, AnyObject>])! {
                            addValueToDictionary(param, dict: &(configurator.parameters))
                        }
                    }
                }
                if let val =  json?["feeds"]{
                    if(val != nil){
                        for feed in (val as? [Dictionary<String, AnyObject>])! {
                            addValueToDictionary(feed, dict: &(configurator.feeds))
                        }
                    }
                }
                if let val =  json?["images"]{
                    if(val != nil){
                        for image in (val as? [Dictionary<String, AnyObject>])! {
                            addValueToDictionary(image, dict: &(configurator.images))
                        }
                    }
                }
                if let val =  json?["actions"]{
                    if(val != nil){
                        for action in (val as? [Dictionary<String, AnyObject>])! {
                            addValueToDictionary(action, dict: &(configurator.actions))
                        }
                    }
                }
                if let val =  json?["labels"]{
                    if(val != nil){
                        for label in (val as? [Dictionary<String, AnyObject>])! {
                            addValueToDictionary(label, dict: &(configurator.labels))
                        }}
                }
                if let val =  json?["data"]{
                    if(val != nil){
                        for data in (json?["data"] as? [Dictionary<String, AnyObject>])! {
                            addValueToDictionary(data, dict: &(configurator.data))
                        }
                    }
                }
            } catch {
                NSLog("error serializing JSON: \(error)")
            }
            
            
        }
        return configurator
    }
    
    func singletonSetup(_ config: ConfigurationModel){
        let actualVersion = config.parameters["imagesVersion"]
        if (actualVersion != nil &&  actualVersion != UserDefaults.standard.string(forKey: "imagesVersion")){
            ImagesUtility.sharedInstance.forceDownload = true
            ImagesUtility.sharedInstance.useLocalStorage = true
            //result.Images = ImagesUtility(forceDownload: true, useLocalStorage: true)
            UserDefaults.standard.set(actualVersion, forKey: "imagesVersion")
        }else{
            ImagesUtility.sharedInstance.forceDownload = false
            ImagesUtility.sharedInstance.useLocalStorage = true
            //ImagesUtility.setup(false, useLocalStorage: true)
            //result.Images = ImagesUtility(forceDownload: false, useLocalStorage: true)
        }
    }
    
    func addValueToDictionary(_ value: [String:AnyObject], dict: inout [String:String]){
        if let name = value["name"] as? String {
            if let val = value["value"] as? String {
                dict[name] = val
            }
        }
    }

    
    
}
