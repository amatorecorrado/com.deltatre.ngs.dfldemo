//
//  VideoObject.swift
//  AppleTV
//
//  Created by Corrado Amatore on 01/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public struct VideoModel{
    
    public var match_id: String?
    public var video_id: String?
    public var video_date: Date
    public var matchday: Int?
    public var video_title: String?
    public var video_descr: String?
    public var video_thumb: String?
    public var video_thumb_small: String?
    public var video_thumb_large: String?
    public var match: Match
    public var videoData: VideoDataModel
    
    public init(){
        self.match = Match()
        self.videoData = VideoDataModel()
        self.video_date = Date.init(timeIntervalSince1970: 0)
    }
}
