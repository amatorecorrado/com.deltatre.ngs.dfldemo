//
//  RepeatingTimer.swift
//  DFL
//
//  Created by Corrado Amatore on 18/04/2018.
//  Copyright © 2018 Deltatre. All rights reserved.
//

import Foundation

class RepeatingTimer {
    
    var intervalSeconds: Int = 0
    
    init(){
        
    }
    init(intervalSeconds: Int){
        self.intervalSeconds = intervalSeconds
    }
    
    public lazy var timer: DispatchSourceTimer = {
        initialize()
    }()
    
    private func initialize() -> DispatchSourceTimer{
        let t = DispatchSource.makeTimerSource()
        t.schedule(deadline: .now(), repeating: .seconds(self.intervalSeconds), leeway: .milliseconds(100))
        return t
    }
    
    private enum State {
        case suspended
        case resumed
    }
    private var state: State = .suspended
    func resume() {
        if state == .resumed {
            return
        }
        state = .resumed
        timer.resume()
    }
    
    func suspend() {
        if state == .suspended {
            return
        }
        state = .suspended
        timer.suspend()
    }
    
    func clean(){
        timer.cancel()
        timer.setEventHandler {}
        timer = initialize()
        state = .suspended
    }
    
    func deinitTimer() {
        state = .suspended
        timer.setEventHandler {}
        timer.cancel()
        /*
         If the timer is suspended, calling cancel without resuming
         triggers a crash. This is documented here https://forums.developer.apple.com/thread/15902
         */
        //resume()
    }
}
