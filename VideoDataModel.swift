//
//  VideoDataModel.swift
//  AppleTV
//
//  Created by Corrado Amatore on 01/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public struct VideoDataModel {
    
    var format = ""
    var eventId = ""
    var videoUrl = ""
    var timeCodeIn = ""
    var videoId = ""
}
