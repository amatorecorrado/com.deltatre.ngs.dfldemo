//
//  DispatcherView.swift
//  AppleTV
//
//  Created by Corrado Amatore on 17/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

class DispatcherView: BaseViewController{
    
    let screenRect = UIScreen.main.bounds
    var splashImageView: HighlightImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(isConnected){
            let splashFrame = CGRect(x: 0, y: 0, width: screenRect.width, height: screenRect.height)
            splashImageView = HighlightImage(frame: splashFrame)
            let img =  UIImage(named: "LaunchImage")!
            splashImageView!.image = img
            self.view.addSubview(splashImageView!)
            
            //TEST
            //let viewPlayer = ViewPlayerSimple()
            //viewPlayer.MatchID = 2019340
            //viewPlayer.VideoUrl = "http://clip-p.ngs.deltatre.net/videos/multilateral/001_ma1617/00/00/04/2019340_200906-1-3485309.mp4"
            //navigationController?.pushViewController(viewPlayer, animated: false)
            
            NSLog("Dispatcher View take charge")
            ConfiguratorController.sharedInstance.navigationController = navigationController
            let startConfigurationUrl =  Bundle.main.infoDictionary?["START CONFIGURATION URL"] as! String
            if(startConfigurationUrl.characters.count > 0){
                ConfiguratorController.sharedInstance.action(startConfigurationUrl, additionalData: nil)
                self.removeFromParentViewController()
            }
            
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
}
