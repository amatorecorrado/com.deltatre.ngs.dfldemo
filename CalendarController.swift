//
//  CalendarController.swift
//  AppleTV
//
//  Created by Corrado Amatore on 21/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


open class CalendarController{
    
    fileprivate var _feedUrl: String = ""
    fileprivate var _cup: String = ""
    fileprivate var _season: String = ""
    
    public init(feedUrl: String, cup: String, season: String){
        _feedUrl = feedUrl
        _cup = cup
        _season = season
    }
    open func get() -> [MatchdayModel]{
        var results: [MatchdayModel] = []
        var md = MatchdayModel()
        let url = _feedUrl.replacingOccurrences(of: "{CUP}", with: _cup).replacingOccurrences(of: "{SEASON}", with: _season)
        let json = try? Data(contentsOf: URL(string: url)!)
        if (json?.count > 0) {
            do {
                let json = try JSONSerialization.jsonObject(with: json!, options: .allowFragments)as? [String: Any]
                for item in (json?["matchdays"] as? [Dictionary<String, AnyObject>])!{
                    md.date_string_formatted = item["date_string_formatted"] as! String
                    md.matchday_id = item["matchday_id"] as! Int
                    md.phase = item["phase"] as! Int
                    md.round_code = item["round_code"] as! String
                    md.round_id = item["round_id"] as! Int
                    md.round_n = item["round_n"] as! String
                    md.url = item["url"] as! String
                    md.dates = item["dates"] as! [String]
                    results.append(md)
                }
                
            } catch {
                NSLog("error serializing JSON dictionary events type: \(error)")
            }
        }
        return results
    }
}
