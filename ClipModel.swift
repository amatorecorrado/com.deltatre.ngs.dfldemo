//
//  Clip.swift
//  AppleTV
//
//  Created by Corrado Amatore on 11/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public struct ClipModel{
    public var ID: String?
    public var Title: String?
    public var minute: String?
    public var timeCodeIn: Date?
    public var ThumbnailUrl: String?
    public var jsonDetailUrl: String?
    
    public var Phase: String?
    
    public var competitionID: Int?
    public var competitionTypeID: Int?
    public var seasonID: Int?
    public var roundID: Int?
    public var matchdayID: Int?
    public var sessionID: Int?
    public var matchID: Int?
    public var eventID: String?
    
    public var Event: EventModel
    public var Videos: [VideoClipModel]
    
    public init() {

        self.timeCodeIn = Date.init(timeIntervalSince1970: 0)
        self.Videos = []
        self.Event = EventModel()
        
    }
}
