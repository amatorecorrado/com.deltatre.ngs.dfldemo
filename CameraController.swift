//
//  CameraController.swift
//  AppleTV
//
//  Created by Corrado Amatore on 19/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



open class CameraController{

    fileprivate var _feedUrl: String = ""
    
    public init(feedUrl: String){
        _feedUrl = feedUrl
    }
    open func get() -> [CameraModel]{
        var cameras: [CameraModel] = []
        let jsonCameras = try? Data(contentsOf: URL(string: _feedUrl)!)
        if (jsonCameras?.count > 0) {
            do {
                let json = try JSONSerialization.jsonObject(with: jsonCameras!, options: .allowFragments) as? [String: Any]
                for item in (json?["cameras"] as? [Dictionary<String, AnyObject>])!{
                    let ID = item["id"] as! String
                    let Name = item["name"] as! String
                    let Code = item["code"] as! String
                    cameras.append(CameraModel(ID: ID, Code: Code, Name: Name))
                }
                
            } catch {
                NSLog("error serializing JSON dictionary events type: \(error)")
            }
        }
        return cameras
    }
    
    open static func  getCameraFromCode(_ code: String, cameras: [CameraModel]) -> CameraModel?{
        for cam in cameras {
            if(cam.Code?.lowercased() ==  code.lowercased()){
                return cam
            }
        }
        var def = CameraModel();
        def.Code = code
        def.ID = code
        def.Name = code
        return def
    }
}
