//
//  PlayByPlayModel.swift
//  AppleTV
//
//  Created by Corrado Amatore on 11/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public struct NotificationModel{

    public var Id: String
    public var TimeCode: String
    public var `Type`: String
    public var EventTypeId: String
    public var EventType: String
    public var EventTypeName: String
    public var GameTime: String
    public var Text: String
    public var ThumbnailUrl: String
    public var Videos: [ModelVideoItem]?
    static public var Cameras: [Camera]?
        
    public init(){
        
        Id = ""
        TimeCode = ""
        Type = ""
        EventTypeId = ""
        EventType = ""
        EventTypeName = ""
        GameTime = ""
        Text = ""
        ThumbnailUrl = ""
        Videos = nil
        
    }
    
    public init(id: String, timeCode: String, type: String, eventTypeId: String, eventType: String, eventTypeName: String, gameTime: String, text: String, thumbnailUrl: String, videos: [ModelVideoItem],vocabularyUrl: String, matchId: Int, cupId: Int, seasonId: Int, urlFeedTranslations : String) {
        Id = id
        TimeCode = timeCode
        Type = type
        EventTypeId = eventTypeId
        EventType = eventType
        EventTypeName = eventTypeName
        GameTime = gameTime
        Text = text
        ThumbnailUrl = thumbnailUrl
        Videos = videos
        
    }
    
    
    public func IsMulticam() ->(Bool){
        return (self.ThumbnailUrl != "" && self.Videos != nil)
        //      return (self.ThumbnailUrl != "" && self.Videos != nil && (self.EventyType == "ShotOnPost" || self.EventyType == "ShotOnBar" || self.EventyType == "ShotOnTarget" || self.EventyType == "ShotWide" || self.EventyType == "Goal" || self.EventyType == "OwnGoal" || self.EventyType == "Substitution" || self.EventyType == "YellowCard" || self.EventyType == "RedCard" || self.EventyType == "Skill" || self.EventyType == "BigChance"))
    }

}
