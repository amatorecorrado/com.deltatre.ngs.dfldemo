//
//  Configuration.swift
//  AppleTV
//
//  Created by Corrado Amatore on 14/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
open class ConfigurationModel {
    var id: String?
    var type: String?
    var feeds: [String:String] = [:]
    var images: [String:String] = [:]
    var parameters: [String:String] = [:]
    var actions: [String:String] = [:]
    var labels: [String:String] = [:]
    var data: [String:String] = [:]
    
    init() {
        
    }
    
    func getParameters() -> ParameterModel{
        var params = ParameterModel()
        
        if let data = self.parameters.getValue(key: "cup"), data.count > 0{
            params.Cup = data
        }
        if let data = self.parameters.getValue(key: "season"), data.count > 0{
            params.Season = data
        }
        if let data = self.parameters.getValue(key: "cupSeason"), data.count > 0{
            params.CupSeason = data
        }
        if let data = self.parameters.getValue(key: "matchId"), data.count > 0{
            params.MatchId = data
        }
        if let data = self.parameters.getValue(key: "cupCode"), data.count > 0{
            params.CupCode = data
        }
        return params
        
    }
    
}
extension Dictionary where Key: ExpressibleByStringLiteral, Value: ExpressibleByStringLiteral {
    func getValue(_ key: String) -> String{
        if let dict = (self as? AnyObject) as? Dictionary<String, AnyObject> {
            let val = dict[key] as? String
            if(val != nil){
                return val!
            }
        }
        return ""
    }
}
